<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/15/2018
 * Time: 5:48 PM
 */
namespace App\Http\Middleware;

use App\Entities\User;
use Illuminate\Auth\Access\AuthorizationException;

/**
 * Class AdminMiddleware
 *
 * @package App\Http\Middleware
 */
class AdminMiddleware
{
    /**
     * @param $request
     * @param \Closure $next
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function handle($request, \Closure $next)
    {
        $user = auth()->guard()->user();

        if ($user->role !== User::ROLE_ADMIN) {
            throw new AuthorizationException("You are not allowed to access resource.");
        }

        return $next($request);
    }
}