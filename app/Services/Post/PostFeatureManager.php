<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:21 PM
 */

namespace App\Services\Post;

use App\Core\Abstracts\AbstractManager;
use App\Entities\Feature;
use App\Entities\Post;

/**
 * Class PostFeatureManager
 *
 * @package App\Services\Post
 */
class PostFeatureManager extends AbstractManager
{
    /**
     * @var \App\Entities\Post
     */
    protected $post;

    /**
     * @var \App\Entities\Feature
     */
    protected $feature;

    /**
     * PostFeatureManager constructor.
     * @param Post $post
     * @param Feature $feature
     */
    public function __construct(Post $post, Feature $feature)
    {
        $this->post = $post;

        $this->feature = $feature;
    }

    /**
     * @param $userId
     * @param $postId
     * @param array $features
     * @return mixed
     */
    public function fillByUser($userId, $postId, $features = [])
    {
        return $this->post
            ->where([
                'user_id' => intval($userId),
                'id' => intval($postId)
            ])
            ->firstOrFail()
            ->features()
            ->sync($features);
    }

    /**
     * @param $userId
     * @param $postId
     * @param $featureId
     * @return mixed
     */
    public function attachByUser($userId, $postId, $featureId)
    {
        $this->check([$featureId]);

        return $this->post
            ->where([
                'user_id' => $userId,
                'id' => $postId
            ])
            ->firstOrFail()
            ->features()
            ->attach($featureId);
    }

    /**
     * @param $userId
     * @param $postId
     * @param $featureId
     * @return mixed
     */
    public function detachByUser($userId, $postId, $featureId)
    {
        $this->check([$featureId]);

        return $this->post
            ->where([
                'user_id' => $userId,
                'id' => $postId
            ])
            ->firstOrFail()
            ->features()
            ->detach($featureId);
    }

    /**
     * @param array $features
     * @return array
     */
    public function check($features = [])
    {
        foreach ($features as $feature) {
            $this->feature->findOrFail($feature);
        }

        return array_unique($features);
    }

    /**
     * @param $postId
     * @param array $features
     * @return mixed
     */
    public function fill($postId, $features = [])
    {
        $features = $this->check($features);

        return $this->post
            ->where('id', $postId)
            ->firstOrFail()
            ->features()
            ->sync($features);
    }

    /**
     * @param $postId
     * @param $featureId
     * @return mixed
     */
    public function attach($postId, $featureId)
    {
        $features = $this->check([$featureId]);

        return $this->post
            ->where('id', $postId)
            ->firstOrFail()
            ->features()
            ->syncWithoutDetaching($features);
    }

    /**
     * @param $postId
     * @param $featureId
     * @return mixed
     */
    public function detach($postId, $featureId)
    {
        $this->check([$featureId]);

        return $this->post
            ->where('id', $postId)
            ->firstOrFail()
            ->features()
            ->detach($featureId);
    }
}