<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 12:25 PM
 */

namespace App\Entities\Relations;

use App\Entities\Country;

/**
 * Trait WithCountry
 *
 * @package App\Entities\Relations
 */
trait WithCountry
{
    /**
     * @return mixed
     */
    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }
}