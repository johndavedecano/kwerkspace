// users module
import { load, show, destroy, update, create } from '@/api/users-api'
import { notify } from '@/shared/notify'
import merge from 'lodash/merge'

import {
  USER_LOAD,
  USER_LOAD_SUCCESS,
  USER_LOAD_FAILURE,
} from '@/store/constants'

const users = {
  namespaced: true,
  state: {
    items: [],
    meta: {},
    isLoading: false,
    isLoaded: false,
  },
  actions: {
    async loadUser({ commit }, params = {}) {
      try {
        commit(USER_LOAD, params)
        const { data, meta } = await load(params)
        commit(USER_LOAD_SUCCESS, { data, meta })
      } catch (err) {
        commit(USER_LOAD_FAILURE)
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
    async showUser(_, id) {
      try {
        const { data } = await show(id)
        return data
      } catch (err) {
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
    async destroyUser(_, id) {
      try {
        const { data } = await destroy(id)
        notify({ type: 'success', text: 'Succesfully Deleted' })
        return data
      } catch (err) {
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
    async updateUser(_, { id, params }) {
      try {
        const { data } = await update(id, params)
        notify({ type: 'success', text: 'Succesfully Updated' })
        return data
      } catch (err) {
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
    async createUser(_, params) {
      try {
        const { data } = await create(params)
        notify({ type: 'success', text: 'Succesfully Created' })
        return data
      } catch (err) {
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
  },
  mutations: {
    [USER_LOAD]: (state, params) => {
      state.isLoading = true
      state.meta = merge(state.meta, params)
    },
    [USER_LOAD_SUCCESS]: (state, { meta, data }) => {
      state.isLoading = false
      state.isLoaded = true
      state.items = data
      state.meta = merge(state.meta, meta)
    },
    [USER_LOAD_FAILURE]: state => {
      state.isLoading = false
      state.isLoaded = false
      state.items = []
      state.meta = {}
    },
  },
  getters: {
    isLoaded: state => state.isLoaded,
    isLoading: state => state.isLoading,
    items: state => state.items,
    meta: state => state.meta,
  },
}

export default users
