import Vue from 'vue'
import Router from 'vue-router'

const Account = () => import('@/views/account/Account')

const Categories = () => import('@/views/categories/Categories')
const CategoriesCreate = () => import('@/views/categories/CategoriesCreate')
const CategoriesUpdate = () => import('@/views/categories/CategoriesUpdate')

const Skills = () => import('@/views/skills/Skills')
const SkillsCreate = () => import('@/views/skills/SkillsCreate')
const SkillsUpdate = () => import('@/views/skills/SkillsUpdate')

const Features = () => import('@/views/features/Features')
const FeaturesCreate = () => import('@/views/features/FeaturesCreate')
const FeaturesUpdate = () => import('@/views/features/FeaturesUpdate')

const Countries = () => import('@/views/countries/Countries')
const CountriesCreate = () => import('@/views/countries/CountriesCreate')
const CountriesUpdate = () => import('@/views/countries/CountriesUpdate')

const Users = () => import('@/views/users/Users')
const UsersCreate = () => import('@/views/users/UsersCreate')
const UsersUpdate = () => import('@/views/users/UsersUpdate')

const Dashboard = () => import('@/views/dashboard/Dashboard')
const DefaultContainer = () => import('@/containers/DefaultContainer')

const Inquiries = () => import('@/views/inquiries/Inquiries')
const Notifications = () => import('@/views/notifications/Notifications')
const Posts = () => import('@/views/posts/Posts')
const Reviews = () => import('@/views/reviews/Reviews')

const Login = () => import('@/views/auth/Login')
const Register = () => import('@/views/auth/Register')
const Forgot = () => import('@/views/auth/Forgot')
const Reset = () => import('@/views/auth/Reset')

Vue.use(Router)

const router = new Router({
  mode: 'history', // https://router.vuejs.org/api/#mode
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      redirect: '/dashboard',
      name: 'Home',
      component: DefaultContainer,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          meta: {
            requiresLogin: true,
          },
          component: Dashboard,
        },
        {
          path: 'categories',
          redirect: 'categories/index',
          name: 'Categories',
          component: {
            render(c) {
              return c('router-view')
            },
          },
          children: [
            {
              path: 'index',
              name: 'Categories Index',
              meta: {
                requiresLogin: true,
              },
              component: Categories,
            },
            {
              path: 'create',
              name: 'Categories Create',
              meta: {
                requiresLogin: true,
              },
              component: CategoriesCreate,
            },
            {
              path: ':id/update',
              name: 'Categories Update',
              meta: {
                requiresLogin: true,
              },
              component: CategoriesUpdate,
            },
          ],
        },
        {
          path: 'features',
          redirect: 'features/index',
          name: 'Features',
          component: {
            render(c) {
              return c('router-view')
            },
          },
          children: [
            {
              path: 'index',
              name: 'Features Index',
              meta: {
                requiresLogin: true,
              },
              component: Features,
            },
            {
              path: 'create',
              name: 'Features Create',
              meta: {
                requiresLogin: true,
              },
              component: FeaturesCreate,
            },
            {
              path: ':id/update',
              name: 'Features Update',
              meta: {
                requiresLogin: true,
              },
              component: FeaturesUpdate,
            },
          ],
        },
        {
          path: 'countries',
          redirect: 'countries/index',
          name: 'Countries',
          component: {
            render(c) {
              return c('router-view')
            },
          },
          children: [
            {
              path: 'index',
              name: 'Countries Index',
              meta: {
                requiresLogin: true,
              },
              component: Countries,
            },
            {
              path: 'create',
              name: 'Countries Create',
              meta: {
                requiresLogin: true,
              },
              component: CountriesCreate,
            },
            {
              path: ':id/update',
              name: 'Countries Update',
              meta: {
                requiresLogin: true,
              },
              component: CountriesUpdate,
            },
          ],
        },
        {
          path: 'inquiries',
          name: 'Inquiries',
          meta: {
            requiresLogin: true,
          },
          component: Inquiries,
        },
        {
          path: 'notifications',
          name: 'Notifications',
          meta: {
            requiresLogin: true,
          },
          component: Notifications,
        },
        {
          path: 'reviews',
          name: 'Reviews',
          meta: {
            requiresLogin: true,
          },
          component: Reviews,
        },
        {
          path: 'posts',
          name: 'Posts',
          meta: {
            requiresLogin: true,
          },
          component: Posts,
        },
        {
          path: 'skills',
          redirect: 'skills/index',
          name: 'Skills',
          component: {
            render(c) {
              return c('router-view')
            },
          },
          children: [
            {
              path: 'index',
              name: 'Skills Index',
              meta: {
                requiresLogin: true,
              },
              component: Skills,
            },
            {
              path: 'create',
              name: 'Skills Create',
              meta: {
                requiresLogin: true,
              },
              component: SkillsCreate,
            },
            {
              path: ':id/update',
              name: 'Skills Update',
              meta: {
                requiresLogin: true,
              },
              component: SkillsUpdate,
            },
          ],
        },
        {
          path: 'account',
          name: 'Account',
          meta: {
            requiresLogin: true,
          },
          component: Account,
        },
        {
          path: 'users',
          redirect: 'users/index',
          name: 'Users',
          component: {
            render(c) {
              return c('router-view')
            },
          },
          children: [
            {
              path: 'index',
              name: 'Users Index',
              meta: {
                requiresLogin: true,
              },
              component: Users,
            },
            {
              path: 'create',
              name: 'Users Create',
              meta: {
                requiresLogin: true,
              },
              component: UsersCreate,
            },
            {
              path: ':id/update',
              name: 'Users Update',
              meta: {
                requiresLogin: true,
              },
              component: UsersUpdate,
            },
          ],
        },
      ],
    },
    {
      path: '/auth',
      redirect: '/auth/login',
      name: 'Auth',
      component: {
        render(c) {
          return c('router-view')
        },
      },
      children: [
        {
          path: 'login',
          name: 'Login',
          component: Login,
        },
        {
          path: 'forgot',
          name: 'Forgot',
          component: Forgot,
        },
        {
          path: 'reset',
          name: 'Reset',
          component: Reset,
        },
        {
          path: 'register',
          name: 'Register',
          component: Register,
        },
      ],
    },
  ],
})

router.beforeEach((to, _, next) => {
  if (to.meta.requiresLogin) {
    if (localStorage.getItem('user') && localStorage.getItem('user')) {
      next()
    } else {
      next({
        path: '/auth/login',
        params: { nextUrl: to.fullPath },
      })
    }
  } else {
    next()
  }
})

export default router
