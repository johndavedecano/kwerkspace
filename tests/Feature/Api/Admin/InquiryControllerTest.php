<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/6/2018
 * Time: 7:46 PM
 */

namespace Tests\Feature\Api\Admin;

use App\Entities\Inquiry;
use App\Entities\Post;
use App\Entities\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\Feature\Api\Authentication;
use Tests\TestCase;

/**
 * Class InquiryControllerTest
 *
 * @package Tests\Feature\Api\Admin
 */
class InquiryControllerTest extends TestCase
{
    use RefreshDatabase, Authentication;

    public function setUp()
    {
        parent::setUp();

        $this->admin = $this->getAuthHeaders(User::ROLE_ADMIN);

        $this->user = $this->getAuthHeaders(User::ROLE_USER);
    }

    public function testIndexFails()
    {
        $response = $this->withHeaders(['Accept' => 'application/json'])->get('/api/admin/inquiries');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testIndexFailsNotAllowed()
    {
        $response = $this->withHeaders($this->user)->get('/api/admin/inquiries');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testIndexSuccess()
    {
        $response = $this->withHeaders($this->admin)->get('/api/admin/inquiries');

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testStoreFails()
    {
        $response = $this->withHeaders(['Accept' => 'application/json'])->post('/api/admin/inquiries');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testStoreValidationFails()
    {
        $response = $this->withHeaders($this->admin)->post('/api/admin/inquiries');

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStoreSuccess()
    {
        $data = [
            'user_id' => factory(User::class)->create()->id,
            'post_id' => factory(Post::class)->create()->id,
            'email' => 'test@gmail.com',
            'name' => 'John Dave Decano',
            'contact_number' => 5646546,
            'description' => 'test',
        ];

        $response = $this->withHeaders($this->admin)->post('/api/admin/inquiries', $data);

        $response->assertStatus(Response::HTTP_CREATED);
    }

    public function testUpdateFails()
    {
        $results = factory(Inquiry::class)->create();

        $response = $this->withHeaders(['Accept' => 'application/json'])->put('/api/admin/inquiries/'.$results->id);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testUpdateFailsValidation()
    {
        $results = factory(Inquiry::class)->create();

        $response = $this->withHeaders($this->admin)->put('/api/admin/inquiries/'.$results->id, ['name' => '']);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdateSuccess()
    {
        $results = factory(Inquiry::class)->create();

        $response = $this->withHeaders($this->admin)->put('/api/admin/inquiries/'.$results->id, ['name' => 'test']);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testDestroyFails()
    {
        $results = factory(Inquiry::class)->create();

        $response = $this->withHeaders(['Accept' => 'application/json'])->delete('/api/admin/inquiries/'.$results->id);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testDestroyFailsNotFound()
    {
        $response = $this->withHeaders($this->admin)->delete('/api/admin/inquiries/3496878963');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testDestroySuccess()
    {
        $results = factory(Inquiry::class)->create();

        $response = $this->withHeaders($this->admin)->delete('/api/admin/inquiries/'.$results->id);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testShowFails()
    {
        $results = factory(Inquiry::class)->create();

        $response = $this->withHeaders(['Accept' => 'application/json'])->get('/api/admin/inquiries/'.$results->id);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testShowFailsNotFound()
    {
        $response = $this->withHeaders($this->admin)->get('/api/admin/inquiries/3496878963');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testShowSuccess()
    {
        $results = factory(Inquiry::class)->create();

        $response = $this->withHeaders($this->admin)->get('/api/admin/inquiries/'.$results->id);

        $response->assertStatus(Response::HTTP_OK);
    }
}