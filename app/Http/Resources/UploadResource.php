<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UploadResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $fields = [
            'dir',
            'filename',
            'filedir',
            's3_url',
            'width',
            'height',
            'filesize',
            'is_squared'
        ];

        return [
            'original' => array_only($this->resource, [
                'original_filename',
                'original_filedir',
                'original_width',
                'original_height',
                's3_url'
            ]),
            'square50' => array_only($this->resource['dimensions']['square50'], $fields),
            'square100' => array_only($this->resource['dimensions']['square100'], $fields),
            'square200' => array_only($this->resource['dimensions']['square200'], $fields)
        ];
    }
}


