<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:22 PM
 */

namespace App\Services\User;

use App\Core\Abstracts\AbstractManager;
use App\Core\Constants;
use App\Entities\User;

/**
 * Class UserManager
 *
 * @package App\Services\User
 */
class UserManager extends AbstractManager
{
    /**
     * @var \App\Entities\User
     */
    public $model;

    /**
     * UserManager constructor.
     *
     * @param \App\Entities\User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create($data = [])
    {
        $user = $this->model->create(array_only($data, $this->model->getFillable()));

        $user->sendEmailVerificationNotification();

        return $user;
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function updateUser($id, $data = [])
    {
        $data = array_except($data, ['email']);

        $data = array_only($data, $this->model->getFillable());

        return $this->model->findOrFail($id)->update($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, $data = [])
    {
        $data = array_only($data, $this->model->getFillable());

        return $this->model->findOrFail($id)->update($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model->findOrFail($id)->update(['status' => Constants::STATUS_DELETED]);
    }
}