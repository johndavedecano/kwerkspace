// reviews-api.js
import axios from 'axios'

export function load(params = {}) {
  return axios.get('/admin/reviews', { params }).then(repsonse => repsonse.data)
}

export function show(id) {
  return axios.get(`/admin/reviews/${id}`).then(repsonse => repsonse.data)
}

export function destroy(id) {
  return axios.delete(`/admin/reviews/${id}`).then(repsonse => repsonse.data)
}

export function update(id, params = {}) {
  return axios
    .put(`/admin/reviews/${id}`, { params })
    .then(repsonse => repsonse.data)
}

export function create(params = {}) {
  return axios.post('/admin/reviews', params).then(repsonse => repsonse.data)
}
