<?php

namespace App\Entities;

use App\Entities\Others\WithSlug;
use App\Entities\Scopes\ScopeActive;
use App\Entities\Scopes\ScopeDeleted;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Country
 *
 * @package App\Entities
 */
class Country extends Model
{
    use ScopeActive, ScopeDeleted;

    /**
     * Database table name
     */
    protected $table = 'countries';

    /**
     * Protected columns from mass assignment
     */
    protected $guarded = ['id'];

    /**
     * Mass assignable columns
     */
    protected $fillable = ['status', 'name', 'slug'];

    /**
     * @var bool
     */
    public $timestamps = false;
}