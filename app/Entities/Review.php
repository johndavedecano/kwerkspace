<?php

namespace App\Entities;

use App\Entities\Relations\WithPost;
use App\Entities\Relations\WithUser;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Review
 *
 * @package App\Entities
 */
class Review extends Model
{
    use WithUser, WithPost;

    const RATING_RANGE = 5;

    /**
     * Database table name
     */
    protected $table = 'reviews';

    /**
     * Protected columns from mass assignment
     */
    protected $guarded = ['id'];

    /**
     * Mass assignable columns
     */
    protected $fillable = ['post_id', 'user_id', 'title', 'description', 'rating'];
}