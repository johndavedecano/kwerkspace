<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/6/2018
 * Time: 7:46 PM
 */

namespace Tests\Feature\Api\Admin;

use App\Entities\Post;
use App\Entities\PostsLike;
use App\Entities\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\Feature\Api\Authentication;
use Tests\TestCase;

/**
 * Class PostLikeControllerTest.php
 *
 * @package Tests\Feature\Api\Admin
 */
class PostLikeControllerTest extends TestCase
{
    use RefreshDatabase, Authentication;

    public function setUp()
    {
        parent::setUp();

        $this->admin = $this->getAuthHeaders(User::ROLE_ADMIN);

        $this->user = $this->getAuthHeaders(User::ROLE_USER);
    }

    public function testIndexFails()
    {
        $response = $this->withHeaders(['Accept' => 'application/json'])->get('/api/admin/posts/1/likes');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testIndexFailsNotAllowed()
    {
        $response = $this->withHeaders($this->user)->get('/api/admin/posts/1/likes');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testIndexSuccess()
    {
        $post = factory(Post::class)->create();

        $response = $this->withHeaders($this->admin)->get('/api/admin/posts/'.$post->id.'/likes');

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testDestroyFails()
    {
        $post = factory(Post::class)->create();

        $like = factory(PostsLike::class)->create();

        $response = $this->withHeaders([])->delete('/api/admin/posts/'.$post->id.'/likes/'.$like->id);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testDestroyFailsNotFound()
    {
        $response = $this->withHeaders($this->admin)->delete('/api/admin/posts/262362362362/likes/23623623623632');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testDestroySuccess()
    {
        $post = factory(Post::class)->create();

        $like = factory(PostsLike::class)->create(['post_id' => $post->id]);

        $response = $this->withHeaders($this->admin)->delete('/api/admin/posts/'.$post->id.'/likes/'.$like->id);

        $response->assertStatus(Response::HTTP_OK);
    }
}