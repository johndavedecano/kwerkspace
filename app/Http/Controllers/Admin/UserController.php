<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\UserResource;
use App\Services\User\UserCollection;
use App\Services\User\UserManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class UserController
 *
 * @package App\Http\Controllers\Admin
 */
class UserController extends Controller
{
    /**
     * @var \App\Services\User\UserManager
     */
    protected $manager;

    /**
     * @var \App\Services\User\UserCollection
     */
    protected $collection;

    /**
     * UserController constructor.
     *
     * @param \App\Services\User\UserCollection $collection
     * @param \App\Services\User\UserManager $manager
     */
    public function __construct(UserCollection $collection, UserManager $manager)
    {
        $this->collection = $collection;

        $this->manager = $manager;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $results = $this->collection->listFromAdmin();

        return UserResource::collection($results);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed|min:6|max:16',
            'password_confirmation' => 'required',
            'country_id' => 'required|exists:countries,id',
            'status' => 'required|in:active,inactive,deleted',
            'role' => 'required|in:user,admin,company',
        ]);

        $results = $this->manager->create($request->all());

        return new UserResource($results);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\UserResource
     */
    public function show($id)
    {
        $results = $this->collection->find($id);

        return new UserResource($results);
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'sometimes|required',
            'email' => 'sometimes|required|email|unique:users,email,'.$id,
            'password' => 'sometimes|required|confirmed|min:6|max:16',
            'country_id' => 'sometimes|required|exists:countries,id',
            'status' => 'sometimes|required|in:active,inactive,deleted',
            'role' => 'sometimes|required|in:admin,company,user',
            'city' => 'sometimes|required|min:6',
            'lat' => 'sometimes|required|numeric',
            'lon' => 'sometimes|required|numeric',
            'linkedin' => 'sometimes|required|url',
            'twitter' => 'sometimes|required|url',
            'facebook' => 'sometimes|required|url',
            'website' => 'sometimes|required|url',
            'youtube' => 'sometimes|required|url',
            'about' => 'sometimes|required',
            'is_public' => 'sometimes|required|boolean',
        ];

        $this->validate($request, $rules);

        $this->manager->update($id, $request->all());

        return $this->show($id);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\UserResource
     */
    public function destroy($id)
    {
        $this->manager->delete($id);

        return $this->show($id);
    }
}
