<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 3:39 PM
 */

namespace App\Entities\Others;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

trait WithSlug
{
    use HasSlug;

    /**
     * Get the options for generating the slug.
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()->generateSlugsFrom($this->slugFrom)->saveSlugsTo($this->slugTo);
    }
}