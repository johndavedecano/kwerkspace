<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 4:02 PM
 */

namespace App\Core\Interfaces;

use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class AbstractCollection
 *
 * @package App\Core\Abstracts
 */
interface CollectionInterface
{
    /**
     * @return \Spatie\QueryBuilder\QueryBuilder
     */
    public function getBuilder(): QueryBuilder;

    /**
     * @param \Spatie\QueryBuilder\QueryBuilder $builder
     */
    public function setBuilder(QueryBuilder $builder): void;
}