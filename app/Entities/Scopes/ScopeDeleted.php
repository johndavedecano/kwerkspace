<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:55 PM
 */

namespace App\Entities\Scopes;

use App\Core\Constants;

trait ScopeDeleted
{
    public function scopeNotDeleted($query)
    {
        return $query->where('status', '<>', Constants::STATUS_DELETED);
    }
}