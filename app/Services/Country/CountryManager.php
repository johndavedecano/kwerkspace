<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:20 PM
 */

namespace App\Services\Country;

use App\Core\Abstracts\AbstractManager;
use App\Core\Constants;
use App\Entities\Country;

class CountryManager extends AbstractManager
{
    /**
     * @var \App\Entities\Country
     */
    public $model;

    /**
     * CountryManager constructor.
     *
     * @param \App\Entities\Country $model
     */
    public function __construct(Country $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $request
     * @return mixed
     */
    public function create($request = [])
    {
        return $this->model->create([
            'name' => $request['name'],
            'slug' => str_slug($request['slug'])
        ]);
    }

    /**
     * @param $id
     * @param array $request
     * @return mixed
     */
    public function update($id, $request = [])
    {
        $data = array_only($request, $this->model->getFillable());

        return $this->model->findOrFail($id)->update($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model->findOrFail($id)->update(['status' => Constants::STATUS_DELETED]);
    }
}