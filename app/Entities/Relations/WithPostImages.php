<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 12:27 PM
 */

namespace App\Entities\Relations;

use App\Entities\PostsImage;

/**
 * Trait WithPostImages
 *
 * @package App\Entities\Relations
 */
trait WithPostImages
{
    /**
     * postsImages
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function images()
    {
        return $this->hasMany(PostsImage::class, 'post_id');
    }
}