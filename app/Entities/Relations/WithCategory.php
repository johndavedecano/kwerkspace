<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 12:30 PM
 */

namespace App\Entities\Relations;

use App\Entities\Category;

/**
 * Trait WithCategory
 *
 * @package App\Entities\Relations
 */
trait WithCategory
{
    /**
     * @return mixed
     */
    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}