<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\SkillResource;
use App\Services\Skill\SkillCollection;
use App\Services\Skill\SkillManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class SkillController
 *
 * @package App\Http\Controllers\Admin
 */
class SkillController extends Controller
{
    /**
     * @var \App\Services\Feature\FeatureManager
     */
    public $manager;

    /**
     * @var \App\Services\Feature\FeatureCollection
     */
    public $collection;

    /**
     * SkillController constructor.
     *
     * @param SkillCollection $collection
     * @param SkillManager $manager
     */
    public function __construct(SkillCollection $collection, SkillManager $manager)
    {
        $this->collection = $collection;

        $this->manager = $manager;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $collection = $this->collection->listFromAdmin();

        return SkillResource::collection($collection);
    }

    /**
     * @param Request $request
     * @return SkillResource
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $results = $this->manager->create($request->get('name'));

        return new SkillResource($results);
    }

    /**
     * @param $id
     * @return SkillResource
     */
    public function show($id)
    {
        $results = $this->collection->find($id);

        return new SkillResource($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \App\Http\Resources\SkillResource
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['name' => 'required', 'status' => 'in:active,inactive,deleted']);

        $this->manager->update($id, $request->all());

        return $this->show($id);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\SkillResource
     */
    public function destroy($id)
    {
        $this->manager->delete($id);

        return $this->show($id);
    }
}
