<?php

namespace App\Http\Controllers\Admin;

use App\Core\Constants;
use App\Http\Resources\FeatureResource;
use App\Services\Feature\FeatureCollection;
use App\Services\Feature\FeatureManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class FeatureController
 *
 * @package App\Http\Controllers\Admin
 */
class FeatureController extends Controller
{
    /**
     * @var \App\Services\Feature\FeatureManager
     */
    public $manager;

    /**
     * @var \App\Services\Feature\FeatureCollection
     */
    public $collection;

    /**
     * FeatureController constructor.
     *
     * @param \App\Services\Feature\FeatureCollection $collection
     * @param \App\Services\Feature\FeatureManager $manager
     */
    public function __construct(FeatureCollection $collection, FeatureManager $manager)
    {
        $this->collection = $collection;

        $this->manager = $manager;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $collection = $this->collection->listFromAdmin();

        return FeatureResource::collection($collection);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\FeatureResource
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $results = $this->manager->create($request->get('name'));

        return new FeatureResource($results);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\FeatureResource
     */
    public function show($id)
    {
        $results = $this->collection->find($id);

        return new FeatureResource($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \App\Http\Resources\FeatureResource
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['name' => 'required', 'status' => 'in:active,inactive,deleted']);

        $this->manager->update($id, $request->all());

        return $this->show($id);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\FeatureResource
     */
    public function destroy($id)
    {
        $this->manager->delete($id);

        return $this->show($id);
    }
}
