<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/5/2018
 * Time: 7:36 PM
 */

namespace App\Core\Abstracts;

use Illuminate\Foundation\Bus\DispatchesJobs;

abstract class AbstractManager
{
    use DispatchesJobs;
}