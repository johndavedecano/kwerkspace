<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/6/2018
 * Time: 8:21 PM
 */

namespace Tests\Feature\Api\User;

use App\Entities\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Http\UploadedFile;
use Tests\Feature\Api\Authentication;
use Tests\TestCase;

class UploadControllerTest extends TestCase
{
    use RefreshDatabase, Authentication;

    public function setUp()
    {
        parent::setUp();

        $this->user = $this->getAuthHeaders(User::ROLE_USER);
    }

    public function testUploadFails()
    {
        $response = $this->withHeaders(['Accept' => 'application/json'])->post('/api/user/upload');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testUploadValidationFails()
    {
        $response = $this->withHeaders($this->user)->post('/api/user/upload');

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUploadFailsBadFile()
    {
        $data = [
            'file' => UploadedFile::fake()->create('test.pdf')
        ];

        $response = $this->withHeaders($this->user)->post('/api/user/upload', $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUploadSuccess()
    {
        $data = [
            'file' => UploadedFile::fake()->image('test.jpg'),
        ];

        $response = $this->withHeaders($this->user)->post('/api/user/upload', $data);

        $response->assertStatus(Response::HTTP_OK);
    }
}