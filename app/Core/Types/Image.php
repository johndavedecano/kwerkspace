<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/6/2018
 * Time: 11:42 AM
 */

namespace App\Core\Types;

use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Image
{
    private $rules = [
        'original' => 'required',
        'square50' => 'required',
        'square100' => 'required',
        'square200' => 'required',
    ];

    private $files = [];

    /**
     * @var string
     */
    private $filesRaw = '';

    /**
     * Image constructor.
     *
     * @param $string
     */
    public function __construct($string)
    {
        $this->filesRaw = $string;

        $files = (array) json_decode($string);

        $validator = Validator::make($files, $this->rules);

        if ($validator->fails()) {
            throw new HttpException(400, 'Invalid image structure');
        }

        $this->files = $files;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->filesRaw;
    }
}