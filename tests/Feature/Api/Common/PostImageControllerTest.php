<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/9/2018
 * Time: 3:53 PM
 */

namespace Tests\Post\Api\Common;

use App\Entities\Post;
use App\Entities\PostsImage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PostImageControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testIndexSuccess()
    {
        $result = factory(Post::class)->create();

        $response = $this->get('api/common/posts/'.$result->id.'/images');

        $response->assertOk();
    }

    public function testShowSuccess()
    {
        $result = factory(Post::class)->create();

        $postImage = factory(PostsImage::class)->create(['post_id' => $result->id]);

        $response = $this->get('api/common/posts/'.$result->id.'/images/'.$postImage->id);

        $response->assertOk();
    }
}