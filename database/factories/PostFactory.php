<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Entities\Post::class, function (Faker $faker) {
    return [
        'user_id' => function () {
            return factory(\App\Entities\User::class)->create()->id;
        },
        'category_id' => $faker->numberBetween(1, 8),
        'country_id' => 175,
        'title' => $faker->text(100),
        'status' => \App\Core\Constants::STATUS_ACTIVE,
        'description' => $faker->paragraph,
        'policies' => $faker->paragraph,
        'max_occupants' => $faker->numberBetween(1, 25),
        'payment_type' => $faker->randomElement(['monthly', 'weekly', 'daily', 'annually', 'hourly']),
        'payment_amount' => $faker->numberBetween(100, 2000),
        'address' => $faker->address,
        'city' => $faker->city,
        'state' => $faker->city,
        'zipcode' => $faker->postcode,
        'lon' => $faker->longitude,
        'lat' => $faker->latitude,
        'area' => 36,
    ];
});
