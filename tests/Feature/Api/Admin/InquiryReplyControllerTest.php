<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/6/2018
 * Time: 7:46 PM
 */

namespace Tests\Feature\Api\Admin;

use App\Entities\InquiriesReply;
use App\Entities\Inquiry;
use App\Entities\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\Feature\Api\Authentication;
use Tests\TestCase;

/**
 * Class InquiryControllerTest
 *
 * @package Tests\Feature\Api\Admin
 */
class InquiryReplyControllerTest extends TestCase
{
    use RefreshDatabase, Authentication;

    public function setUp()
    {
        parent::setUp();

        $this->admin = $this->getAuthHeaders(User::ROLE_ADMIN);

        $this->user = $this->getAuthHeaders(User::ROLE_USER);
    }

    public function testIndexFails()
    {
        $inquiry = factory(Inquiry::class)->create();

        $response = $this->withHeaders(['Accept' => 'application/json'])->get('/api/admin/inquiries/'.$inquiry->id.'/replies');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testIndexFailsNotAllowed()
    {
        $inquiry = factory(Inquiry::class)->create();

        $response = $this->withHeaders($this->user)->get('/api/admin/inquiries/'.$inquiry->id.'/replies');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testIndexSuccess()
    {
        $inquiry = factory(Inquiry::class)->create();

        $response = $this->withHeaders($this->admin)->get('/api/admin/inquiries/'.$inquiry->id.'/replies');

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testStoreFails()
    {
        $inquiry = factory(Inquiry::class)->create();

        $response = $this->withHeaders(['Accept' => 'application/json'])->post('/api/admin/inquiries/'.$inquiry->id.'/replies');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testStoreValidationFails()
    {
        $inquiry = factory(Inquiry::class)->create();

        $response = $this->withHeaders($this->admin)->post('/api/admin/inquiries/'.$inquiry->id.'/replies');

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStoreSuccess()
    {
        $inquiry = factory(Inquiry::class)->create();
        $user = factory(User::class)->create();

        $data = [
            'user_id' => $user->id,
            'post_id' => $inquiry->post_id,
            'inquiry_id' => $inquiry->id,
            'message' => 'hello',
        ];

        $response = $this->withHeaders($this->admin)->post('/api/admin/inquiries/'.$inquiry->id.'/replies', $data);

        $response->assertStatus(Response::HTTP_CREATED);
    }

    public function testUpdateFails()
    {
        $reply = factory(InquiriesReply::class)->create();

        $data = [
            'message' => '',
        ];

        $response = $this->withHeaders(['Accept' => 'application/json'])->put('/api/admin/inquiries/'.$reply->inquiry_id.'/replies/'.$reply->id, $data);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testUpdateValidationFails()
    {
        $reply = factory(InquiriesReply::class)->create();

        $data = [
            'message' => '',
        ];

        $response = $this->withHeaders($this->admin)->put('/api/admin/inquiries/'.$reply->inquiry_id.'/replies/'.$reply->id, $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdateSuccess()
    {
        $reply = factory(InquiriesReply::class)->create();

        $data = [
            'message' => 'test',
        ];

        $response = $this->withHeaders($this->admin)->put('/api/admin/inquiries/'.$reply->inquiry_id.'/replies/'.$reply->id, $data);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testDestroyFails()
    {
        $reply = factory(InquiriesReply::class)->create();

        $response = $this->withHeaders([])->delete('/api/admin/inquiries/'.$reply->inquiry_id.'/replies/'.$reply->id);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testDestroyFailsNotFound()
    {
        $reply = factory(InquiriesReply::class)->create();

        $response = $this->withHeaders($this->admin)->delete('/api/admin/inquiries/'.$reply->inquiry_id.'/replies/456456456');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testDestroySuccess()
    {
        $reply = factory(InquiriesReply::class)->create();

        $response = $this->withHeaders($this->admin)->delete('/api/admin/inquiries/'.$reply->inquiry_id.'/replies/'.$reply->id);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testShowFails()
    {
        $reply = factory(InquiriesReply::class)->create();

        $response = $this->withHeaders([])->get('/api/admin/inquiries/'.$reply->inquiry_id.'/replies/'.$reply->id);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testShowFailsNotFound()
    {
        $reply = factory(InquiriesReply::class)->create();

        $response = $this->withHeaders($this->admin)->get('/api/admin/inquiries/'.$reply->inquiry_id.'/replies/456456456');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testShowSuccess()
    {
        $reply = factory(InquiriesReply::class)->create();

        $response = $this->withHeaders($this->admin)->get('/api/admin/inquiries/'.$reply->inquiry_id.'/replies/'.$reply->id);

        $response->assertStatus(Response::HTTP_OK);
    }
}