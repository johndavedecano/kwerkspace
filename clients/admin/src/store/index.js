import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'

import authentication from './modules/authentication'
import categories from './modules/categories'
import skills from './modules/skills'
import features from './modules/features'
import countries from './modules/countries'
import users from './modules/users'

const debug = process.env.NODE_ENV !== 'production'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    authentication,
    categories,
    skills,
    countries,
    features,
    users,
  },
  strict: debug,
  plugins: debug ? [createLogger()] : [],
})

export default store
