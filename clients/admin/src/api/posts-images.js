// posts-images-api.js
import axios from 'axios'

export function load(postId, params = {}) {
  return axios
    .get(`/admin/posts/${postId}/images`, { params })
    .then(repsonse => repsonse.data)
}

export function show(postId, imageId) {
  return axios
    .get(`/admin/posts/${postId}/images/${imageId}`)
    .then(repsonse => repsonse.data)
}

export function destroy(postId, imageId) {
  return axios
    .delete(`/admin/posts/${postId}/images/${imageId}`)
    .then(repsonse => repsonse.data)
}

export function update(postId, imageId, params = {}) {
  return axios
    .put(`/admin/posts/${postId}/images/${imageId}`, { params })
    .then(repsonse => repsonse.data)
}

export function create(postId, params = {}) {
  return axios
    .post(`/admin/posts/${postId}/images`, params)
    .then(repsonse => repsonse.data)
}
