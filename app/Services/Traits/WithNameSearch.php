<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/5/2018
 * Time: 8:06 PM
 */

namespace App\Services\Traits;

use Spatie\QueryBuilder\Filter;
use App\Core\Constants;

trait WithNameSearch
{
    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function listFromAdmin()
    {
        return $this->builder
            ->allowedFilters([
                Filter::partial('name'),
                Filter::exact('status')
            ])
            ->paginate(Constants::PAGINATION_LIMIT);
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function listFromCommon()
    {
        return $this->builder
            ->where('status', '<>', Constants::STATUS_DELETED)
            ->allowedFilters([
                Filter::partial('name'),
            ])
            ->paginate(Constants::PAGINATION_LIMIT);
    }
}