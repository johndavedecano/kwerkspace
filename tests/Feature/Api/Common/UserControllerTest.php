<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/9/2018
 * Time: 3:53 PM
 */

namespace Tests\Feature\Api\Common;

use App\Entities\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testIndexSuccess()
    {
        $response = $this->get('api/common/users');

        $response->assertOk();
    }

    public function testShowSuccess()
    {
        $entity = factory(User::class)->create();

        $response = $this->get('api/common/users/'.$entity->id);

        $response->assertOk();
    }
}