<?php

namespace App\Entities;

use App\Entities\Relations\WithFeature;
use App\Entities\Relations\WithSkill;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserSkill
 *
 * @package App\Entities
 */
class UserSkill extends Model
{
    use WithSkill;

    /**
     * Database table name
     */
    protected $table = 'users_skills';

    /**
     * Protected columns from mass assignment
     */
    protected $guarded = ['id'];

    /**
     * Mass assignable columns
     */
    protected $fillable = ['post_id', 'skill_id', 'experience'];
}