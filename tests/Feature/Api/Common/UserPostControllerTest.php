<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/9/2018
 * Time: 3:53 PM
 */

namespace Tests\Feature\Api\Common;

use App\Entities\Post;
use App\Entities\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserPostControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testIndexSuccess()
    {
        $entity = factory(User::class)->create();

        $response = $this->get('api/common/users/'.$entity->id.'/posts');

        $response->assertOk();
    }

    public function testShowSuccess()
    {
        $entity = factory(User::class)->create();

        $relation = factory(Post::class)->create(['user_id' => $entity->id]);

        $response = $this->get('api/common/users/'.$entity->id.'/posts/'.$relation->id);

        $response->assertOk();
    }
}