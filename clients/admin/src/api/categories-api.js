// categories-api.js
import axios from 'axios'

export function load(params = {}) {
  return axios
    .get('/admin/categories', { params })
    .then(repsonse => repsonse.data)
}

export function show(id) {
  return axios.get(`/admin/categories/${id}`).then(repsonse => repsonse.data)
}

export function destroy(id) {
  return axios.delete(`/admin/categories/${id}`).then(repsonse => repsonse.data)
}

export function update(id, params = {}) {
  return axios
    .put(`/admin/categories/${id}`, params)
    .then(repsonse => repsonse.data)
}

export function create(params = {}) {
  return axios.post('/admin/categories', params).then(repsonse => repsonse.data)
}
