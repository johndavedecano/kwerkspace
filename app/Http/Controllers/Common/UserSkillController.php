<?php
/**
 * Created by QualityTrade.
 * User: johndavedecano
 * Date: 05/10/2018
 * Time: 2:49 PM
 */

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;
use App\Http\Resources\SkillResource;
use App\Services\User\UserSkillsCollection;
use App\Services\User\UserSkillsManager;

/**
 * Class UserSkillController
 *
 * @package App\Http\Controllers\Admin
 */
class UserSkillController extends Controller
{
    /**
     * @var \App\Services\User\UserSkillsManager
     */
    protected $manager;

    /**
     * @var \App\Services\User\UserSkillsCollection
     */
    protected $collection;

    /**
     * UserSkillController constructor.
     *
     * @param \App\Services\User\UserSkillsCollection $collection
     * @param \App\Services\User\UserSkillsManager $manager
     */
    public function __construct(UserSkillsCollection $collection, UserSkillsManager $manager)
    {
        $this->collection = $collection;

        $this->manager = $manager;
    }

    /**
     * @param $userId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index($userId)
    {
        $results = $this->collection->listUserSkills($userId);

        return SkillResource::collection($results);
    }


    public function show($userId, $skillId)
    {
        $results = $this->collection->findByUser($userId, $skillId);

        return new SkillResource($results);
    }
}