<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('posts')->truncate();
        \Illuminate\Support\Facades\DB::table('posts_likes')->truncate();
        \Illuminate\Support\Facades\DB::table('inquiries')->truncate();
        \Illuminate\Support\Facades\DB::table('inquiries_replies')->truncate();
        \Illuminate\Support\Facades\DB::table('posts_features')->truncate();
        \Illuminate\Support\Facades\DB::table('posts_images')->truncate();
        \Illuminate\Support\Facades\DB::table('reviews')->truncate();

        factory(\App\Entities\Post::class, 10)->create()->each(function ($post) {
            factory(\App\Entities\PostsFeature::class, 5)->create(['post_id' => $post->id ]);
            factory(\App\Entities\Review::class, 5)->create(['post_id' => $post->id]);
            factory(\App\Entities\PostsLike::class, 10)->create(['post_id' => $post->id]);
            factory(\App\Entities\Inquiry::class, 10)->create(['post_id' => $post->id]);
        });
    }
}
