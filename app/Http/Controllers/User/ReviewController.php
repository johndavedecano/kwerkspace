<?php

namespace App\Http\Controllers\User;

use App\Http\Resources\ReviewResource;
use App\Services\Review\ReviewCollection;
use App\Services\Review\ReviewManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class ReviewController
 *
 * @package App\Http\Controllers\Admin
 */
class ReviewController extends Controller
{
    /**
     * @var \App\Services\Review\ReviewCollection
     */
    protected $collection;

    /**
     * @var \App\Services\Review\ReviewManager
     */
    protected $manager;

    /**
     * ReviewController constructor.
     *
     * @param \App\Services\Review\ReviewManager $manager
     * @param \App\Services\Review\ReviewCollection $collection
     */
    public function __construct(ReviewManager $manager, ReviewCollection $collection)
    {
        $this->manager = $manager;

        $this->collection = $collection;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $results = $this->collection->listFromUser(auth()->id());

        return ReviewResource::collection($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\ReviewResource
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'post_id' => 'required|exists:posts,id',
            'title' => 'required',
            'description' => 'required',
            'rating' => 'required|numeric|min:1|max:5',
        ]);

        $request = array_merge($request->all(), ['user_id' => auth()->id()]);

        $results = $this->manager->createByUser($request);

        return new ReviewResource($results);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\ReviewResource
     */
    public function show($id)
    {
        $results = $this->collection->findByUser(auth()->id(), $id);

        $results->load('user');

        return new ReviewResource($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \App\Http\Resources\ReviewResource
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'sometimes|required',
            'description' => 'sometimes|required',
            'rating' => 'sometimes|required|numeric|min:1|max:5',
        ]);

        $request = array_merge($request->all(), ['user_id' => auth()->id()]);

        $this->manager->updateByUser(auth()->id(), $id, $request);

        return $this->show($id);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\ReviewResource
     */
    public function destroy($id)
    {
        $this->manager->deleteByUser(auth()->id(), $id);

        return response()->json(['id' => $id]);
    }
}
