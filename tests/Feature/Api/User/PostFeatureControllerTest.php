<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/6/2018
 * Time: 7:46 PM
 */

namespace Tests\Feature\Api\User;

use App\Entities\Feature;
use App\Entities\Post;
use App\Entities\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\Feature\Api\Authentication;
use Tests\TestCase;

/**'
 * Class PostControllerTest
 *
 * @package Tests\Feature\Api\Admin
 */
class PostFeatureControllerTest extends TestCase
{
    use RefreshDatabase, Authentication;

    public function setUp()
    {
        parent::setUp();

        $this->user = $this->getAuthHeaders(User::ROLE_USER);
    }

    public function testIndexFails()
    {
        $response = $this->withHeaders(['Accept' => 'application/json'])->get('/api/user/posts/1/features');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testIndexSuccess()
    {
        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $response = $this->withHeaders($this->user)->get('/api/user/posts/'.$post->id.'/features');

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testStoreFails()
    {
        $response = $this->withHeaders([])->post('/api/user/posts/346363/features');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testStoreFailsNotFound()
    {
        $data = ['features' => [factory(Feature::class)->create()->id]];

        $response = $this->withHeaders($this->user)->post('/api/user/posts/112323522/features', $data);

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testStoreValidationFails()
    {
        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $response = $this->withHeaders($this->user)->post('/api/user/posts/'.$post->id.'/features', []);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStoreSuccess()
    {
        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $data = ['features' => [factory(Feature::class)->create()->id]];

        $response = $this->withHeaders($this->user)->post('/api/user/posts/'.$post->id.'/features', $data);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testUpdateFails()
    {
        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $feature = factory(Feature::class)->create();

        $response = $this->withHeaders([])->put('/api/user/posts/'.$post->id.'/features/'.$feature->id, []);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testUpdateFailsNotFound()
    {
        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $response = $this->withHeaders($this->user)->put('/api/user/posts/'.$post->id.'/features/2353252');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testUpdateSuccess()
    {
        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $feature = factory(Feature::class)->create();

        $data = [$feature->id];

        $response = $this->withHeaders($this->user)->put('/api/user/posts/'.$post->id.'/features/'.$feature->id, $data);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testDestroyFails()
    {
        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $feature = factory(Feature::class)->create();

        $response = $this->withHeaders([])->delete('/api/user/posts/'.$post->id.'/features/'.$feature->id);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testDestroyFailsNotFound()
    {
        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $response = $this->withHeaders($this->user)->delete('/api/user/posts/'.$post->id.'/features/23623623623632');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testDestroySuccess()
    {
        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $feature = factory(Feature::class)->create();

        $response = $this->withHeaders($this->user)->delete('/api/user/posts/'.$post->id.'/features/'.$feature->id);

        $response->assertStatus(Response::HTTP_OK);
    }
}