<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInquiriesTable extends Migration {

	public function up()
	{
		Schema::create('inquiries', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('post_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->string('email');
			$table->string('name');
			$table->text('description');
			$table->dateTime('viewing_date')->nullable();
			$table->string('contact_number');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('inquiries');
	}
}