<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/5/2018
 * Time: 6:25 PM
 */

namespace App\Services\User;

use App\Core\Abstracts\AbstractManager;
use App\Core\Constants;
use App\Entities\Skill;
use App\Entities\User;
use App\Entities\UserSkill;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class UserSkillsManager
 *
 * @package App\Services\User
 */
class UserSkillsManager extends AbstractManager
{
    /**
     * @var \App\Entities\User
     */
    public $model;

    /**
     * @var \App\Entities\Skill
     */
    protected $skill;

    /**
     * UserSkillsManager constructor.
     *
     * @param \App\Entities\User $model
     * @param \App\Entities\Skill $skill
     */
    public function __construct(User $model, Skill $skill)
    {
        $this->model = $model;

        $this->skill = $skill;
    }

    /**
     * @param array $skills
     */
    public function check($skills = [])
    {
        foreach ($skills as $skill) {
            $this->skill->findOrFail($skill['skill_id']);
        }
    }

    /**
     * @param $userId
     * @param array $skills
     * @return mixed
     */
    public function fill($userId, $skills = [])
    {
        $this->check($skills);

        $builder = $this->model->where('id', $userId)->firstOrFail()->skills();

        $total = $builder->count() + count($skills);

        if ($total >= Constants::USER_SKILLS_LIMIT) {
            throw new HttpException(400, 'You have reached maximum of '.Constants::USER_SKILLS_LIMIT.' limit');
        }

        $lists = [];

        foreach ($skills as $skill) {
            $lists[] = new UserSkill(array_only($skill, ['skill_id', 'experience']));
        }

        return $builder->saveMany($lists);
    }

    /**
     * @param $userId
     * @param $skill
     * @return mixed
     */
    public function attach($userId, $skill)
    {
        $this->skill->findOrFail($skill['skill_id']);

        $builder = $this->model->where('id', $userId)->firstOrFail()->skills();

        $total = $builder->count() + 1;

        if ($total >= Constants::USER_SKILLS_LIMIT) {
            throw new HttpException(400, 'You have reached maximum of '.Constants::USER_SKILLS_LIMIT.' limit');
        }

        return $builder->save(new UserSkill($skill));
    }

    /**
     * @param $userId
     * @param $skillId
     * @return mixed
     */
    public function detach($userId, $skillId)
    {
        return $this->model->where('id', $userId)->firstOrFail()->skills()->where('id', $skillId)->firstOrFail()->delete();
    }
}