<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/5/2018
 * Time: 6:25 PM
 */

namespace App\Services\User;

use App\Core\Abstracts\AbstractCollection;
use App\Entities\User;
use App\Services\Traits\WithFindSlug;
use Spatie\QueryBuilder\QueryBuilder;

class UserSkillsCollection extends AbstractCollection
{
    use WithFindSlug;

    /**
     * UserSkillsCollection constructor.
     *
     * @param \App\Entities\User $model
     */
    public function __construct(User $model)
    {
        $this->builder = QueryBuilder::for(User::class);

        $this->model = $model;
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function listUserSkills($userId)
    {
        $user = $this->model->findOrFail($userId);

        return $user->skills()->with('skill')->get();
    }

    /**
     * @param $userId
     * @param $skillId
     * @return mixed
     */
    public function findByUser($userId, $skillId)
    {
        return $this->model
            ->findOrFail($userId)
            ->skills()
            ->with('skill')
            ->where('skill_id', $skillId)
            ->firstOrFail();
    }
}