<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Entities\Review::class, function (Faker $faker) {
    return [
        'title' => $faker->text(100),
        'description' => $faker->paragraph,
        'rating' => $faker->numberBetween(1, 5),
        'post_id' => function () {
            return factory(\App\Entities\Post::class)->create()->id;
        },
        'user_id' => function () {
            return factory(\App\Entities\User::class)->create()->id;
        },
    ];
});
