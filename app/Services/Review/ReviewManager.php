<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:21 PM
 */

namespace App\Services\Review;

use App\Core\Abstracts\AbstractManager;
use App\Entities\Review;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ReviewManager extends AbstractManager
{
    /**
     * @var \App\Entities\Review
     */
    public $model;

    /**
     * ReviewManager constructor.
     *
     * @param \App\Entities\Review $model
     */
    public function __construct(Review $model)
    {
        $this->model = $model;
    }

    /**
     * @param $userId
     * @param $postId
     * @return bool
     */
    public function isAlreadyReviewed($userId, $postId)
    {
        return $this->model->where(['user_id' => $userId, 'post_id' => $postId])->count() > 0;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function createByUser($data = [])
    {
        if ($this->isAlreadyReviewed($data['user_id'], $data['post_id'])) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, 'You already reviewed this post');
        }

        return $this->model->create(array_only($data, ['title', 'post_id','user_id', 'description', 'rating']));
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create($data = [])
    {
        return $this->model->create(array_only($data, $this->model->getFillable()));
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, $data = [])
    {
        return $this->model->findOrFail($id)->update(array_only($data, $this->model->getFillable()));
    }

    /**
     * @param $userId
     * @param $reviewId
     * @param array $data
     * @return mixed
     */
    public function updateByUser($userId, $reviewId, $data = [])
    {
        $review = $this->model->where(['user_id' => $userId, 'id' => $reviewId])->firstOrFail();

        return $review->update(array_only($data, ['title', 'description', 'rating']));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model->findOrFail($id)->delete();
    }

    /**
     * @param $userId
     * @param $reviewId
     * @param array $data
     * @return mixed
     */
    public function deleteByUser($userId, $reviewId, $data = [])
    {
        $review = $this->model->where(['user_id' => $userId, 'id' => $reviewId])->firstOrFail();

        return $review->update(array_only($data, ['title', 'description', 'rating']));
    }
}