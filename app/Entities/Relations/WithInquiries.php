<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 12:23 PM
 */

namespace App\Entities\Relations;

use App\Entities\Inquiry;

/**
 * Trait WithInquiries
 *
 * @package App\Entities\Relations
 */
trait WithInquiries
{
    /**
     * inquiries
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function inquiries()
    {
        return $this->hasMany(Inquiry::class, 'post_id');
    }
}