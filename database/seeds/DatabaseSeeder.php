<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("SET foreign_key_checks=0");

        Model::unguard();

        $this->call([
            UsersTableSeeder::class,
            CategoriesTableSeeder::class,
            CountriesTableSeeder::class,
            FeaturesTableSeeder::class,
            SkillsTableSeeder::class,
            PostsTableSeeder::class,
        ]);

        Model::reguard();

        DB::statement("SET foreign_key_checks=1");
    }
}
