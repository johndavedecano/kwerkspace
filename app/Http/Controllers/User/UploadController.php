<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/1/2018
 * Time: 1:53 AM
 */

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\UploadResource;
use App\Services\Upload\UploadManager;
use Illuminate\Http\Request;

/**
 * Class UploadController
 *
 * @package App\Http\Controllers\Admin
 */
class UploadController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\UploadResource
     */
    public function upload(Request $request)
    {
        $this->validate($request, ['file' => 'required|mimes:jpeg,png|max:5120']);

        $results = UploadManager::upload($request->file('file'));

        return new UploadResource($results);
    }
}