<?php

namespace App\Notifications;

use App\Entities\Inquiry;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class PostInquiryCreated extends Notification
{
    use Queueable;

    /**
     * @var \App\Entities\Inquiry
     */
    protected $inquiry;

    /**
     * PostInquiryCreated constructor.
     *
     * @param \App\Entities\Inquiry $inquiry
     */
    public function __construct(Inquiry $inquiry)
    {
        $this->inquiry = $inquiry;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $name = $this->inquiry->post->user->name;

        return (new MailMessage)
            ->greeting('Hi '.$name)
            ->subject('Inquiry from: '.$this->inquiry->name)
            ->line($this->inquiry->description)->line('Name: '.$this->inquiry->name)
            ->line('Email: '.$this->inquiry->email)
            ->line('Contact Number: '.$this->inquiry->contact_number)
            ->line('Viewing Date: '.$this->inquiry->viewing_date)
            ->line('Post Title: '.$this->inquiry->post->title)
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'inquiry_id' => $this->inquiry->id,
            'post_id' => $this->inquiry->post_id,
            'post_title' => $this->inquiry->post->title,
            'name' => $this->inquiry->name,
            'email' => $this->inquiry->email,
            'title' => $this->inquiry->title,
            'description' => $this->inquiry->description,
        ];
    }
}
