<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/9/2018
 * Time: 3:53 PM
 */

namespace Tests\Post\Api\Common;

use App\Entities\Feature;
use App\Entities\Post;
use App\Entities\PostsFeature;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PostFeatureControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testIndexSuccess()
    {
        $result = factory(Post::class)->create();

        $response = $this->get('api/common/posts/'.$result->id.'/features');

        $response->assertOk();
    }

    public function testShowSuccess()
    {
        $result = factory(Post::class)->create();

        $feature = factory(Feature::class)->create();

        $relation = factory(PostsFeature::class)->create(['post_id' => $result->id, 'feature_id' => $feature->id]);

        $response = $this->get('api/common/posts/'.$result->id.'/features/'.$relation->feature_id);

        $response->assertOk();
    }
}