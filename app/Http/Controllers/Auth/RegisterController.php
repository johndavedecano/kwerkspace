<?php

namespace App\Http\Controllers\Auth;

use App\Services\User\UserAuthManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class RegisterController
 *
 * @package App\Http\Controllers\Auth
 */
class RegisterController extends Controller
{
    /**
     * @var \App\Services\User\UserAuthManager
     */
    protected $manager;

    /**
     * RegisterController constructor.
     *
     * @param \App\Services\User\UserAuthManager $manager
     */
    public function __construct(UserAuthManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6|max:16|confirmed',
            'password_confirmation' => 'required',
        ]);

        $data = $request->only(['email', 'name', 'password']);

        $this->manager->register($data);

        return response()->json(['data' => $data]);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request)
    {
        $this->validate($request, ['token' => 'required']);

        $results = $this->manager->verify($request->get('token'));

        return response()->json(['data' => $results]);
    }
}
