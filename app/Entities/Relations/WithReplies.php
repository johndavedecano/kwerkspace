<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 12:26 PM
 */

namespace App\Entities\Relations;

use App\Entities\InquiriesReply;

/**
 * Trait WithReplies
 *
 * @package App\Entities\Relations
 */
trait WithReplies
{
    /**
     * inquiriesReplies
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function replies()
    {
        return $this->hasMany(InquiriesReply::class, 'post_id');
    }
}