<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 12:24 PM
 */

namespace App\Entities\Relations;

use App\Entities\Feature;

/**
 * Trait WithFeatures
 *
 * @package App\Entities\Relations
 */
trait WithFeatures
{
    /**
     * @return mixed
     */
    public function features()
    {
        return $this->belongsToMany(Feature::class, 'posts_features');
    }
}