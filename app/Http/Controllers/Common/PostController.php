<?php

namespace App\Http\Controllers\Common;

use App\Http\Resources\PostResource;
use App\Services\Post\PostCollection;
use App\Http\Controllers\Controller;

/**
 * Class PostController
 *
 * @package App\Http\Controllers\Common
 */
class PostController extends Controller
{
    /**
     * @var \App\Services\Post\PostCollection
     */
    public $collection;

    /**
     * PostController constructor.
     *
     * @param \App\Services\Post\PostCollection $collection
     */
    public function __construct(PostCollection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $collection = $this->collection->list();

        return PostResource::collection($collection);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\PostResource
     */
    public function show($id)
    {
        $results = $this->collection->findFromCommon($id);

        return new PostResource($results);
    }
}
