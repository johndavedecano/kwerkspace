<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:21 PM
 */

namespace App\Services\Inquiry;

use App\Core\Abstracts\AbstractManager;
use App\Entities\InquiriesReply;
use App\Entities\Inquiry;
use App\Entities\Post;
use Illuminate\Auth\Access\AuthorizationException;

/**
 * Class InquiryReplyManager
 *
 * @package App\Services\Inquiry
 */
class InquiryReplyManager extends AbstractManager
{
    /**
     * @var \App\Entities\Inquiry
     */
    public $model;

    /**
     * @var \App\Entities\Inquiry
     */
    protected $inquiry;

    /**
     * @var \App\Entities\Post
     */
    protected $post;

    /**
     * InquiryReplyManager constructor.
     *
     * @param \App\Entities\Inquiry $inquiry
     * @param \App\Entities\InquiriesReply $model
     * @param \App\Entities\Post $post
     */
    public function __construct(Inquiry $inquiry, InquiriesReply $model, Post $post)
    {
        $this->model = $model;

        $this->inquiry = $inquiry;

        $this->post = $post;
    }

    /**
     * @param $userId
     * @param $inquiryId
     * @param array $data
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function createByUser($userId, $inquiryId, $data = [])
    {
        $inquiry = $this->inquiry->findOrFail($inquiryId);

        $post = $this->post->findOrFail($inquiry->post_id);

        $this->belongs($userId, $post, $inquiry);

        return $this->model->create([
            'user_id'    => $userId,
            'inquiry_id' => $inquiryId,
            'post_id'    => $inquiry->post_id,
            'message'    => $data['message']
        ]);
    }

    /**
     * @param $userId
     * @param \App\Entities\Post $post
     * @param \App\Entities\Inquiry $inquiry
     * @return bool
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function belongs($userId, Post $post, Inquiry $inquiry)
    {
        if (intval($post->user_id) === intval($userId) || intval($inquiry->user_id) === intval($userId)) {
            return true;
        }

        throw new AuthorizationException('You are not allowed to post this message');
    }


    /**
     * @param $inquiryId
     * @param array $data
     * @return mixed
     */
    public function create($inquiryId, $data = [])
    {
        $inquiry = $this->inquiry->findOrFail($inquiryId);

        return $this->model->create([
           'user_id'    => $data['user_id'],
           'inquiry_id' => $inquiryId,
           'post_id'    => $inquiry->post_id,
           'message'    => $data['message']
        ]);
    }

    /**
     * @param $userId
     * @param $inquiryId
     * @param $replyId
     * @param array $data
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function updateByUser($userId, $inquiryId, $replyId, $data = [])
    {
        $inquiry = $this->inquiry->findOrFail($inquiryId);

        $post = $this->post->findOrFail($inquiry->post_id);

        $this->belongs($userId, $post, $inquiry);

        $reply = $this->model->where(['user_id' => $userId, 'id' => $replyId])->firstOrFail();

        return $reply->update(array_only($data, $this->model->getFillable()));
    }


    /**
     * @param $inquiryId
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($inquiryId, $id, $data = [])
    {
        $reply = $this->inquiry->findOrFail($inquiryId)->replies()->findOrFail($id);

        return $reply->update(array_only($data, $this->model->getFillable()));
    }

    /**
     * @param $inquiryId
     * @param $id
     * @return mixed
     */
    public function delete($inquiryId, $id)
    {
        return $this->inquiry->findOrFail($inquiryId)->replies()->findOrFail($id)->delete();
    }

    /**
     * @param $userId
     * @param $inquiryId
     * @param $replyId
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function deleteByUser($userId, $inquiryId, $replyId)
    {
        $inquiry = $this->inquiry->findOrFail($inquiryId);

        $post = $this->post->findOrFail($inquiry->post_id);

        $this->belongs($userId, $post, $inquiry);

        $reply = $this->model->where(['user_id' => $userId, 'id' => $replyId])->firstOrFail();

        return $reply->delete();
    }
}