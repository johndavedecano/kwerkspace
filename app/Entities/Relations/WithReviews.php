<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 12:29 PM
 */

namespace App\Entities\Relations;

use App\Entities\Review;

/**
 * Trait WithReviews
 *
 * @package App\Entities\Relations
 */
trait WithReviews
{
    /**
     * @return mixed
     */
    public function reviews()
    {
        return $this->hasMany(Review::class, 'post_id');
    }
}