<?php
/**
 * Created by QualityTrade.
 * User: johndavedecano
 * Date: 05/10/2018
 * Time: 2:49 PM
 */

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\SkillResource;
use App\Services\User\UserSkillsCollection;
use App\Services\User\UserSkillsManager;
use Illuminate\Http\Request;

/**
 * Class UserSkillController
 *
 * @package App\Http\Controllers\Admin
 */
class UserSkillController extends Controller
{
    /**
     * @var \App\Services\User\UserSkillsManager
     */
    protected $manager;

    /**
     * @var \App\Services\User\UserSkillsCollection
     */
    protected $collection;

    /**
     * UserSkillController constructor.
     *
     * @param \App\Services\User\UserSkillsCollection $collection
     * @param \App\Services\User\UserSkillsManager $manager
     */
    public function __construct(UserSkillsCollection $collection, UserSkillsManager $manager)
    {
        $this->collection = $collection;

        $this->manager = $manager;
    }

    /**
     * @param $userId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index($userId)
    {
        $results = $this->collection->listUserSkills($userId);

        return SkillResource::collection($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $userId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function store(Request $request, $userId)
    {
        $this->validate($request, [
            'skills' => 'required|array',
            'skills.*.skill_id' => 'required|numeric|exists:skills,id',
            'skills.*.experience' => 'required|numeric',
        ]);

        $this->manager->fill($userId, $request->get('skills'));

        return $this->index($userId);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $userId
     * @param $skillId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function update(Request $request, $userId, $skillId)
    {
        $this->validate($request, [
            'experience' => 'required|numeric',
        ]);

        $this->manager->attach($userId, [
            'skill_id' => $skillId,
            'experience' => $request->get('experience', 0)
        ]);

        return $this->index($userId);
    }

    /**
     * @param $userId
     * @param $skillId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function destroy($userId, $skillId)
    {
        $this->manager->detach($userId, $skillId);

        return $this->index($userId);
    }
}