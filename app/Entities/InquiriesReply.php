<?php

namespace App\Entities;

use App\Entities\Relations\WithUser;
use Illuminate\Database\Eloquent\Model;

/**
 * Class InquiriesReply
 *
 * @package App\Entities
 */
class InquiriesReply extends Model
{
    use WithUser;

    /**
     * Database table name
     */
    protected $table = 'inquiries_replies';

    /**
     * Protected columns from mass assignment
     */
    protected $guarded = ['id'];

    /**
     * Mass assignable columns
     */
    protected $fillable = ['message', 'user_id', 'post_id', 'inquiry_id'];

    /**
     * Date time columns.
     */
    protected $dates = [];
}