<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:20 PM
 */

namespace App\Services\Inquiry;

use App\Core\Abstracts\AbstractCollection;
use App\Core\Constants;
use App\Core\Interfaces\CollectionInterface;
use App\Entities\InquiriesReply;
use App\Entities\Inquiry;
use App\Entities\Post;
use Illuminate\Auth\Access\AuthorizationException;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class InquiryReplyCollection
 *
 * @package App\Services\Inquiry
 */
class InquiryReplyCollection extends AbstractCollection implements CollectionInterface
{
    /**
     * @var \App\Entities\Inquiry
     */
    protected $inquiry;

    /**
     * @var \App\Entities\Post
     */
    protected $post;

    /**
     * InquiryReplyCollection constructor.
     *
     * @param \App\Entities\InquiriesReply $model
     * @param \App\Entities\Post $post
     * @param \App\Entities\Inquiry $inquiry
     */
    public function __construct(InquiriesReply $model, Post $post, Inquiry $inquiry)
    {
        $this->builder = QueryBuilder::for(Inquiry::class);

        $this->model = $model;

        $this->post = $post;

        $this->inquiry = $inquiry;
    }

    /**
     * @param $userId
     * @param \App\Entities\Post $post
     * @param \App\Entities\Inquiry $inquiry
     * @return bool
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function belongs($userId, Post $post, Inquiry $inquiry)
    {
        if (intval($post->user_id) === intval($userId) || intval($inquiry->user_id) === intval($userId)) {
            return true;
        }

        throw new AuthorizationException('You are not allowed to post this message');
    }

    /**
     * @param $userId
     * @param $inquiryId
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function listByUser($userId, $inquiryId)
    {
        $inquiry = $this->inquiry->findOrFail($inquiryId);

        $post = $this->post->findOrFail($inquiry->post_id);

        $this->belongs($userId, $post, $inquiry);

        return $this->builder
            ->with([
                'user' => function($query) {
                    return $query->select(['name', 'slug', 'id', 'avatar']);
                }
            ])
            ->where('inquiry_id', $inquiryId)
            ->paginate(Constants::PAGINATION_LIMIT);
    }

    /**
     * @param $userId
     * @param $inquiryId
     * @param $replyId
     * @return mixed
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function findByUser($userId, $inquiryId, $replyId)
    {
        $inquiry = $this->inquiry->findOrFail($inquiryId);

        $post = $this->post->findOrFail($inquiry->post_id);

        $this->belongs($userId, $post, $inquiry);

        return $this->model->findOrFail($replyId);
    }

    /**
     * @param $inquiryId
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function listByInquiry($inquiryId)
    {
        return $this->builder
            ->with([
                'user' => function($query) {
                    return $query->select(['name', 'slug', 'id', 'avatar']);
                }
            ])
            ->where('inquiry_id', $inquiryId)
            ->paginate(Constants::PAGINATION_LIMIT);
    }

    /**
     * @param $inquiryId
     * @param $replyId
     * @return mixed
     */
    public function findByInquiry($inquiryId, $replyId)
    {
        return $this->model->where('inquiry_id', $inquiryId)->where('id', $replyId)->firstOrFail();
    }
}