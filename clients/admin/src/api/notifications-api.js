// notifications-api.js
import axios from 'axios'

export function load(params = {}) {
  return axios
    .get('/admin/notifications', { params })
    .then(repsonse => repsonse.data)
}

export function show(id) {
  return axios.get(`/admin/notifications/${id}`).then(repsonse => repsonse.data)
}

export function destroy(id) {
  return axios
    .delete(`/admin/notifications/${id}`)
    .then(repsonse => repsonse.data)
}

export function update(id, params = {}) {
  return axios
    .put(`/admin/notifications/${id}`, { params })
    .then(repsonse => repsonse.data)
}

export function create(params = {}) {
  return axios
    .post('/admin/notifications', params)
    .then(repsonse => repsonse.data)
}
