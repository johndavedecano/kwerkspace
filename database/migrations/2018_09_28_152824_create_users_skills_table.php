<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersSkillsTable extends Migration {

	public function up()
	{
		Schema::create('users_skills', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('skill_id')->unsigned();
			$table->integer('experience')->default(1);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('users_skills');
	}
}