<?php

namespace App\Notifications;

use App\Entities\PostsLike;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PostLikeCreated extends Notification implements ShouldQueue
{
    use Queueable;

    protected $postLike;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(PostsLike $postsLike)
    {
        $this->postLike = $postsLike;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $post = $this->postLike->post;

        $user = $this->postLike->user;

        return [
            'post' => [
                'id' => $post->id,
                'title' => $post->title,
                'slug' => $post->slug,
            ],
            'user' => [
                'id' => $user->id,
                'name' => $user->name,
                'slug' => $user->slug,
                'avatar' => $user->avatar,
            ]
        ];
    }
}
