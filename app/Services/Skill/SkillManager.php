<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:20 PM
 */

namespace App\Services\Skill;

use App\Core\Abstracts\AbstractManager;
use App\Core\Constants;
use App\Entities\Skill;

class SkillManager extends AbstractManager
{
    /**
     * @var \App\Entities\Feature
     */
    public $model;

    /**
     * FeatureManager constructor.
     * @param Skill $model
     */
    public function __construct(Skill $model)
    {
        $this->model = $model;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function create($name)
    {
        return $this->model->create(['name' => $name]);
    }

    /**
     * @param $id
     * @param array $request
     * @return mixed
     */
    public function update($id, $request = [])
    {
        return $this->model->findOrFail($id)->update(array_only($request, $this->model->getFillable()));
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model->findOrFail($id)->update(['status' => Constants::STATUS_DELETED]);
    }
}