<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/6/2018
 * Time: 7:46 PM
 */

namespace Tests\Feature\Api\Admin;

use App\Core\Constants;
use App\Entities\Country;
use App\Entities\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\Feature\Api\Authentication;
use Tests\TestCase;

/**
 * Class UserControllerTest
 *
 * @package Tests\Feature\Api\Admin
 */
class UserControllerTest extends TestCase
{
    use RefreshDatabase, Authentication;

    public function setUp()
    {
        parent::setUp();

        $this->admin = $this->getAuthHeaders(User::ROLE_ADMIN);

        $this->user = $this->getAuthHeaders(User::ROLE_USER);
    }

    public function testIndexFails()
    {
        $response = $this->withHeaders(['Accept' => 'application/json'])->get('/api/admin/users');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testIndexFailsNotAllowed()
    {
        $response = $this->withHeaders($this->user)->get('/api/admin/users');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testIndexSuccess()
    {
        $response = $this->withHeaders($this->admin)->get('/api/admin/users');

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testStoreFails()
    {
        $response = $this->withHeaders(['Accept' => 'application/json'])->post('/api/admin/users');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testStoreValidationFails()
    {
        $response = $this->withHeaders($this->admin)->post('/api/admin/users');

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStoreSuccess()
    {
        $data = [
            'name' => 'required',
            'email' => 'johndave@test.com',
            'password' => 'password',
            'password_confirmation' => 'password',
            'country_id' => factory(Country::class)->create()->id,
            'status' => Constants::STATUS_INACTIVE,
            'role' => User::ROLE_USER,
        ];

        $response = $this->withHeaders($this->admin)->post('/api/admin/users', $data);

        $response->assertStatus(Response::HTTP_CREATED);
    }

    public function testUpdateFails()
    {
        $results = factory(User::class)->create();

        $response = $this->withHeaders(['Accept' => 'application/json'])->put('/api/admin/users/'.$results->id);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testUpdateFailsValidation()
    {
        $results = factory(User::class)->create();

        $response = $this->withHeaders($this->admin)->put('/api/admin/users/'.$results->id, ['name' => '']);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdateSuccess()
    {
        $results = factory(User::class)->create();

        $response = $this->withHeaders($this->admin)->put('/api/admin/users/'.$results->id, ['name' => 'test']);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testDestroyFails()
    {
        $results = factory(User::class)->create();

        $response = $this->withHeaders(['Accept' => 'application/json'])->delete('/api/admin/users/'.$results->id);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testDestroyFailsNotFound()
    {
        $response = $this->withHeaders($this->admin)->delete('/api/admin/users/3496878963');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testDestroySuccess()
    {
        $results = factory(User::class)->create();

        $response = $this->withHeaders($this->admin)->delete('/api/admin/users/'.$results->id);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testShowFails()
    {
        $results = factory(User::class)->create();

        $response = $this->withHeaders(['Accept' => 'application/json'])->get('/api/admin/users/'.$results->id);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testShowFailsNotFound()
    {
        $response = $this->withHeaders($this->admin)->get('/api/admin/users/3496878963');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testShowSuccess()
    {
        $results = factory(User::class)->create();

        $response = $this->withHeaders($this->admin)->get('/api/admin/users/'.$results->id);

        $response->assertStatus(Response::HTTP_OK);
    }
}