<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:21 PM
 */

namespace App\Services\Inquiry;

use App\Core\Abstracts\AbstractManager;
use App\Entities\Inquiry;
use App\Entities\Post;
use App\Notifications\PostInquiryCreated;
use Illuminate\Http\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class InquiryManager extends AbstractManager
{
    /**
     * @var \App\Entities\Inquiry
     */
    public $model;

    /**
     * @var \App\Entities\Post
     */
    public $post;

    /**
     * InquiryManager constructor.
     *
     * @param \App\Entities\Inquiry $model
     * @param \App\Entities\Post $post
     */
    public function __construct(Inquiry $model, Post $post)
    {
        $this->model = $model;

        $this->post = $post;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create($data = [])
    {
        $post = $this->post->findOrFail($data['post_id']);

        if ($post->user_id === $data['user_id']) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, 'You cannot create inquiry at your own post.');
        }

        $inquiry = $this->model->create($data);

        $post->user->notify(new PostInquiryCreated($inquiry));

        return $inquiry;
    }

    /**
     * @param $userId
     * @param $inquiryId
     * @param array $data
     * @return mixed
     */
    public function updateByUser($userId, $inquiryId, $data = [])
    {
        return $this->model->where(['user_id' => $userId, 'id' => $inquiryId])->firstOrFail()->update($data);
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, $data = [])
    {
        return $this->model->findOrFail($id)->update($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model->findOrFail($id)->delete();
    }

    /**
     * @param $userId
     * @param $inquiryId
     * @return mixed
     */
    public function deleteByUser($userId, $inquiryId)
    {
        return $this->model->where(['user_id' => $userId, 'id' => $inquiryId])->firstOrFail()->delete();
    }

}