<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Entities\UserSkill::class, function (Faker $faker) {
    return [
        'skill_id' => function () {
            return factory(\App\Entities\Skill::class)->create()->id;
        },
        'user_id' => function () {
            return factory(\App\Entities\User::class)->create()->id;
        },
        'experience' => $faker->numberBetween(1,10)
    ];
});
