<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/9/2018
 * Time: 3:53 PM
 */

namespace Tests\Post\Api\Common;

use App\Entities\Post;
use App\Entities\PostsLike;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PostLikeControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testIndexSuccess()
    {
        $result = factory(Post::class)->create();

        factory(PostsLike::class, 20)->create(['post_id' => $result->id]);

        $response = $this->get('api/common/posts/'.$result->id.'/likes');

        $response->assertOk();
    }

    public function testShowSuccess()
    {
        $result = factory(Post::class)->create();

        $relation = factory(PostsLike::class)->create(['post_id' => $result->id]);

        $response = $this->get('api/common/posts/'.$result->id.'/likes/'.$relation->user_id);

        $response->assertOk();
    }
}