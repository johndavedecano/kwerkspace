// posts-likes-api.js
import axios from 'axios'

export function load(postId, params = {}) {
  return axios
    .get(`/admin/posts/${postId}/likes`, { params })
    .then(repsonse => repsonse.data)
}

export function show(postId, likeId) {
  return axios
    .get(`/admin/posts/${postId}/likes/${likeId}`)
    .then(repsonse => repsonse.data)
}

export function destroy(postId, likeId) {
  return axios
    .delete(`/admin/posts/${postId}/likes/${likeId}`)
    .then(repsonse => repsonse.data)
}

export function update(postId, likeId, params = {}) {
  return axios
    .put(`/admin/posts/${postId}/likes/${likeId}`, { params })
    .then(repsonse => repsonse.data)
}

export function create(postId, params = {}) {
  return axios
    .post(`/admin/posts/${postId}/likes`, params)
    .then(repsonse => repsonse.data)
}
