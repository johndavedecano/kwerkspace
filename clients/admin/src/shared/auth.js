// user.js
import axios from 'axios'

export const user = function() {
  try {
    return JSON.parse(localStorage.getItem('user'))
  } catch (err) {
    console.error('error is logged in')
    return null
  }
}

export const token = function() {
  try {
    return localStorage.getItem('token')
  } catch (err) {
    console.error('error is logged in')
    return null
  }
}

export const initToken = function({ user, token }) {
  localStorage.setItem('user', JSON.stringify(user))
  localStorage.setItem('token', token)
  axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
}

export const removeToken = function() {
  localStorage.removeItem('user')
  localStorage.removeItem('token')
}
