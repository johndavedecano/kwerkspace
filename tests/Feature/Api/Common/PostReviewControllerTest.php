<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/9/2018
 * Time: 3:53 PM
 */

namespace Tests\Post\Api\Common;

use App\Entities\Post;
use App\Entities\PostsImage;
use App\Entities\Review;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PostReviewControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testIndexSuccess()
    {
        $result = factory(Post::class)->create();

        $response = $this->get('api/common/posts/'.$result->id.'/reviews');

        $response->assertOk();
    }

    public function testShowSuccess()
    {
        $result = factory(Post::class)->create();

        $postImage = factory(Review::class)->create(['post_id' => $result->id]);

        $response = $this->get('api/common/posts/'.$result->id.'/reviews/'.$postImage->id);

        $response->assertOk();
    }
}