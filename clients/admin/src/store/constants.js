// constants.js
export const LOGIN = 'login'
export const LOGIN_FAILURE = 'login.failure'
export const LOGIN_SUCCESS = 'login.success'
export const LOGOUT = 'logout'

export const CATEGORY_LOAD = 'category.load'
export const CATEGORY_LOAD_SUCCESS = 'category.load.success'
export const CATEGORY_LOAD_FAILURE = 'category.load.failure'

export const FEATURE_LOAD = 'feature.load'
export const FEATURE_LOAD_SUCCESS = 'feature.load.success'
export const FEATURE_LOAD_FAILURE = 'feature.load.failure'

export const SKILL_LOAD = 'skill.load'
export const SKILL_LOAD_SUCCESS = 'skill.load.success'
export const SKILL_LOAD_FAILURE = 'skill.load.failure'

export const COUNTRY_LOAD = 'country.load'
export const COUNTRY_LOAD_SUCCESS = 'country.load.success'
export const COUNTRY_LOAD_FAILURE = 'country.load.failure'

export const POST_LOAD = 'post.load'
export const POST_LOAD_SUCCESS = 'post.load.success'
export const POST_LOAD_FAILURE = 'post.load.failure'

export const USER_LOAD = 'user.load'
export const USER_LOAD_SUCCESS = 'user.load.success'
export const USER_LOAD_FAILURE = 'user.load.failure'
