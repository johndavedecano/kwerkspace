// features module
import { load, show, destroy, update, create } from '@/api/features-api'
import { notify } from '@/shared/notify'
import merge from 'lodash/merge'

import {
  FEATURE_LOAD,
  FEATURE_LOAD_SUCCESS,
  FEATURE_LOAD_FAILURE,
} from '@/store/constants'

const features = {
  namespaced: true,
  state: {
    items: [],
    meta: {},
    isLoading: false,
    isLoaded: false,
  },
  actions: {
    async loadFeature({ commit }, params = {}) {
      try {
        commit(FEATURE_LOAD, params)
        const { data, meta } = await load(params)
        commit(FEATURE_LOAD_SUCCESS, { data, meta })
      } catch (err) {
        commit(FEATURE_LOAD_FAILURE)
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
    async showFeature(_, id) {
      try {
        const { data } = await show(id)
        return data
      } catch (err) {
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
    async destroyFeature(_, id) {
      try {
        const { data } = await destroy(id)
        notify({ type: 'success', text: 'Succesfully Deleted' })
        return data
      } catch (err) {
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
    async updateFeature(_, { id, params }) {
      try {
        const { data } = await update(id, params)
        notify({ type: 'success', text: 'Succesfully Updated' })
        return data
      } catch (err) {
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
    async createFeature(_, params) {
      try {
        const { data } = await create(params)
        notify({ type: 'success', text: 'Succesfully Created' })
        return data
      } catch (err) {
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
  },
  mutations: {
    [FEATURE_LOAD]: (state, params) => {
      state.isLoading = true
      state.meta = merge(state.meta, params)
    },
    [FEATURE_LOAD_SUCCESS]: (state, { meta, data }) => {
      state.isLoading = false
      state.isLoaded = true
      state.items = data
      state.meta = merge(state.meta, meta)
    },
    [FEATURE_LOAD_FAILURE]: state => {
      state.isLoading = false
      state.isLoaded = false
      state.items = []
      state.meta = {}
    },
  },
  getters: {
    isLoaded: state => state.isLoaded,
    isLoading: state => state.isLoading,
    items: state => state.items,
    meta: state => state.meta,
  },
}

export default features
