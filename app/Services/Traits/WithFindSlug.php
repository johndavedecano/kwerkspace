<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 4:17 PM
 */

namespace App\Services\Traits;

/**
 * Trait WithFindSlug
 *
 * @package App\Services\Traits
 */
trait WithFindSlug
{
    /**
     * Find model by slug
     *
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug)
    {
        return $this->model->where('slug', $slug)->firstOrFail();
    }
}