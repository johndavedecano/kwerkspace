<?php
/**
 * Created by QualityTrade.
 * User: johndavedecano
 * Date: 04/10/2018
 * Time: 7:28 PM
 */

namespace App\Http\Controllers;

/**
 * Trait WithCurrentUser
 *
 * @package App\Http\Controllers
 */
trait WithCurrentUser
{
    public function user()
    {
        return auth()->guard()->user();
    }
}