<?php

namespace App\Http\Controllers\User;

use App\Core\Constants;
use App\Http\Resources\PostResource;
use App\Services\Post\PostCollection;
use App\Services\Post\PostManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class PostController
 *
 * @package App\Http\Controllers\User
 */
class PostController extends Controller
{
    /**
     * @var \App\Services\Post\PostCollection
     */
    protected $collection;

    /**
     * @var \App\Services\Post\PostManager
     */
    protected $manager;

    /**
     * PostController constructor.
     *
     * @param \App\Services\Post\PostCollection $collection
     * @param \App\Services\Post\PostManager $manager
     */
    public function __construct(PostCollection $collection, PostManager $manager)
    {
        $this->collection = $collection;

        $this->manager = $manager;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $results = $this->collection->listByUser(auth()->id());

        return PostResource::collection($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\PostResource
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required|exists:categories,id',
            'title' => 'required',
            'description' => 'required',
            'thumbnail' => 'required',
            'images' => 'required|array',
            'features' => 'required|array',
            'cover' => 'required',
            'area' => 'numeric',
            'max_occupants' => 'numeric|min:1',
            'payment_type' => 'required|in:monthly,daily,hourly',
            'payment_amount' => 'required|numeric|min:1',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zipcode' => 'required',
            'lon' => 'required',
            'lat' => 'required',
        ]);

        $request = $request->all();

        $request['country_id'] = Constants::DEFAULT_COUNTRY;

        $request['user_id'] = auth()->id();

        $request['status'] = Constants::STATUS_INACTIVE;

        $results = $this->manager->create($request);

        return new PostResource($results);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\PostResource
     */
    public function show($id)
    {
        $results = $this->collection->findPostByUser(auth()->id(), $id);

        return new PostResource($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \App\Http\Resources\PostResource
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'category_id' => 'sometimes|required|exists:categories,id',
            'title' => 'sometimes|required|string|min:60|max:140',
            'description' => 'sometimes|required|string|min:140|max:2000',
            'thumbnail' => 'sometimes|required',
            'images' => 'sometimes|required|array',
            'features' => 'sometimes|required|array',
            'cover' => 'sometimes|required|string',
            'area' => 'sometimes|required|numeric',
            'max_occupants' => 'sometimes|required|numeric|min:1',
            'payment_type' => 'sometimes|required|in:monthly,daily,hourly',
            'payment_amount' => 'sometimes|required|numeric|min:1',
            'address' => 'sometimes|required|max:100',
            'city' => 'sometimes|required|string',
            'state' => 'sometimes|required|string',
            'zipcode' => 'sometimes|required|string',
            'lon' => 'sometimes|required|numeric',
            'lat' => 'sometimes|required|required',
        ]);

        $request = $request->all();

        $request['country_id'] = Constants::DEFAULT_COUNTRY;

        $request['user_id'] = auth()->id();

        $request['status'] = Constants::STATUS_INACTIVE;

        $results = $this->manager->update($id, $request);

        return new PostResource($results);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\PostResource
     */
    public function destroy($id)
    {
        $this->manager->deleteByUser(auth()->id(), $id);

        return response()->json(['id' => $id]);
    }
}
