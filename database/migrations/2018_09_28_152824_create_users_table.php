<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
            $table->string('name')->nullable();
            $table->string('title')->nullable();
            $table->string('company')->nullable();
            $table->text('company_logo')->nullable();
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
			$table->integer('country_id')->unsigned()->nullable();
			$table->integer('experience')->default(0);
			$table->string('status')->default('inactive');
			$table->string('role')->default('user');
			$table->string('address')->default('');
			$table->string('city')->default('');
			$table->string('state')->default('');
			$table->decimal('lat', 9, 6)->nullable();
			$table->decimal('lon', 9, 6)->nullable();
			$table->text('avatar')->nullable();
			$table->string('zipcode')->default('');
			$table->string('slug')->unique();
			$table->string('linkedin')->nullable();
			$table->string('twitter')->nullable();
			$table->string('facebook')->nullable();
			$table->string('website')->nullable();
			$table->string('youtube')->nullable();
			$table->text('about')->nullable();
			$table->boolean('is_public')->default(false);
			$table->timestamp('last_login_at')->nullable();
            $table->timestamps();
            $table->rememberToken();
		});
	}

	public function down()
	{
		Schema::drop('users');
	}
}