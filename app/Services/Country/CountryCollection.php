<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:20 PM
 */

namespace App\Services\Country;

use App\Core\Abstracts\AbstractCollection;
use App\Core\Interfaces\CollectionInterface;
use App\Entities\Country;
use App\Services\Traits\WithFindSlug;
use App\Services\Traits\WithNameSearch;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class CountryCollection
 *
 * @package App\Services\Country
 */
class CountryCollection extends AbstractCollection implements CollectionInterface
{
    use WithFindSlug, WithNameSearch;

    /**
     * CountryCollection constructor.
     *
     * @param \App\Entities\Country $model
     */
    public function __construct(Country $model)
    {
        $this->builder = QueryBuilder::for(Country::class);

        $this->model = $model;
    }
}