<?php

namespace App\Entities;

use App\Entities\Others\ForgotPasswordEmail;
use App\Entities\Others\MustVerifyEmail;
use App\Entities\Others\WithSlug;
use App\Entities\Relations\WithCountry;
use App\Entities\Relations\WithInquiries;
use App\Entities\Relations\WithReplies;
use App\Entities\Relations\WithPost;
use App\Entities\Relations\WithPostLikes;
use App\Entities\Relations\WithReviews;

use App\Entities\Relations\WithSkills;
use App\Entities\Scopes\ScopeActive;
use App\Entities\Scopes\ScopeDeleted;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class User
 *
 * @package App\Entities
 */
class User extends Authenticable implements JWTSubject
{
    use Notifiable;

    use WithCountry, WithInquiries, WithReplies, WithPost, WithPostLikes, WithReviews, WithSkills;

    use ScopeDeleted, ScopeActive;

    use ForgotPasswordEmail, MustVerifyEmail, WithSlug;

    const ROLE_ADMIN = 'admin';

    const ROLE_USER = 'user';

    const ROLE_COMPANY = 'company';

    /**
     * Database table name
     */
    protected $table = 'users';

    /**
     * Protected columns from mass assignment
     */
    protected $guarded = ['id'];

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'name',
        'company',
        'company_logo',
        'email',
        'email_verified_at',
        'password',
        'country_id',
        'status',
        'role',
        'address',
        'city',
        'state',
        'lat',
        'lon',
        'avatar',
        'zipcode',
        'slug',
        'linkedin',
        'twitter',
        'youtube',
        'facebook',
        'about',
        'title',
        'is_public',
        'last_login_at',
        'title',
        'experience',
    ];

    /**
     * casts
     */
    protected $casts = [
        'avatar' => 'array',
        'company_logo' => 'array',
        'skills' => 'array',
    ];

    /**
     * @var string
     */
    public $slugFrom = 'name';

    /**
     * @var string
     */
    public $slugTo = 'slug';

    /**
     * Date time columns.
     */
    protected $dates = ['email_verified_at', 'last_login_at'];

    /**
     * Automatically creates hash for the user password.
     *
     * @param  string $value
     * @return void
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    /**
     * @param $value
     * @return array|mixed
     */
    public function getAvatarAttribute($value)
    {
        try {
            return (array) json_decode($value);
        } catch (\Exception $e) {
            return [];
        }
    }

    /**
     * @param $value
     * @return array|mixed
     */
    public function getCompanyLogoAttribute($value)
    {
        try {
            return (array) json_decode($value);
        } catch (\Exception $e) {
            return [];
        }
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}