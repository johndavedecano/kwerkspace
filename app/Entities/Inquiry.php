<?php

namespace App\Entities;

use App\Entities\Relations\WithPost;
use App\Entities\Relations\WithReplies;
use App\Entities\Relations\WithUser;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Inquiry
 *
 * @package App\Entities
 */
class Inquiry extends Model
{
    use WithUser, WithPost, WithReplies;

    /**
     * Database table name
     */
    protected $table = 'inquiries';

    /**
     * Protected columns from mass assignment
     */
    protected $guarded = ['id'];

    /**
     * @var array
     */
    protected $dates = [
        'viewing_date',
    ];

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'post_id',
        'user_id',
        'email',
        'name',
        'description',
        'contact_number',
        'viewing_date',
    ];
}