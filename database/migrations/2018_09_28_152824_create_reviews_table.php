<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReviewsTable extends Migration {

	public function up()
	{
		Schema::create('reviews', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('post_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->string('title');
			$table->text('description');
			$table->integer('rating');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('reviews');
	}
}