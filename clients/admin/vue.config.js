var path = require('path')

function resolve(dir) {
  return path.join(__dirname, dir)
}

module.exports = {
  lintOnSave: false,
  runtimeCompiler: true,
  chainWebpack: config => {
    return config.resolve.alias.set('@', resolve('./src'))
  }
}
