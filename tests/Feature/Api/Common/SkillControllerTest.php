<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/9/2018
 * Time: 3:53 PM
 */

namespace Tests\Feature\Api\Common;

use App\Entities\Skill;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SkillControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testIndexSuccess()
    {
        $response = $this->get('api/common/skills');

        $response->assertOk();
    }

    public function testShowSuccess()
    {
        $skill = factory(Skill::class)->create();

        $response = $this->get('api/common/skills/'.$skill->id);

        $response->assertOk();
    }
}