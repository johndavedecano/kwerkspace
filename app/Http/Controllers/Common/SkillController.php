<?php

namespace App\Http\Controllers\Common;

use App\Core\Constants;
use App\Http\Resources\SkillResource;
use App\Services\Skill\SkillCollection;
use App\Services\Skill\SkillManager;
use App\Http\Controllers\Controller;

/**
 * Class SkillController
 *
 * @package App\Http\Controllers\Admin
 */
class SkillController extends Controller
{
    /**
     * @var \App\Services\Feature\FeatureManager
     */
    public $manager;

    /**
     * @var \App\Services\Feature\FeatureCollection
     */
    public $collection;

    /**
     * SkillController constructor.
     *
     * @param SkillCollection $collection
     * @param SkillManager $manager
     */
    public function __construct(SkillCollection $collection, SkillManager $manager)
    {
        $this->collection = $collection;

        $this->manager = $manager;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $collection = $this->collection->listFromCommon();

        return SkillResource::collection($collection);
    }

    /**
     * @param $id
     * @return SkillResource
     */
    public function show($id)
    {
        $results = $this->collection->find($id);

        return new SkillResource($results);
    }
}
