<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 12:20 PM
 */

namespace App\Entities\Relations;

use App\Entities\Post;

/**
 * Trait WithPost
 *
 * @package App\Entities\Relations
 */
trait WithPost
{
    /**
     * @return mixed
     */
    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }
}