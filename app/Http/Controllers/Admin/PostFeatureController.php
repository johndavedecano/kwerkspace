<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\PostFeatureResource;
use App\Services\Post\PostFeatureCollection;
use App\Services\Post\PostFeatureManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class PostFeatureController
 *
 * @package App\Http\Controllers\Admin
 */
class PostFeatureController extends Controller
{
    /**
     * @var \App\Services\Post\PostFeatureManager
     */
    protected $manager;

    /**
     * @var \App\Services\Post\PostFeatureCollection
     * @var \App\Services\Post\PostFeatureCollection
     */
    protected $collection;

    /**
     * PostFeatureController constructor.
     *
     * @param \App\Services\Post\PostFeatureCollection $collection
     * @param \App\Services\Post\PostFeatureManager $manager
     */
    public function __construct(PostFeatureCollection $collection, PostFeatureManager $manager)
    {
        $this->collection = $collection;

        $this->manager = $manager;
    }

    /**
     * @param $postId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index($postId)
    {
        $results = $this->collection->listByPost($postId);

        return PostFeatureResource::collection($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $postId
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, $postId)
    {
        $this->validate($request, ['features' => 'required|array']);

        $results = $this->manager->fill($postId, $request->get('features'));

        return response()->json($results);
    }

    /**
     * @param $postId
     * @param $featureId
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($postId, $featureId)
    {
        $results = $this->manager->attach($postId, $featureId);

        return response()->json($results);
    }

    /**
     * @param $postId
     * @param $featureId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($postId, $featureId)
    {
        $results = $this->manager->detach($postId, $featureId);

        return response()->json($results);
    }
}
