<?php

namespace App\Http\Controllers\User;

use App\Http\Resources\InquiryResource;
use App\Services\Inquiry\InquiryCollection;
use App\Services\Inquiry\InquiryManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class InquiryController
 *
 * @package App\Http\Controllers\User
 */
class InquiryController extends Controller
{
    /**
     * @var InquiryCollection
     */
    protected $collection;

    /**
     * @var InquiryManager
     */
    protected $manager;

    /**
     * InquiryController constructor.
     *
     * @param InquiryCollection $collection
     * @param InquiryManager $manager
     */
    public function __construct(InquiryCollection $collection, InquiryManager $manager)
    {
        $this->collection = $collection;

        $this->manager = $manager;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $user = $this->user();

        $results = $this->collection->listFromUser($user->id);

        return InquiryResource::collection($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\InquiryResource
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'post_id' => 'required|exists:posts,id',
            'email' => 'required|email',
            'name' => 'required',
            'contact_number' => 'required',
            'description' => 'required',
        ]);

        $request = array_merge($request->all(), ['user_id' => auth()->id()]);

        $results = $this->manager->create($request);

        return new InquiryResource($results);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\InquiryResource
     */
    public function show($id)
    {
        $results = $this->collection->findByUser(auth()->id(), $id);

        return new InquiryResource($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \App\Http\Resources\InquiryResource
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'post_id' => 'sometimes|required|exists:posts,id',
            'email' => 'sometimes|required|email',
            'name' => 'sometimes|required',
            'contact_number' => 'sometimes|required',
            'description' => 'sometimes|required',
        ]);

        $request = array_merge($request->all(), ['user_id' => auth()->id()]);

        $this->manager->updateByUser(auth()->id(), $id, $request);

        return $this->show($id);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $this->manager->deleteByUser(auth()->id(), $id);

        return response()->json(['data' => 1]);
    }
}
