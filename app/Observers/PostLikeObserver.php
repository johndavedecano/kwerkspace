<?php

namespace App\Observers;


use App\Entities\PostsLike;
use App\Notifications\PostLikeCreated;

/**
 * Class PostLikeObserver
 *
 * @package App\Observers
 */
class PostLikeObserver
{
    /**
     * @param \App\Entities\PostsLike $postLike
     */
    public function created(PostsLike $postLike)
    {
        $post = $postLike->post;

        $post->user->notify(new PostLikeCreated($postLike));
    }

    /**
     * @param \App\Entities\PostsLike $postLike
     */
    public function updated(PostsLike $postLike)
    {
        //
    }

    /**
     * @param \App\Entities\PostsLike $postLike
     */
    public function forceDeleted(PostsLike $postLike)
    {
        //
    }
}
