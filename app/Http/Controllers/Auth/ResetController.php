<?php

namespace App\Http\Controllers\Auth;

use App\Services\User\UserAuthManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class ResetController
 *
 * @package App\Http\Controllers\Auth
 */
class ResetController extends Controller
{
    /**
     * @var \App\Services\User\UserAuthManager
     */
    protected $manager;

    /**
     * ResetController constructor.
     *
     * @param \App\Services\User\UserAuthManager $manager
     */
    public function __construct(UserAuthManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'password' => 'min:6|max:8|confirmed',
            'password_confirmation' => 'required',
            'token' => 'required',
        ]);

        $results = $this->manager->reset($request->only(['token', 'password', 'password_confirmation']));

        return response()->json($results);
    }
}
