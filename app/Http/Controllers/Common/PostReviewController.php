<?php

namespace App\Http\Controllers\Common;

use App\Http\Resources\ReviewResource;
use App\Http\Controllers\Controller;
use App\Services\Review\ReviewCollection;

/**
 * Class PostReviewController
 *
 * @package App\Http\Controllers\Common
 */
class PostReviewController extends Controller
{
    /**
     * @var \App\Services\Review\ReviewCollection
     */
    public $collection;

    /**
     * PostReviewController constructor.
     *
     * @param \App\Services\Review\ReviewCollection $collection
     */
    public function __construct(ReviewCollection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * @param $postId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index($postId)
    {
        $results = $this->collection->listByPost($postId);

        return ReviewResource::collection($results);
    }

    /**
     * @param $postId
     * @param $id
     * @return \App\Http\Resources\ReviewResource
     */
    public function show($postId, $id)
    {
        $results = $this->collection->findByPost($postId, $id);

        return new ReviewResource($results);
    }
}
