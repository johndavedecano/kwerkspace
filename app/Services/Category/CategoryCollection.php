<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:20 PM
 */

namespace App\Services\Category;

use App\Core\Abstracts\AbstractCollection;
use App\Core\Interfaces\CollectionInterface;
use App\Entities\Category;
use App\Services\Traits\WithFindSlug;
use App\Services\Traits\WithNameSearch;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class CategoryCollection
 *
 * @package App\Services\Category
 */
class CategoryCollection extends AbstractCollection implements CollectionInterface
{
    use WithFindSlug, WithNameSearch;

    /**
     * CategoryCollection constructor.
     *
     * @param \App\Entities\Category $model
     */
    public function __construct(Category $model)
    {
        $this->builder = QueryBuilder::for(Category::class);

        $this->model = $model;
    }
}