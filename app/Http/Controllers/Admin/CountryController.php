<?php

namespace App\Http\Controllers\Admin;

use App\Core\Constants;
use App\Http\Resources\CountryResource;
use App\Services\Country\CountryCollection;
use App\Services\Country\CountryManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class CountryController
 *
 * @package App\Http\Controllers\Admin
 */
class CountryController extends Controller
{
    /**
     * @var \App\Services\Country\CountryManager
     */
    public $manager;

    /**
     * @var \App\Services\Country\CountryCollection
     */
    public $collection;

    /**
     * CountryController constructor.
     *
     * @param \App\Services\Country\CountryCollection $collection
     * @param \App\Services\Country\CountryManager $manager
     */
    public function __construct(CountryCollection $collection, CountryManager $manager)
    {
        $this->collection = $collection;

        $this->manager = $manager;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $collection = $this->collection->listFromAdmin();

        return CountryResource::collection($collection);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\CountryResource
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
        ]);

        $results = $this->manager->create($request->all());

        return new CountryResource($results);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\CountryResource
     */
    public function show($id)
    {
        $results = $this->collection->find($id);

        return new CountryResource($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \App\Http\Resources\CountryResource
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name' => 'sometimes|required|min:1|max:24',
            'status' => 'sometimes|in:active,inactive,deleted',
        ];

        $this->validate($request, $rules);

        $this->manager->update($id, $request->all());

        return $this->show($id);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\CountryResource
     */
    public function destroy($id)
    {
        $this->manager->delete($id);

        return $this->show($id);
    }
}
