<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:21 PM
 */

namespace App\Services\Post;

use App\Core\Constants;
use App\Core\Types\Image;
use App\Entities\Post;

/**
 * Class PostManager
 *
 * @package App\Services\Post
 */
class PostManager
{
    /**
     * @var \App\Services\Post\Post
     */
    public $model;

    /**
     * PostManager constructor.
     *
     * @param \App\Entities\Post $model
     */
    public function __construct(Post $model)
    {
        $this->model = $model;
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function create($data = [])
    {
        $post = $this->model->create(array_only($data, $this->model->getFillable()));

        if (isset($data['images']) && !empty($data['images'])) {
            foreach ($data['images'] as $files) {

                $image = new Image($files);

                $post->images()->create(['files' => (string)$image]);
            }
        }

        if (isset($data['features']) && !empty($data['features'])) {
            $post->features()->sync($data['features']);
        }

        return $post;
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed
     */
    public function update($id, $data = [])
    {
        $post = $this->model->findOrFail($id);

        $post->update(array_only($data, $this->model->getFillable()));

        if (isset($data['features']) && !empty($data['features'])) {
            $post->features()->sync($data['features']);
        }

        return $post;
    }

    /**
     * @param $userId
     * @param $postId
     * @return mixed
     */
    public function deleteByUser($userId, $postId)
    {
        return $this->model
            ->where('id', $postId)
            ->where('user_id', $userId)
            ->firstOrFail()
            ->update(['status' => Constants::STATUS_DELETED]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model->where('id', $id)->firstOrFail()->update(['status' => Constants::STATUS_DELETED]);
    }
}