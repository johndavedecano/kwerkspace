<?php

namespace App\Http\Controllers\Common;

use App\Http\Resources\PostFeatureResource;
use App\Services\Post\PostFeatureCollection;
use App\Http\Controllers\Controller;

/**
 * Class PostFeatureController
 *
 * @package App\Http\Controllers\Common
 */
class PostFeatureController extends Controller
{
    /**
     * @var \App\Services\Post\PostFeatureCollection
     */
    public $collection;

    /**
     * PostFeatureController constructor.
     *
     * @param \App\Services\Post\PostFeatureCollection $collection
     */
    public function __construct(PostFeatureCollection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * @param $postId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index($postId)
    {
        $results = $this->collection->listByPost($postId);

        return PostFeatureResource::collection($results);
    }

    /**
     * @param $postId
     * @param $id
     * @return \App\Http\Resources\PostFeatureResource
     */
    public function show($postId, $id)
    {
        $results = $this->collection->findByPost($postId, $id);

        return new PostFeatureResource($results);
    }
}
