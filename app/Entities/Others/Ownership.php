<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/4/2018
 * Time: 10:41 PM
 */

namespace App\Entities\Others;

use Illuminate\Auth\Access\AuthorizationException;

trait Ownership
{
    /**
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function checkOwnership()
    {
        if (auth()->id() !== $this->user_id) {
            throw new AuthorizationException('You are not authorized to modify this data.');
        }
    }
}