<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:22 PM
 */

namespace App\Services\Upload;

use Illuminate\Http\UploadedFile;
use Imageupload;

class UploadManager
{
    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @return array
     */
    public static function upload(UploadedFile $file)
    {
        return Imageupload::upload($file);
    }
}