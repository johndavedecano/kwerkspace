<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/7/2018
 * Time: 12:22 PM
 */

namespace App\Core\Types;

class FakeImage
{
    public function __invoke()
    {
        return [
            'original' => [
                'original_filename' => '43102657_10217424143416392_9218710816222609408_n.jpg',
                'original_filedir' => 'uploads/images/43102657_10217424143416392_9218710816222609408_n.jpg',
                'original_width' => 480,
                'original_height' => 672,
                's3_url' => '',
            ],
            'square50' => [
                'dir' => 'uploads/images/43102657_10217424143416392_9218710816222609408_n_square50.jpg',
                'filename' => '43102657_10217424143416392_9218710816222609408_n_square50.jpg',
                'filedir' => 'uploads/images/43102657_10217424143416392_9218710816222609408_n_square50.jpg',
                's3_url' => '',
                'width' => 50,
                'height' => 50,
                'filesize' => 1781,
                'is_squared' => true,
            ],
            'square100' => [
                'dir' => 'uploads/images/43102657_10217424143416392_9218710816222609408_n_square100.jpg',
                'filename' => '43102657_10217424143416392_9218710816222609408_n_square100.jpg',
                'filedir' => 'uploads/images/43102657_10217424143416392_9218710816222609408_n_square100.jpg',
                's3_url' => '',
                'width' => 100,
                'height' => 100,
                'filesize' => 4337,
                'is_squared' => true,
            ],
            'square200' => [
                'dir' => 'uploads/images/43102657_10217424143416392_9218710816222609408_n_square200.jpg',
                'filename' => '43102657_10217424143416392_9218710816222609408_n_square200.jpg',
                'filedir' => 'uploads/images/43102657_10217424143416392_9218710816222609408_n_square200.jpg',
                's3_url' => '',
                'width' => 200,
                'height' => 200,
                'filesize' => 12070,
                'is_squared' => true,
            ],
        ];
    }
}