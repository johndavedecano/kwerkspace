<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('countries')->truncate();

        $id = 1;

        foreach (\App\Core\Types\Country::LIST as $key => $val) {

            \App\Entities\Country::create([
                'id' => $id,
                'name' => $val,
                'slug' => $key,
            ]);

            $id++;
        }
    }
}
