<?php

namespace App\Http\Controllers\Common;

use App\Http\Resources\CountryResource;
use App\Services\Country\CountryCollection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class CountryController
 *
 * @package App\Http\Controllers\Common
 */
class CountryController extends Controller
{
    /**
     * @var \App\Services\Country\CountryCollection
     */
    public $collection;

    /**
     * CountryController constructor.
     *
     * @param \App\Services\Country\CountryCollection $collection
     */
    public function __construct(CountryCollection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        
        if (request()->has('all')) {
            $collection = $this->collection->model->all();
        } else {
            $collection = $this->collection->listFromCommon();
        }

        return CountryResource::collection($collection);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\CountryResource
     */
    public function show($id)
    {
        $results = $this->collection->findNotDeleted($id);

        return new CountryResource($results);
    }
}
