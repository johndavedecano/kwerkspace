<?php

namespace App\Http\Controllers\Common;

use App\Http\Resources\CategoryResource;
use App\Services\Category\CategoryCollection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class CategoryController
 *
 * @package App\Http\Controllers\Common
 */
class CategoryController extends Controller
{
    /**
     * @var \App\Services\Category\CategoryCollection
     */
    public $collection;

    /**
     * CategoryController constructor.
     *
     * @param \App\Services\Category\CategoryCollection $collection
     */
    public function __construct(CategoryCollection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $collection = $this->collection->listFromCommon();

        return CategoryResource::collection($collection);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\CategoryResource
     */
    public function show($id)
    {
        $results = $this->collection->findNotDeleted($id);

        return new CategoryResource($results);
    }
}
