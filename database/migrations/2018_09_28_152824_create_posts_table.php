<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsTable extends Migration {

	public function up()
	{
		Schema::create('posts', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('category_id')->unsigned()->nullable();
			$table->integer('country_id')->unsigned()->nullable();
			$table->string('title')->default('');
			$table->string('status')->default('active');
			$table->text('description')->nullable();
			$table->text('policies')->nullable();
			$table->text('thumbnail')->nullable();
            $table->string('slug')->unique();
			$table->text('cover')->nullable();
			$table->decimal('area')->nullable();
			$table->integer('max_occupants')->default(1);
			$table->enum('payment_type', array('monthly', 'weekly', 'daily', 'annually', 'hourly'));
			$table->decimal('payment_amount');
			$table->string('address')->default('');
			$table->string('city');
			$table->index('city');
			$table->string('state')->default('Metro Manila');
			$table->index('state');
			$table->string('zipcode')->default('');
			$table->decimal('lon', 9, 6)->nullable();
			$table->decimal('lat', 9, 6)->nullable();

			$table->string('mon')->default(true);
			$table->string('tue')->default(true);
			$table->string('wed')->default(true);
			$table->string('thu')->default(true);
			$table->string('fri')->default(true);
			$table->string('sat')->default(true);
			$table->string('sun')->default(true);

			$table->decimal('rating')->default(0);

			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('posts');
	}
}