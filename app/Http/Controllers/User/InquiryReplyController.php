<?php

namespace App\Http\Controllers\User;

use App\Entities\Inquiry;
use App\Http\Resources\InquiryReplyResource;
use App\Services\Inquiry\InquiryReplyCollection;
use App\Services\Inquiry\InquiryReplyManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class InquiryReplyController
 *
 * @package App\Http\Controllers\User
 */
class InquiryReplyController extends Controller
{
    /**
     * @var \App\Services\Inquiry\InquiryReplyCollection
     */
    protected $collection;

    /**
     * @var \App\Services\Inquiry\InquiryReplyManager
     */
    protected $manager;

    /**
     * InquiryReplyController constructor.
     *
     * @param \App\Services\Inquiry\InquiryReplyCollection $collection
     * @param \App\Services\Inquiry\InquiryReplyManager $manager
     */
    public function __construct(InquiryReplyCollection $collection, InquiryReplyManager $manager)
    {
        $this->manager = $manager;

        $this->collection = $collection;
    }

    /**
     * @param $inquiryId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index($inquiryId)
    {
        $results = $this->collection->listByUser(auth()->id(), $inquiryId);

        return InquiryReplyResource::collection($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $inquiryId
     * @return \App\Http\Resources\InquiryReplyResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request, $inquiryId)
    {
        $this->validate($request, [
            'message' => 'required',
        ]);

        $results = $this->manager->createByUser(auth()->id(), $inquiryId, [
            'message' => $request->get('message'),
        ]);

        return new InquiryReplyResource($results);
    }

    /**
     * @param $inquiryId
     * @param $id
     * @return \App\Http\Resources\InquiryReplyResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show($inquiryId, $id)
    {
        $results = $this->collection->findByUser(auth()->id(), $inquiryId, $id);

        return new InquiryReplyResource($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $inquiryId
     * @param $id
     * @return \App\Http\Resources\InquiryReplyResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(Request $request, $inquiryId, $id)
    {
        $this->validate($request, [
            'message' => 'sometimes|required',
        ]);

        $this->manager->updateByUser(auth()->id(), $inquiryId, $id, $request->all());

        return $this->show($inquiryId, $id);
    }

    /**
     * @param $inquiryId
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy($inquiryId, $id)
    {
        $this->manager->deleteByUser(auth()->id(), $inquiryId, $id);

        return response()->json(['id' => $id]);
    }
}
