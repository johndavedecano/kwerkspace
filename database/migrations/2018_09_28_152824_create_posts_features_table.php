<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsFeaturesTable extends Migration {

	public function up()
	{
		Schema::create('posts_features', function(Blueprint $table) {
			$table->integer('post_id')->unsigned();
			$table->integer('feature_id')->unsigned();
		});
	}

	public function down()
	{
		Schema::drop('posts_features');
	}
}