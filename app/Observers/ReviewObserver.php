<?php

namespace App\Observers;

use App\Entities\Review;
use App\Jobs\CalculatePostRating;
use App\Notifications\PostReviewCreated;

class ReviewObserver
{
    /**
     * @param \App\Entities\Review $review
     */
    public function created(Review $review)
    {
        CalculatePostRating::dispatch($review);

        $review->user->notify(new PostReviewCreated($review));
    }

    /**
     * @param \App\Entities\Review $review
     */
    public function updated(Review $review)
    {
        CalculatePostRating::dispatch($review);
    }

    /**
     * @param \App\Entities\Review $review
     */
    public function forceDeleted(Review $review)
    {
        CalculatePostRating::dispatch($review);
    }
}
