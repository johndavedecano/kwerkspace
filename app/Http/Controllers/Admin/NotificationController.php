<?php

namespace App\Http\Controllers\Admin;

use App\Core\Constants;
use App\Http\Resources\NotificationResource;
use App\Http\Controllers\Controller;

/**
 * Class NotificationController
 *
 * @package App\Http\Controllers\Admin
 */
class NotificationController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $user = auth()->user();

        if (request()->has('all') && request()->get('all')) {
            $results = $user->notifications()->paginate(Constants::PAGINATION_LIMIT);
        } else {
            $results = $user->unreadNotifications()->paginate(Constants::PAGINATION_LIMIT);
        }

        return NotificationResource::collection($results);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function read($id)
    {
        $user = auth()->user();

        $results = $user->unreadNotifications->where('id', $id)->markAsRead();

        return response()->json(['data' => $results]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function readAll()
    {
        $user = auth()->user();

        $results = $user->unreadNotifications->markAsRead();

        return response()->json(['data' => $results]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $user = auth()->user();

        $results = $user->notifications()->where('id', $id)->delete();

        return response()->json(['data' => $results]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroyAll()
    {
        $user = auth()->user();

        $results = $user->notifications()->delete();

        return response()->json(['data' => $results]);
    }
}
