<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/3/2018
 * Time: 8:14 PM
 */

namespace App\Entities\Others;

use App\Core\Constants;
use App\Entities\User;
use App\Notifications\VerifyEmail;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;

trait MustVerifyEmail
{
    /**
     * @return string
     */
    public function token()
    {
        return encrypt($this->email);
    }

    /**
     * @param $token
     * @return mixed
     */
    public static function verifyToken($token)
    {
        $email = decrypt($token);

        return static::where('email', $email)->firstOrFail();
    }

    /**
     * Determine if the user has verified their email address.
     *
     * @return bool
     */
    public function hasVerifiedEmail()
    {
        return ! is_null($this->email_verified_at);
    }

    /**
     * Mark the given user's email as verified.
     *
     * @return bool
     */
    public function markEmailAsVerified()
    {
        return $this->forceFill([
            'email_verified_at' => $this->freshTimestamp(),
            'status' => Constants::STATUS_ACTIVE,
        ])->save();
    }

    public function sendEmailVerificationNotification()
    {
        if ($this->status === Constants::STATUS_ACTIVE) {
            throw new HttpException(400, 'Looks like user is already verified');
        }

        $this->notify(new VerifyEmail($this->token()));
    }
}