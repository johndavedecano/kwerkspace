<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:20 PM
 */

namespace App\Services\Inquiry;

use App\Core\Abstracts\AbstractCollection;
use App\Core\Constants;
use App\Core\Interfaces\CollectionInterface;
use App\Entities\Inquiry;
use App\Services\Traits\WithFindSlug;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class InquiryCollection
 *
 * @package App\Services\Inquiry
 */
class InquiryCollection extends AbstractCollection implements CollectionInterface
{
    const USER_FIELDS = ['name', 'slug', 'id', 'avatar', 'company', 'company_logo'];

    /**
     * InquiryCollection constructor.
     *
     * @param \App\Entities\Inquiry $model
     */
    public function __construct(Inquiry $model)
    {
        $this->builder = QueryBuilder::for(Inquiry::class);

        $this->model = $model;
    }

    /**
     * @param $userId
     * @param $inquiryId
     * @return mixed
     */
    public function findByUser($userId, $inquiryId)
    {
        return $this->model->where([
            'user_id' => $userId,
            'id' => $inquiryId,
        ])->firstOrFail();
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function listFromUser($userId)
    {
        return $this->builder->where('user_id', $userId)->where('status', '<>', Constants::STATUS_DELETED)->with([
                'user' => function ($query) {
                    return $query->select(self::USER_FIELDS);
                },
            ])->with('post')->paginate(Constants::PAGINATION_LIMIT);
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function listFromAdmin()
    {
        return $this->builder->with([
            'user' => function ($query) {
                return $query->select(self::USER_FIELDS);
            },
        ])->with('post')->paginate(Constants::PAGINATION_LIMIT);
    }
}