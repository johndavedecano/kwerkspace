<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/8/2018
 * Time: 7:32 PM
 */

namespace Tests\Feature\Api\Auth;

use App\Entities\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class ForgotControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testStoreFailsValidation()
    {
        $response = $this->post('api/auth/forgot');

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStoreFailsEmailDoesNotExists()
    {
        $response = $this->post('api/auth/forgot', ['email' => 'email@doesnotexists.com']);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStoreSuccess()
    {
        $user = factory(User::class)->create(['email' => 'test@gmail.com']);

        $response = $this->post('api/auth/forgot', ['email' => $user->email]);

        $response->assertOk();
    }
}