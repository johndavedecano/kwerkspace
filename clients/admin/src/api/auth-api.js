// auth-api
import axios from 'axios'

export function login(email, password) {
  return axios
    .post('/auth/login', { email, password })
    .then(response => response.data)
}
