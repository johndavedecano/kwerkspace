<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/6/2018
 * Time: 7:46 PM
 */

namespace Tests\Feature\Api\User;

use App\Entities\InquiriesReply;
use App\Entities\Inquiry;
use App\Entities\Post;
use App\Entities\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\Feature\Api\Authentication;
use Tests\TestCase;

/**
 * Class InquiryControllerTest
 *
 * @package Tests\Feature\Api\Admin
 */
class InquiryReplyControllerTest extends TestCase
{
    use RefreshDatabase, Authentication;

    public function setUp()
    {
        parent::setUp();

        $this->user = $this->getAuthHeaders(User::ROLE_USER);
    }

    public function testIndexFails()
    {
        $inquiry = factory(Inquiry::class)->create(['user_id' => $this->auth['user']->id]);

        $response = $this->withHeaders(['Accept' => 'application/json'])->get('/api/user/inquiries/'.$inquiry->id.'/replies');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }


    public function testIndexSuccess()
    {
        $inquiry = factory(Inquiry::class)->create(['user_id' => $this->auth['user']->id]);

        $response = $this->withHeaders($this->user)->get('/api/user/inquiries/'.$inquiry->id.'/replies');

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testStoreFails()
    {
        $inquiry = factory(Inquiry::class)->create(['user_id' => $this->auth['user']->id]);

        $response = $this->withHeaders(['Accept' => 'application/json'])->post('/api/user/inquiries/'.$inquiry->id.'/replies');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testStoreValidationFails()
    {
        $inquiry = factory(Inquiry::class)->create(['user_id' => $this->auth['user']->id]);

        $response = $this->withHeaders($this->user)->post('/api/user/inquiries/'.$inquiry->id.'/replies');

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStoreSuccess()
    {
        $inquiry = factory(Inquiry::class)->create(['user_id' => $this->auth['user']->id]);

        $data = [
            'user_id' => $this->auth['user']->id,
            'post_id' => $inquiry->post_id,
            'inquiry_id' => $inquiry->id,
            'message' => 'hello',
        ];

        $response = $this->withHeaders($this->user)->post('/api/user/inquiries/'.$inquiry->id.'/replies', $data);

        $response->assertStatus(Response::HTTP_CREATED);
    }

    public function testUpdateFails()
    {
        $reply = factory(InquiriesReply::class)->create(['user_id' => $this->auth['user']->id]);

        $data = [
            'message' => '',
        ];

        $response = $this->withHeaders(['Accept' => 'application/json'])->put('/api/user/inquiries/'.$reply->inquiry_id.'/replies/'.$reply->id, $data);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testUpdateValidationFails()
    {
        $reply = factory(InquiriesReply::class)->create(['user_id' => $this->auth['user']->id]);

        $data = [
            'message' => '',
        ];

        $response = $this->withHeaders($this->user)->put('/api/user/inquiries/'.$reply->inquiry_id.'/replies/'.$reply->id, $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdateSuccess()
    {

        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $inquiry = factory(Inquiry::class)->create(['user_id' => $this->auth['user']->id, 'post_id' => $post->id]);

        $reply = factory(InquiriesReply::class)->create(['user_id' => $this->auth['user']->id, 'inquiry_id' => $inquiry->id]);

        $data = [
            'message' => 'test',
        ];

        $response = $this->withHeaders($this->user)->put('/api/user/inquiries/'.$inquiry->id.'/replies/'.$reply->id, $data);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testDestroyFails()
    {
        $reply = factory(InquiriesReply::class)->create(['user_id' => $this->auth['user']->id]);

        $response = $this->withHeaders([])->delete('/api/user/inquiries/'.$reply->inquiry_id.'/replies/'.$reply->id);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testDestroyFailsNotFound()
    {
        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $inquiry = factory(Inquiry::class)->create(['user_id' => $this->auth['user']->id, 'post_id' => $post->id]);

        $reply = factory(InquiriesReply::class)->create(['user_id' => $this->auth['user']->id, 'inquiry_id' => $inquiry->id]);

        $response = $this->withHeaders($this->user)->delete('/api/user/inquiries/'.$reply->inquiry_id.'/replies/456456456');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testDestroySuccess()
    {
        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $inquiry = factory(Inquiry::class)->create(['user_id' => $this->auth['user']->id, 'post_id' => $post->id]);

        $reply = factory(InquiriesReply::class)->create(['user_id' => $this->auth['user']->id, 'inquiry_id' => $inquiry->id]);

        $response = $this->withHeaders($this->user)->delete('/api/user/inquiries/'.$inquiry->id.'/replies/'.$reply->id);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testShowFails()
    {
        $reply = factory(InquiriesReply::class)->create(['user_id' => $this->auth['user']->id]);

        $response = $this->withHeaders([])->get('/api/user/inquiries/'.$reply->inquiry_id.'/replies/'.$reply->id);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testShowFailsNotFound()
    {
        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $inquiry = factory(Inquiry::class)->create(['user_id' => $this->auth['user']->id, 'post_id' => $post->id]);

        $response = $this->withHeaders($this->user)->get('/api/user/inquiries/'.$inquiry->id.'/replies/5235325');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testShowSuccess()
    {
        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $inquiry = factory(Inquiry::class)->create(['user_id' => $this->auth['user']->id, 'post_id' => $post->id]);

        $reply = factory(InquiriesReply::class)->create(['user_id' => $this->auth['user']->id, 'inquiry_id' => $inquiry->id]);

        $response = $this->withHeaders($this->user)->get('/api/user/inquiries/'.$inquiry->id.'/replies/'.$reply->id);

        $response->assertStatus(Response::HTTP_OK);
    }
}