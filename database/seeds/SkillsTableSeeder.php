<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SkillsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('skills')->truncate();

        $categories = ["CSS", "Javascript", "HTML", "Web Development", "PHP", "WordPress Development", "jQuery", "HTML5", "Bootstrap", "Java", "eCommerce & Shopping Carts", "Android", "AngularJS", "Software Testing", "Python", "Node.js", "C#", "Laravel", "Mobile Development", "React", "API Development", "Google Maps", "iOS Development", "AJAX", "Data Scraping", "Ruby on Rails", "CodeIgniter", "ASP & ASP.NET", "Magento", "Git", "Web Scraping", "C/C++", "CMS", "PSD to WordPress", "Software Architecture", "Shopify Development", "SCSS", "Front-end", ".NET", "Software Development", "Swift", "Joomla", "XML", "Vue.js", "cPanel, Plesk & Control Panels", "Django", "React Native", "Email Design", "WooCommerce", "Visual Basic & VB.NET", "Development", "CakePHP", "REST", "Sass", "Objective C", "Web Security", "Drupal", "Game Design & Development", "GitHub", "JSON", "MVC", "Express.js", "Scrum", "Back-end", "Web Services", "Wix", "Ionic Framework", "Docker", "Ruby", "Desktop Development", "OpenCart", "Redux", "Dreamweaver", "Web API", "Prototyping", "Google App Engine", "Material Design", "TypeScript", "Infusionsoft", "Programming", "Yii", "JSP & J2EE", "PayPal Development", "Spring Framework", "Twitter Development", "Symfony", "webpack", "Flash", "SaaS", "LeSS", "Unity3D", "Windows Development", "Firebase", "Cordova", "Gulp.js", "Full Stack", "User Acceptance Testing", "Unit Testing", "Jenkins", "Big Commerce", "App Development", "Chatbot", "ADO.NET", "corePHP", "R Programming", "Payment Gateway Integration", "Flask", "Hibernate", "MEAN Stack", "Entity Framework", "Spring Boot", "Go", "Zend Framework", "LAMP Stack", "OOP", "Spring MVC", "TDD", "Xamarin", "MATLAB", "Twilio", "Bash", "Xcode", "AI (Artificial Intelligence)", "Perl", "Authorize.net", "macOS Development", "Grunt", "Backbone.js", "MeteorJS", "Bitbucket", "Microservices", "Kotlin", "Chef", "Foundation", "Theme Development", "Scala", "Divi", "Functional Programming", "Atom", "RSpec", "Axure RP", "GraphQL", "PhoneGap", "NumPy", "npm", "PowerShell", "ES", "D3.js", "OSCommerce", "Ember.js", "Delphi", "Electron", "LINQ", "Tomcat", "AR (Augmented Reality)", "Flex", "Knockout", "Plugin Development", "CoffeeScript", "JUnit", "Pascal", "WCF", "UML", "JMeter", "PhpStorm", "OpenGL Programming", "Pandas", "CI", "Sencha", "Eclipse", "Bower", "Leaflet", "Unreal Engine", "Cold Fusion", "Lumen", "TensorFlow", "Elixir", "Cucumber", "Socket.IO", "Slim Framework", "TestNG", "Travis CI", "JPA", "Kendo UI", "Ext JS", "Scikit-learn", "Kafka", "Solr", "Redmine", "Bot Development", "Serverless", "OAuth", "JSF", "Mocha", "Haml", "SQLAlchemy", "ActionScript", "ReactiveX", "Solidity", "Telerik", "Sails", "Struts", "Semantic UI", "Chrome Extensions Development", "Underscore.js", "Core Data", "hapi.js", "Apollo", "Highcharts", "Handlebars", "R", "GUI", "Phalcon", "Silverlight", "Three.js", "PhantomJS", "Jest", "Neo4j", "Koa", "Lua", "jekyll", "Jboss", "Gradle", "Mern Stack", "COBOL", "Capistrano", "Cocos2d", "Pug", "YAML", "JDBC", "Sidekiq", "Babel", "Twig", "Realm", "JADE", "LoopBack", "Sinatra", "Visualforce", "NativeScript", "Akka", "Cocoa Touch", "Phoenix", "AMP", "Haskell", "WebGL", "Mockito", "Marionette,js", "Rust", "Grails", "MobX", "Clojure", "EJB", "Play Framework", "Smarty", "ABAP", "Parse", "Shopware", "FabricJS", "LEMP Stack", "SpecFlow", "Genesis", "Flux", "GWT", "Framer", "SPA", "SignalR", "GreenSock", "DotNetNuke", "VHDL", "ZURB", "SaltStack", "Skeleton", "RequireJS", "MapReduce", "Doctrine", "Nuke", "Appcelerator", "GlassFish", "Camel", "Dojo", "Swing", "Xilinx", "Relay", "Kohana", "Dagger", "Alamofire", "Puma", "MapKit", "TOGAF", "Flutter", "DSP", "QML", "Elm", "Gerrit", "EventBus", "Feathers.", "Watir", "Thymeleaf", "fastlane", "Box2D", "Aurelia", "Haxe"];

        $index = 1;

        foreach ($categories as $category) {
            \App\Entities\Skill::create([
                'id' => $index,
                'name' => $category,
            ]);

            $index++;
        }
    }
}
