<?php

namespace App\Http\Controllers\Auth;

use App\Services\User\UserAuthManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class ForgotController
 *
 * @package App\Http\Controllers\Auth
 */
class ForgotController extends Controller
{
    /**
     * @var \App\Services\User\UserAuthManager
     */
    protected $manager;

    /**
     * ForgotController constructor.
     *
     * @param \App\Services\User\UserAuthManager $manager
     */
    public function __construct(UserAuthManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $this->validate($request, ['email' => 'required|email|exists:users,email']);

        $results = $this->manager->forgot($request->only(['email']));

        return response()->json(['email' => $results->email]);
    }
}
