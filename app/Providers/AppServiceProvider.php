<?php

namespace App\Providers;

use App\Entities\PostsLike;
use App\Entities\Review;
use App\Observers\PostLikeObserver;
use App\Observers\ReviewObserver;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Review::observe(ReviewObserver::class);
        PostsLike::observe(PostLikeObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
