<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 12:19 PM
 */

namespace App\Entities\Relations;

use App\Entities\User;

/**
 * Trait WithUser
 *
 * @package App\Entities\Relations
 */
trait WithUser
{
    /**
     * user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}