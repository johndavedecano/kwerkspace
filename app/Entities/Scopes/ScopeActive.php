<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:59 PM
 */

namespace App\Entities\Scopes;

use App\Core\Constants;

/**
 * Class ScopeActive
 *
 * @package App\Entities\Scopes
 */
trait ScopeActive
{
    public function scopeActive($query)
    {
        return $query->where('status', Constants::STATUS_ACTIVE);
    }
}