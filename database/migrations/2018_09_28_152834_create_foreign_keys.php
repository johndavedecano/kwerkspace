<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('users', function(Blueprint $table) {
			$table->foreign('country_id')->references('id')->on('countries')
						->onDelete('set null')
						->onUpdate('no action');
		});
		Schema::table('posts_images', function(Blueprint $table) {
			$table->foreign('post_id')->references('id')->on('posts')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('posts', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('posts', function(Blueprint $table) {
			$table->foreign('category_id')->references('id')->on('countries')
						->onDelete('set null')
						->onUpdate('no action');
		});
		Schema::table('posts', function(Blueprint $table) {
			$table->foreign('country_id')->references('id')->on('countries')
						->onDelete('set null')
						->onUpdate('no action');
		});
		Schema::table('posts_features', function(Blueprint $table) {
			$table->foreign('post_id')->references('id')->on('posts')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('posts_features', function(Blueprint $table) {
			$table->foreign('feature_id')->references('id')->on('features')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('inquiries', function(Blueprint $table) {
			$table->foreign('post_id')->references('id')->on('posts')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('inquiries', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('reviews', function(Blueprint $table) {
			$table->foreign('post_id')->references('id')->on('posts')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('reviews', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('posts_likes', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('posts_likes', function(Blueprint $table) {
			$table->foreign('post_id')->references('id')->on('posts')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('inquiries_replies', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('no action');
		});
		Schema::table('inquiries_replies', function(Blueprint $table) {
			$table->foreign('post_id')->references('id')->on('posts')
						->onDelete('cascade')
						->onUpdate('no action');
		});
        Schema::table('inquiries_replies', function(Blueprint $table) {
            $table->foreign('inquiry_id')->references('id')->on('inquiries')
                ->onDelete('cascade')
                ->onUpdate('no action');
        });
	}

	public function down()
	{
		Schema::table('users', function(Blueprint $table) {
			$table->dropForeign('users_country_id_foreign');
		});
		Schema::table('posts_images', function(Blueprint $table) {
			$table->dropForeign('posts_images_post_id_foreign');
		});
		Schema::table('posts', function(Blueprint $table) {
			$table->dropForeign('posts_user_id_foreign');
		});
		Schema::table('posts', function(Blueprint $table) {
			$table->dropForeign('posts_category_id_foreign');
		});
		Schema::table('posts', function(Blueprint $table) {
			$table->dropForeign('posts_country_id_foreign');
		});
		Schema::table('posts_features', function(Blueprint $table) {
			$table->dropForeign('posts_features_post_id_foreign');
		});
		Schema::table('posts_features', function(Blueprint $table) {
			$table->dropForeign('posts_features_feature_id_foreign');
		});
		Schema::table('inquiries', function(Blueprint $table) {
			$table->dropForeign('inquiries_post_id_foreign');
		});
		Schema::table('inquiries', function(Blueprint $table) {
			$table->dropForeign('inquiries_user_id_foreign');
		});
		Schema::table('reviews', function(Blueprint $table) {
			$table->dropForeign('reviews_post_id_foreign');
		});
		Schema::table('reviews', function(Blueprint $table) {
			$table->dropForeign('reviews_user_id_foreign');
		});
		Schema::table('posts_likes', function(Blueprint $table) {
			$table->dropForeign('posts_likes_user_id_foreign');
		});
		Schema::table('posts_likes', function(Blueprint $table) {
			$table->dropForeign('posts_likes_post_id_foreign');
		});
		Schema::table('inquiries_replies', function(Blueprint $table) {
			$table->dropForeign('inquiries_replies_user_id_foreign');
		});
		Schema::table('inquiries_replies', function(Blueprint $table) {
			$table->dropForeign('inquiries_replies_post_id_foreign');
		});
        Schema::table('inquiries_replies', function(Blueprint $table) {
            $table->dropForeign('inquiries_replies_inquiry_id_foreign');
        });
	}
}