<?php

namespace App\Entities;

use App\Entities\Relations\WithPost;
use App\Entities\Relations\WithUser;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PostsLike
 *
 * @package App\Entities
 */
class PostsLike extends Model
{
    use WithUser, WithPost;

    /**
     * Database table name
     */
    protected $table = 'posts_likes';

    /**
     * Protected columns from mass assignment
     */
    protected $guarded = ['id'];

    /**
     * Mass assignable columns
     */
    protected $fillable = ['user_id', 'post_id'];
}