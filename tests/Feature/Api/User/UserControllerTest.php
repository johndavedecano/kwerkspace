<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/6/2018
 * Time: 7:46 PM
 */

namespace Tests\Feature\Api\User;

use App\Core\Constants;
use App\Entities\Country;
use App\Entities\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\Feature\Api\Authentication;
use Tests\TestCase;

/**
 * Class UserControllerTest
 *
 * @package Tests\Feature\Api\Admin
 */
class UserControllerTest extends TestCase
{
    use RefreshDatabase, Authentication;

    public function setUp()
    {
        parent::setUp();

        $this->user = $this->getAuthHeaders(User::ROLE_USER);
    }

    public function testIndexFails()
    {
        $response = $this->withHeaders(['Accept' => 'application/json'])->get('/api/user/profile');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testIndexSuccess()
    {
        $response = $this->withHeaders($this->user)->get('/api/user/profile');

        $response->assertStatus(Response::HTTP_OK);
    }


    public function testUpdateFails()
    {
        $response = $this->withHeaders(['Accept' => 'application/json'])->put('/api/user/profile');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testUpdateFailsValidation()
    {
        $response = $this->withHeaders($this->user)->put('/api/user/profile', ['name' => '']);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdateSuccess()
    {
        $response = $this->withHeaders($this->user)->put('/api/user/profile', ['name' => 'testdfhdshdfhsfdhds']);

        $response->assertStatus(Response::HTTP_OK);
    }
}