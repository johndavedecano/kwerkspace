<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/3/2018
 * Time: 4:40 AM
 */

namespace App\Entities\Others;

use App\Notifications\ForgotPassword;

trait ForgotPasswordEmail
{
    /**
     * Send the password reset notification.
     *
     * @param  string $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ForgotPassword($token));
    }
}