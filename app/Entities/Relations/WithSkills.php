<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 12:24 PM
 */

namespace App\Entities\Relations;

use App\Entities\UserSkill;

/**
 * Trait WithSkills
 *
 * @package App\Entities\Relations
 */
trait WithSkills
{
    /**
     * @return mixed
     */
    public function skills()
    {
        return $this->hasMany(UserSkill::class);
    }
}