<?php

namespace App\Http\Controllers\Admin;

use App\Entities\Inquiry;
use App\Http\Resources\InquiryReplyResource;
use App\Services\Inquiry\InquiryReplyCollection;
use App\Services\Inquiry\InquiryReplyManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class InquiryReplyController
 *
 * @package App\Http\Controllers\Admin
 */
class InquiryReplyController extends Controller
{
    /**
     * @var \App\Services\Inquiry\InquiryReplyCollection
     */
    protected $collection;

    /**
     * @var \App\Services\Inquiry\InquiryReplyManager
     */
    protected $manager;

    /**
     * InquiryReplyController constructor.
     *
     * @param \App\Services\Inquiry\InquiryReplyCollection $collection
     * @param \App\Services\Inquiry\InquiryReplyManager $manager
     */
    public function __construct(InquiryReplyCollection $collection, InquiryReplyManager $manager)
    {
        $this->manager = $manager;

        $this->collection = $collection;
    }

    /**
     * @param $inquiryId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index($inquiryId)
    {
        $results = $this->collection->listByInquiry($inquiryId);

        return InquiryReplyResource::collection($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $inquiryId
     * @return \App\Http\Resources\InquiryReplyResource
     */
    public function store(Request $request, $inquiryId)
    {
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
            'message' => 'required',
        ]);

        $results = $this->manager->create($inquiryId, [
            'user_id' => $request->get('user_id'),
            'message' => $request->get('message'),
        ]);

        return new InquiryReplyResource($results);
    }

    /**
     * @param $inquiryId
     * @param $id
     * @return \App\Http\Resources\InquiryReplyResource
     */
    public function show($inquiryId, $id)
    {
        $results = $this->collection->findByInquiry($inquiryId, $id);

        return new InquiryReplyResource($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $inquiryId
     * @param $id
     * @return \App\Http\Resources\InquiryReplyResource
     */
    public function update(Request $request, $inquiryId, $id)
    {
        $this->validate($request, [
            'user_id' => 'sometimes|required|exists:users,id',
            'message' => 'sometimes|required',
        ]);

        $this->manager->update($inquiryId, $id, $request->all());

        return $this->show($inquiryId, $id);
    }

    /**
     * @param $inquiryId
     * @param $id
     * @return \App\Http\Resources\InquiryReplyResource
     */
    public function destroy($inquiryId, $id)
    {
        $this->manager->delete($inquiryId, $id);

        return new InquiryReplyResource(['id' => $id]);
    }
}
