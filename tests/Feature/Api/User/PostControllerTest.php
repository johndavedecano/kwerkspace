<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/6/2018
 * Time: 7:46 PM
 */

namespace Tests\Feature\Api\User;

use App\Core\Constants;
use App\Core\Types\FakeImage;
use App\Entities\Category;
use App\Entities\Country;
use App\Entities\Feature;
use App\Entities\Post;
use App\Entities\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\Feature\Api\Authentication;
use Tests\TestCase;

/**'
 * Class PostControllerTest
 *
 * @package Tests\Feature\Api\Admin
 */
class PostControllerTest extends TestCase
{
    use RefreshDatabase, Authentication;

    public function setUp()
    {
        parent::setUp();

        $this->user = $this->getAuthHeaders(User::ROLE_USER);
    }

    public function testIndexFails()
    {
        $response = $this->withHeaders(['Accept' => 'application/json'])->get('/api/user/posts');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testIndexSuccess()
    {
        $response = $this->withHeaders($this->user)->get('/api/user/posts');

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testStoreFails()
    {
        $response = $this->withHeaders([])->post('/api/user/posts');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testStoreValidationFails()
    {
        $response = $this->withHeaders($this->user)->post('/api/user/posts', []);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStoreSuccess()
    {
        $image = new FakeImage();

        $data = [
            'category_id' => factory(Category::class)->create()->id,
            'country_id' => factory(Country::class)->create()->id,
            'title' => 'The quick brown fox jumps over the lazy dogs',
            'description' => 'The quick brown fox jumps over the lazy dogs',
            'status' => Constants::STATUS_ACTIVE,
            'thumbnail' => json_encode($image()),
            'images' => [json_encode($image())],
            'features' => [factory(Feature::class)->create()->id],
            'cover' => json_encode($image()),
            'area' => 25,
            'max_occupants' => 100,
            'payment_type' => 'hourly',
            'payment_amount' => 10.25,
            'address' => 'address',
            'city' => 'city',
            'state' => 'state',
            'zipcode' => 'zipcode',
            'lon' => 25.56,
            'lat' => 34.67,
        ];

        $response = $this->withHeaders($this->user)->post('/api/user/posts', $data);

        $response->assertStatus(Response::HTTP_CREATED);
    }

    public function testUpdateFails()
    {
        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $response = $this->withHeaders([])->put('/api/user/posts/'.$post->id, []);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testUpdateFailsNotFound()
    {
        $response = $this->withHeaders($this->user)->put('/api/user/posts/2346262362', []);

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testUpdateFailsValidation()
    {
        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $data = [
            'area' => '363636',
            'max_occupants' => 'gdgsdgsg',
            'lon' => 25.56,
            'lat' => 34.67,
        ];

        $response = $this->withHeaders($this->user)->put('/api/user/posts/'.$post->id, $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdateSuccess()
    {
        $image = new FakeImage();

        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $data = [
            'category_id' => factory(Category::class)->create()->id,
            'country_id' => factory(Country::class)->create()->id,
            'title' => 'The quick brown fox jumps over the lazy dogsThe quick brown fox jumps over the lazy dogs',
            'description' => 'The quick brown fox jumps over the lazy dogsThe quick brown fox jumps over the lazy dogsThe quick brown fox jumps over the lazy dogsThe quick brown fox jumps over the lazy dogs',
            'status' => Constants::STATUS_ACTIVE,
            'thumbnail' => json_encode($image()),
            'images' => [json_encode($image())],
            'features' => [factory(Feature::class)->create()->id],
            'cover' => json_encode($image()),
            'area' => 25,
            'max_occupants' => 100,
            'payment_type' => 'hourly',
            'payment_amount' => 10.25,
            'address' => 'address',
            'city' => 'city',
            'state' => 'state',
            'zipcode' => 'zipcode',
            'lon' => 25.56,
            'lat' => 34.67,
        ];

        $response = $this->withHeaders($this->user)->put('/api/user/posts/'.$post->id, $data);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testDestroyFails()
    {
        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $response = $this->withHeaders([])->delete('/api/user/posts/'.$post->id);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testDestroyFailsNotFound()
    {
        $response = $this->withHeaders($this->user)->delete('/api/user/posts/23525');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testDestroySuccess()
    {
        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $response = $this->withHeaders($this->user)->delete('/api/user/posts/'.$post->id);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testShowFails()
    {
        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $response = $this->withHeaders([])->get('/api/user/posts/'.$post->id);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testShowFailsNotFound()
    {
        $response = $this->withHeaders($this->user)->get('/api/user/posts/23623623623623');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testShowSuccess()
    {
        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $response = $this->withHeaders($this->user)->get('/api/user/posts/'.$post->id);

        $response->assertStatus(Response::HTTP_OK);
    }
}