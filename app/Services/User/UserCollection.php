<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:20 PM
 */

namespace App\Services\User;

use App\Core\Abstracts\AbstractCollection;
use App\Core\Constants;
use App\Core\Interfaces\CollectionInterface;
use App\Entities\User;
use App\Services\Traits\WithFindSlug;
use Spatie\QueryBuilder\Filter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class UserCollection
 *
 * @package App\Services\User
 */
class UserCollection extends AbstractCollection implements CollectionInterface
{
    use WithFindSlug;

    /**
     * UserCollection constructor.
     *
     * @param \App\Entities\User $model
     */
    public function __construct(User $model)
    {
        $this->builder = QueryBuilder::for(User::class);

        $this->model = $model;
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function listFromAdmin()
    {
        return $this->builder
            ->defaultSort('-created_at')
            ->allowedFilters([
                Filter::partial('name'),
                Filter::exact('city'),
                Filter::exact('role'),
                Filter::exact('status'),
            ])
            ->with(['country'])
            ->paginate(Constants::PAGINATION_LIMIT);
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function listFromCommon()
    {
        return $this->builder
            ->where('is_public', '=',true)
            ->where('role', '=', User::ROLE_USER)
            ->where('status', '<>', Constants::STATUS_DELETED)
            ->defaultSort('-created_at')
            ->allowedFilters([
                Filter::partial('name'),
                Filter::exact('country_id'),
                Filter::exact('city')
            ])
            ->with(['country'])
            ->paginate(Constants::PAGINATION_LIMIT);
    }
}