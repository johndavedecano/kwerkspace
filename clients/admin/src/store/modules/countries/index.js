// countries module
import { load, show, destroy, update, create } from '@/api/countries-api'
import { notify } from '@/shared/notify'
import merge from 'lodash/merge'

import {
  COUNTRY_LOAD,
  COUNTRY_LOAD_SUCCESS,
  COUNTRY_LOAD_FAILURE,
} from '@/store/constants'

const countries = {
  namespaced: true,
  state: {
    items: [],
    meta: {},
    isLoading: false,
    isLoaded: false,
  },
  actions: {
    async loadCountry({ commit }, params = {}) {
      try {
        commit(COUNTRY_LOAD, params)
        const { data, meta } = await load(params)
        commit(COUNTRY_LOAD_SUCCESS, { data, meta })
      } catch (err) {
        commit(COUNTRY_LOAD_FAILURE)
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
    async showCountry(_, id) {
      try {
        const { data } = await show(id)
        return data
      } catch (err) {
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
    async destroyCountry(_, id) {
      try {
        const { data } = await destroy(id)
        notify({ type: 'success', text: 'Succesfully Deleted' })
        return data
      } catch (err) {
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
    async updateCountry(_, { id, params }) {
      try {
        const { data } = await update(id, params)
        notify({ type: 'success', text: 'Succesfully Updated' })
        return data
      } catch (err) {
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
    async createCountry(_, params) {
      try {
        const { data } = await create(params)
        notify({ type: 'success', text: 'Succesfully Created' })
        return data
      } catch (err) {
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
  },
  mutations: {
    [COUNTRY_LOAD]: (state, params) => {
      state.isLoading = true
      state.meta = merge(state.meta, params)
    },
    [COUNTRY_LOAD_SUCCESS]: (state, { meta, data }) => {
      state.isLoading = false
      state.isLoaded = true
      state.items = data
      state.meta = merge(state.meta, meta)
    },
    [COUNTRY_LOAD_FAILURE]: state => {
      state.isLoading = false
      state.isLoaded = false
      state.items = []
      state.meta = {}
    },
  },
  getters: {
    isLoaded: state => state.isLoaded,
    isLoading: state => state.isLoading,
    items: state => state.items,
    meta: state => state.meta,
  },
}

export default countries
