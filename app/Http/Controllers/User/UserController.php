<?php

namespace App\Http\Controllers\User;

use App\Http\Resources\UserResource;
use App\Services\User\UserCollection;
use App\Services\User\UserManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class UserController
 *
 * @package App\Http\Controllers\User
 */
class UserController extends Controller
{
    /**
     * @var \App\Services\User\UserManager
     */
    public $manager;

    /**
     * @var \App\Services\User\UserCollection
     */
    public $collection;

    /**
     * UserController constructor.
     *
     * @param \App\Services\User\UserCollection $collection
     * @param \App\Services\User\UserManager $manager
     */
    public function __construct(UserCollection $collection, UserManager $manager)
    {
        $this->manager = $manager;

        $this->collection = $collection;
    }

    /**
     * @return \App\Http\Resources\UserResource
     */
    public function index()
    {
        $results = auth()->guard()->user();

        return new UserResource($results);
    }

    /**]
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\UserResource
     */
    public function update(Request $request)
    {
        $this->validate($request, [
            'name' => 'sometimes|required|min:5',
            'password' => 'sometimes|required|confirmed|min:6|max:16',
            'country_id' => 'sometimes|required|exists:countries,id',
            'status' => 'sometimes|required|in:active,inactive,deleted',
            'role' => 'sometimes|required|in:admin,company,user',
            'city' => 'sometimes|required|min:6|max:100',
            'lat' => 'sometimes|required|numeric',
            'lon' => 'sometimes|required|numeric',
            'linkedin' => 'sometimes|required|url',
            'twitter' => 'sometimes|required|url',
            'facebook' => 'sometimes|required|url',
            'website' => 'sometimes|required|url',
            'youtube' => 'sometimes|required|url',
            'about' => 'sometimes|required|min:140|max:1000',
            'is_public' => 'sometimes|required|boolean',
            'title' => 'sometimes|required|min:20',
        ]);

        $this->manager->update(auth()->id(), $request->all());

        return $this->index();
    }
}
