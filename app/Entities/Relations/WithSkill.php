<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 12:24 PM
 */

namespace App\Entities\Relations;

use App\Entities\Skill;

/**
 * Trait WithSkills
 *
 * @package App\Entities\Relations
 */
trait WithSkill
{
    /**
     * @return mixed
     */
    public function skill()
    {
        return $this->belongsTo(Skill::class);
    }
}