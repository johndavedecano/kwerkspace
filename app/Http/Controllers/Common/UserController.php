<?php

namespace App\Http\Controllers\Common;

use App\Http\Resources\UserCommonResource;
use App\Services\User\UserCollection;
use App\Http\Controllers\Controller;

/**
 * Class UserController
 *
 * @package App\Http\Controllers\Common
 */
class UserController extends Controller
{
    /**
     * @var \App\Services\User\UserCollection
     */
    public $collection;

    /**
     * UserController constructor.
     *
     * @param \App\Services\User\UserCollection $collection
     */
    public function __construct(UserCollection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $collection = $this->collection->listFromCommon();

        return UserCommonResource::collection($collection);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\UserCommonResource
     */
    public function show($id)
    {
        $results = $this->collection->findNotDeleted($id);

        return new UserCommonResource($results);
    }
}
