<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsImagesTable extends Migration {

	public function up()
	{
		Schema::create('posts_images', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('post_id')->unsigned()->nullable();
			$table->text('files')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('posts_images');
	}
}