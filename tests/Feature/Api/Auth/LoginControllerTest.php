<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/8/2018
 * Time: 7:32 PM
 */

namespace Tests\Feature\Api\Auth;

use App\Entities\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class LoginControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testStoreFailsValidation()
    {
        $response = $this->post('api/auth/login');

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStoreFailsEmailDoesNotExists()
    {
        $response = $this->post('api/auth/login', ['email' => 'email@doesnotexists.com', 'password' => 'password']);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStoreSuccess()
    {
        $user = factory(User::class)->create(['email' => 'test@gmail.com', 'password' => 'password']);

        $response = $this->post('api/auth/login', ['email' => $user->email, 'password' => 'password']);

        $response->assertOk();
    }
}