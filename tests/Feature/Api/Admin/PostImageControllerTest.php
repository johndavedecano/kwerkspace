<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/6/2018
 * Time: 7:46 PM
 */

namespace Tests\Feature\Api\Admin;

use App\Core\Types\FakeImage;
use App\Entities\Post;
use App\Entities\PostsImage;
use App\Entities\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\Feature\Api\Authentication;
use Tests\TestCase;

/**
 * Class PostImageControllerTest
 *
 * @package Tests\Feature\Api\Admin
 */
class PostImageControllerTest extends TestCase
{
    use RefreshDatabase, Authentication;

    public function setUp()
    {
        parent::setUp();

        $this->admin = $this->getAuthHeaders(User::ROLE_ADMIN);

        $this->user = $this->getAuthHeaders(User::ROLE_USER);
    }

    public function testIndexFails()
    {
        $response = $this->withHeaders(['Accept' => 'application/json'])->get('/api/admin/posts/1/images');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testIndexFailsNotAllowed()
    {
        $response = $this->withHeaders($this->user)->get('/api/admin/posts/1/images');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testIndexSuccess()
    {
        $post = factory(Post::class)->create();

        $response = $this->withHeaders($this->admin)->get('/api/admin/posts/'.$post->id.'/images');

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testStoreFails()
    {
        $response = $this->withHeaders([])->post('/api/admin/posts/346363/images');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testStoreFailsNotFound()
    {
        $image = new FakeImage();

        $data = ['files' => json_encode($image())];

        $response = $this->withHeaders($this->admin)->post('/api/admin/posts/112323522/images', $data);

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testStoreValidationFails()
    {
        $post = factory(Post::class)->create();

        $response = $this->withHeaders($this->admin)->post('/api/admin/posts/'.$post->id.'/images', []);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStoreSuccess()
    {
        $post = factory(Post::class)->create();

        $image = new FakeImage();

        $data = ['files' => json_encode($image())];

        $response = $this->withHeaders($this->admin)->post('/api/admin/posts/'.$post->id.'/images', $data);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testUpdateFails()
    {
        $post = factory(Post::class)->create();

        $response = $this->withHeaders([])->put('/api/admin/posts/'.$post->id.'/images', []);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testUpdateFailsNotAdmin()
    {
        $post = factory(Post::class)->create();

        $response = $this->withHeaders($this->user)->put('/api/admin/posts/'.$post->id.'/images', []);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testUpdateFailsNotFound()
    {
        $image = new FakeImage();

        $files = json_encode($image());

        $response = $this->withHeaders($this->admin)->put('/api/admin/posts/65444646546/images', ['files' => $files]);

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testUpdateSuccess()
    {
        $post = factory(Post::class)->create();

        $image = new FakeImage();

        $files = json_encode($image());

        $data = ['files' => [$files]];

        $response = $this->withHeaders($this->admin)->put('/api/admin/posts/'.$post->id.'/images', $data);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testUpdateSingleSuccess()
    {
        $post = factory(Post::class)->create();

        $image = new FakeImage();

        $files = json_encode($image());

        $data = ['files' => $files];

        $response = $this->withHeaders($this->admin)->put('/api/admin/posts/'.$post->id.'/images', $data);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testUpdateFailsBadImageStructure()
    {
        $post = factory(Post::class)->create();

        $data = ['files' => ''];

        $response = $this->withHeaders($this->admin)->put('/api/admin/posts/'.$post->id.'/images', $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testDestroyFails()
    {
        $post = factory(Post::class)->create();

        $image = factory(PostsImage::class)->create();

        $response = $this->withHeaders([])->delete('/api/admin/posts/'.$post->id.'/images/'.$image->id);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testDestroyFailsNotFound()
    {
        $response = $this->withHeaders($this->admin)->delete('/api/admin/posts/262362362362/images/23623623623632');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testDestroySuccess()
    {
        $post = factory(Post::class)->create();

        $image = factory(PostsImage::class)->create();

        $response = $this->withHeaders($this->admin)->delete('/api/admin/posts/'.$post->id.'/images/'.$image->id);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testShowFails()
    {
        $post = factory(Post::class)->create();

        $image = factory(PostsImage::class)->create();

        $response = $this->withHeaders([])->delete('/api/admin/posts/'.$post->id.'/images/'.$image->id);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testShowFailsNotFound()
    {
        $response = $this->withHeaders($this->admin)->delete('/api/admin/posts/262362362362/images/23623623623632');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testShowSuccess()
    {
        $post = factory(Post::class)->create();

        $image = factory(PostsImage::class)->create();

        $response = $this->withHeaders($this->admin)->delete('/api/admin/posts/'.$post->id.'/images/'.$image->id);

        $response->assertStatus(Response::HTTP_OK);
    }
}