<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:20 PM
 */

namespace App\Services\Post;

use App\Core\Abstracts\AbstractCollection;
use App\Core\Constants;
use App\Core\Interfaces\CollectionInterface;
use App\Entities\Post;
use App\Services\Traits\WithFindSlug;
use Spatie\QueryBuilder\Filter;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class PostCollection
 *
 * @package App\Services\Post
 */
class PostCollection extends AbstractCollection implements CollectionInterface
{
    use WithFindSlug;

    const USER_FIELDS = ['name', 'slug', 'id', 'avatar', 'company', 'company_logo'];

    /**
     * PostCollection constructor.
     *
     * @param \App\Entities\Post $model
     */
    public function __construct(Post $model)
    {
        $this->builder = QueryBuilder::for(Post::class);

        $this->model = $model;
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function listFromAdmin()
    {
        return $this->builder
            ->with([
                'user' => function($query) {
                    return $query->select(self::USER_FIELDS);
                }
            ])
            ->with('features')
            ->with('category')
            ->with('country')
            ->defaultSort('-created_at')
            ->allowedFilters([
                'category_id',
                'user_id',
                'country_id',
                'city',
                'state'
            ])
            ->paginate(Constants::PAGINATION_LIMIT);
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function list()
    {
        return $this->builder
            ->with([
                'user' => function($query) {
                    return $query->select(self::USER_FIELDS);
                }
            ])
            ->withCount(['likes', 'reviews'])
            ->with('features')
            ->with('category')
            ->with('country')
            ->defaultSort('-created_at')
            ->allowedFilters([
                Filter::scope('not_deleted'),
                'category_id',
                'user_id',
                'country_id',
                'city',
                'state'
            ])
            ->paginate(Constants::PAGINATION_LIMIT);
    }

    /**
     * @param $userId
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function listByUser($userId)
    {
        return $this->builder
            ->with([
                'user' => function($query) {
                    return $query->select(self::USER_FIELDS);
                }
            ])
            ->with('features')
            ->with('category')
            ->with('country')
            ->where('user_id', $userId)
            ->defaultSort('-created_at')
            ->allowedFilters([
                Filter::scope('not_deleted'),
                'category_id',
                'country_id',
                'city',
                'state'
            ])
            ->paginate(Constants::PAGINATION_LIMIT);
    }

    /**
     * @param $userId
     * @param $postId
     * @return mixed
     */
    public function findPostByUser($userId, $postId)
    {
        return $this->model->where('user_id', $userId)->where('id', $postId)->firstOrFail();
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|\Spatie\QueryBuilder\QueryBuilder
     */
    public function findFromCommon($id)
    {
        return $this->builder
            ->with([
                'user' => function($query) {
                    return $query->select(self::USER_FIELDS);
                }
            ])
            ->withCount(['reviews', 'likes'])
            ->with('features')
            ->with('category')
            ->with('country')
            ->where('id', $id)
            ->where('status', '<>', Constants::STATUS_DELETED)
            ->firstOrFail();
    }

    /**
     * @param $userId
     * @param $id
     * @return \Illuminate\Database\Eloquent\Model|\Spatie\QueryBuilder\QueryBuilder
     */
    public function findByUserFromCommon($userId, $id)
    {
        return $this->builder
            ->with([
                'user' => function($query) {
                    return $query->select(self::USER_FIELDS);
                }
            ])
            ->withCount(['reviews', 'likes'])
            ->with('features')
            ->with('category')
            ->with('country')
            ->where('id', $id)
            ->where('user_id', $userId)
            ->where('status', '<>', Constants::STATUS_DELETED)
            ->firstOrFail();
    }
}