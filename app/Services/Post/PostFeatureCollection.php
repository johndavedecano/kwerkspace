<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/30/2018
 * Time: 7:50 AM
 */

namespace App\Services\Post;

use App\Core\Abstracts\AbstractCollection;
use App\Core\Interfaces\CollectionInterface;
use App\Entities\Post;
use App\Entities\PostsFeature;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class PostFeatureCollection
 *
 * @package App\Services\Post
 */
class PostFeatureCollection extends AbstractCollection implements CollectionInterface
{
    /**
     * @var \App\Entities\Post
     */
    protected $post;

    /**
     * PostFeatureCollection constructor.
     *
     * @param \App\Entities\PostsFeature $model
     * @param \App\Entities\Post $post
     */
    public function __construct(PostsFeature $model, Post $post)
    {
        $this->model = $model;

        $this->post = $post;

        $this->builder = QueryBuilder::for(PostsFeature::class);
    }

    /**
     * @param $userId
     * @param $postId
     * @return \Illuminate\Database\Eloquent\Collection|\Spatie\QueryBuilder\QueryBuilder[]
     */
    public function listByUserAndPost($userId, $postId)
    {
        return $this->post
            ->where(['user_id' => $userId, 'id' => $postId])
            ->firstOrFail()
            ->features()
            ->get();
    }

    /**
     * @param $userId
     * @param $postId
     * @param $featureId
     * @return \App\Entities\PostsFeature|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function findByUserAndPost($userId, $postId, $featureId)
    {
        return $this->post
            ->where(
                [
                    'user_id' => $userId,
                    'id' => $postId
                ]
            )
            ->firstOrFail()
            ->features()
            ->where('id', $featureId)
            ->firstOrFail();
    }

    /**
     * @param $postId
     * @return \Illuminate\Database\Eloquent\Collection|\Spatie\QueryBuilder\QueryBuilder[]
     */
    public function listByPost($postId)
    {
        return $this->builder->with('feature')->where('post_id', $postId)->get();
    }

    /**
     * @param $postId
     * @param $featureId
     * @return mixed
     */
    public function findByPost($postId, $featureId)
    {
        return $this->post->findOrFail($postId)->features()->findOrFail($featureId);
    }
}