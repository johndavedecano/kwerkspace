<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/9/2018
 * Time: 3:53 PM
 */

namespace Tests\Post\Api\Common;

use App\Entities\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PostControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testIndexSuccess()
    {
        $response = $this->get('api/common/posts');

        $response->assertOk();
    }

    public function testShowSuccess()
    {
        $result = factory(Post::class)->create();

        $response = $this->get('api/common/posts/'.$result->id);

        $response->assertOk();
    }
}