<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:21 PM
 */

namespace App\Services\Post;

use App\Core\Abstracts\AbstractManager;
use App\Core\Types\Image;
use App\Entities\Post;
use App\Entities\PostsImage;

/**
 * Class PostImageManager
 *
 * @package App\Services\Post
 */
class PostImageManager extends AbstractManager
{
    /**
     * @var \App\Entities\PostsImage
     */
    public $model;

    /**
     * @var \App\Entities\Post
     */
    protected $post;

    /**
     * PostImageManager constructor.
     *
     * @param \App\Entities\PostsImage $model
     * @param \App\Entities\Post $post
     */
    public function __construct(PostsImage $model, Post $post)
    {
        $this->model = $model;

        $this->post = $post;
    }

    /**
     * @param $userId
     * @param $postId
     * @param $files
     * @return mixed
     */
    public function createByUser($userId, $postId, $files)
    {
        $post = $this->post->where(['user_id' => $userId, 'id' => $postId])->firstOrFail();

        if (is_array($files)) {
            foreach ($files as $file) {
                $this->saveImage($file, $post);
            }
        } else {
            $this->saveImage($files, $post);
        }

        return $post->images()->get();
    }

    /**
     * @param $postId
     * @param $files
     * @return \App\Entities\PostsImage
     */
    public function create($postId, $files)
    {
        $post = $this->post->findOrFail($postId);

        if (is_array($files)) {
            foreach ($files as $file) {
                $this->saveImage($file, $post);
            }
        } else {
            $this->saveImage($files, $post);
        }

        return $post->images()->get();
    }

    /**
     * @param $postId
     * @param $files
     * @return mixed
     */
    public function update($postId, $files)
    {
        $post = $this->post->findOrFail($postId);

        $this->saveItems($files, $post);

        return $post;
    }

    /**
     * @param $userId
     * @param $postId
     * @param $files
     * @return mixed
     */
    public function updateByUser($userId, $postId, $files)
    {
        $params = ['user_id' => $userId, 'id' => $postId];

        $post= $this->post->where($params)->firstOrFail();

        $this->saveItems($files, $post);

        return $post;
    }

    /**
     * @param $id
     * @return bool|null
     * @throws \Exception
     */
    public function delete($id)
    {
        return $this->model->delete($id);
    }

    /**
     * @param $postId
     * @param $id
     * @return mixed
     */
    public function deleteByPost($postId, $id)
    {
        return $this->post->findOrfail($postId)->images()->where('id', $id)->delete();
    }

    /**
     * @param $userId
     * @param $postId
     * @param $id
     * @return mixed
     */
    public function deleteByUser($userId, $postId, $id)
    {
        return $this->post->where([
            'user_id' => $userId,
            'id' => $postId
        ])
        ->firstOrFail()
        ->images()
        ->where(['id' => $id])
        ->delete();
    }

    /**
     * @param $files
     * @param $post
     * @return \App\Entities\PostsImage|void
     */
    protected function saveImage($files, $post)
    {
        try {
            $files = new Image($files);

            $image = new PostsImage(['files' => $files->__toString()]);

            $post->images()->save($image);

            return $image;
        } catch(\Exception $exception) {
            return;
        }
    }

    /**
     * @param $files
     * @param $post
     */
    protected function saveItems($files, $post): void
    {
        $items = [];

        if (is_array($files)) {
            foreach ($files as $file) {
                $image = new Image($file);
                $items[] = new PostsImage(['files' => $image->__toString()]);
            }
        } else {
            $image = new Image($files);
            $items[] = new PostsImage(['files' => $image->__toString()]);
        }

        $post->images()->delete();

        $post->images()->saveMany($items);
    }
}