<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:21 PM
 */

namespace App\Services\Post;

use App\Core\Abstracts\AbstractManager;
use App\Entities\Post;
use App\Entities\PostsLike;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class PostLikeManager
 *
 * @package App\Services\Post
 */
class PostLikeManager extends AbstractManager
{
    /**
     * @var \App\Entities\PostsLike
     */
    public $model;

    protected $post;

    /**
     * PostLikeManager constructor.
     *
     * @param \App\Entities\PostsLike $model
     * @param \App\Entities\Post $post
     */
    public function __construct(PostsLike $model, Post $post)
    {
        $this->model = $model;

        $this->post = $post;
    }

    /**
     * @param $userId
     * @param $postId
     * @return mixed
     */
    public function like($userId, $postId)
    {
        $this->post->findOrFail($postId);

        $builder = $this->model->where(['user_id' => $userId, 'post_id' => $postId]);

        if ($builder->count() < 1) {
            return $this->model->create(['user_id' => $userId, 'post_id' => $postId]);
        }

        return false;
    }

    /**
     * @param $userId
     * @param $postId
     * @return mixed
     */
    public function unlike($userId, $postId)
    {
        $this->post->findOrFail($postId);

        return $this->model->where(['user_id' => $userId, 'post_id' => $postId])->delete();
    }

    /**
     * @param $postId
     * @param $likeId
     * @return mixed
     */
    public function deleteByPost($postId, $likeId)
    {
        return $this->model->where('post_id', $postId)->where('id', $likeId)->firstOrFail()->delete();
    }
}