<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\PostImageResource;
use App\Services\Post\PostImageCollection;
use App\Services\Post\PostImageManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class PostImageController
 *
 * @package App\Http\Controllers\Admin
 */
class PostImageController extends Controller
{
    /**
     * @var \App\Services\Post\PostImageManager
     */
    protected $manager;

    /**
     * @var \App\Services\Post\PostImageCollection
     */
    protected $collection;

    /**
     * PostImageController constructor.
     *
     * @param \App\Services\Post\PostImageCollection $collection
     * @param \App\Services\Post\PostImageManager $manager
     */
    public function __construct(PostImageCollection $collection, PostImageManager $manager)
    {
        $this->collection = $collection;

        $this->manager = $manager;
    }

    /**
     * @param $postId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index($postId)
    {
        $results = $this->collection->listByPost($postId);

        return PostImageResource::collection($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $postId
     * @return \App\Http\Resources\PostImageResource
     */
    public function store(Request $request, $postId)
    {
        $this->validate($request, ['files' => 'required']);

        $results = $this->manager->create($postId, $request->get('files'));

        return new PostImageResource($results);
    }

    /**
     * @param $postId
     * @param $id
     * @return \App\Http\Resources\PostImageResource
     */
    public function show($postId, $id)
    {
        $results = $this->collection->findByPost($postId, $id);

        return new PostImageResource($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $postId
     * @return \App\Http\Resources\PostImageResource
     */
    public function update(Request $request, $postId)
    {
        $this->validate($request, ['files' => 'required']);

        $results = $this->manager->update($postId, $request->get('files'));

        return new PostImageResource($results);
    }

    /**
     * @param $postId
     * @param $id
     * @return \App\Http\Resources\PostImageResource
     */
    public function destroy($postId, $id)
    {
        $results = $this->manager->deleteByPost($postId, $id);

        return response()->json(['data' => $results]);
    }
}
