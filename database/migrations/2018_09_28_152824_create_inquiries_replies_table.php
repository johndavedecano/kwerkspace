<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInquiriesRepliesTable extends Migration {

	public function up()
	{
		Schema::create('inquiries_replies', function(Blueprint $table) {
			$table->increments('id');
			$table->text('message');
			$table->integer('inquiry_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->integer('post_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('inquiries_replies');
	}
}