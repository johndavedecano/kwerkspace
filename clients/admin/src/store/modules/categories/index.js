// categories module
import { load, show, destroy, update, create } from '@/api/categories-api'
import { notify } from '@/shared/notify'
import merge from 'lodash/merge'

import {
  CATEGORY_LOAD,
  CATEGORY_LOAD_SUCCESS,
  CATEGORY_LOAD_FAILURE,
} from '@/store/constants'

const categories = {
  namespaced: true,
  state: {
    items: [],
    meta: {},
    isLoading: false,
    isLoaded: false,
  },
  actions: {
    async loadCategory({ commit }, params = {}) {
      try {
        commit(CATEGORY_LOAD, params)
        const { data, meta } = await load(params)
        commit(CATEGORY_LOAD_SUCCESS, { data, meta })
      } catch (err) {
        commit(CATEGORY_LOAD_FAILURE)
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
    async showCategory(_, id) {
      try {
        const { data } = await show(id)
        return data
      } catch (err) {
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
    async destroyCategory(_, id) {
      try {
        const { data } = await destroy(id)
        notify({ type: 'success', text: 'Succesfully Deleted' })
        return data
      } catch (err) {
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
    async updateCategory(_, { id, params }) {
      try {
        const { data } = await update(id, params)
        notify({ type: 'success', text: 'Succesfully Updated' })
        return data
      } catch (err) {
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
    async createCategory(_, params) {
      try {
        const { data } = await create(params)
        notify({ type: 'success', text: 'Succesfully Created' })
        return data
      } catch (err) {
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
  },
  mutations: {
    [CATEGORY_LOAD]: (state, params) => {
      state.isLoading = true
      state.meta = merge(state.meta, params)
    },
    [CATEGORY_LOAD_SUCCESS]: (state, { meta, data }) => {
      state.isLoading = false
      state.isLoaded = true
      state.items = data
      state.meta = merge(state.meta, meta)
    },
    [CATEGORY_LOAD_FAILURE]: state => {
      state.isLoading = false
      state.isLoaded = false
      state.items = []
      state.meta = {}
    },
  },
  getters: {
    isLoaded: state => state.isLoaded,
    isLoading: state => state.isLoading,
    items: state => state.items,
    meta: state => state.meta,
  },
}

export default categories
