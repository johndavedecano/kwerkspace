<?php
/**
 * Created by QualityTrade.
 * User: johndavedecano
 * Date: 05/10/2018
 * Time: 2:49 PM
 */

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\SkillResource;
use App\Services\User\UserSkillsCollection;
use App\Services\User\UserSkillsManager;
use Illuminate\Http\Request;

/**
 * Class UserSkillController
 *
 * @package App\Http\Controllers\Admin
 */
class UserSkillController extends Controller
{
    /**
     * @var \App\Services\User\UserSkillsManager
     */
    protected $manager;

    /**
     * @var \App\Services\User\UserSkillsCollection
     */
    protected $collection;

    /**
     * UserSkillController constructor.
     *
     * @param \App\Services\User\UserSkillsCollection $collection
     * @param \App\Services\User\UserSkillsManager $manager
     */
    public function __construct(UserSkillsCollection $collection, UserSkillsManager $manager)
    {
        $this->collection = $collection;

        $this->manager = $manager;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $results = $this->collection->listUserSkills(auth()->id());

        return SkillResource::collection($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'skills' => 'required|array',
            'skills.*.skill_id' => 'required|exists:skills,id',
            'skills.*.experience' => 'required|numeric',
        ]);

        $this->manager->fill(auth()->id(), $request->get('skills'));

        return $this->index();
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $skillId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function update(Request $request, $skillId)
    {
        $this->validate($request, [
            'experience' => 'required|numeric',
        ]);

        $this->manager->attach(auth()->id(), [
            'skill_id' => $skillId,
            'experience' => $request->get('experience', 0),
        ]);

        return $this->index();
    }

    /**
     * @param $skillId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function destroy($skillId)
    {
        $this->manager->detach(auth()->id(), $skillId);

        return $this->index();
    }
}