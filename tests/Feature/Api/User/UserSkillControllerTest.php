<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/6/2018
 * Time: 7:46 PM
 */

namespace Tests\Feature\Api\User;

use App\Entities\Skill;
use App\Entities\User;
use App\Entities\UserSkill;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\Feature\Api\Authentication;
use Tests\TestCase;

/**
 * Class UserSkillControllerTest
 *
 * @package Tests\Feature\Api\Admin
 */
class UserSkillControllerTest extends TestCase
{
    use RefreshDatabase, Authentication;

    public function setUp()
    {
        parent::setUp();

        $this->user = $this->getAuthHeaders(User::ROLE_USER);
    }

    public function testIndexFails()
    {
        $response = $this->withHeaders(['Accept' => 'application/json'])->get('/api/user/skills');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testIndexSuccess()
    {
        $response = $this->withHeaders($this->user)->get('/api/user/skills');

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testStoreFails()
    {
        $response = $this->withHeaders([])->post('/api/user/skills');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testStoreValidationFailsNotFound()
    {
        $data = ['skills' => [
            [
                'skill_id' => 235626526,
                'experience' => 1,
            ]
        ]];

        $response = $this->withHeaders($this->user)->post('/api/user/skills', $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStoreValidationFails()
    {
        $response = $this->withHeaders($this->user)->post('/api/user/skills');

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStoreSuccess()
    {
        $data = ['skills' => [
            [
                'skill_id' => factory(Skill::class)->create()->id,
                'experience' => 1,
            ]
        ]];

        $response = $this->withHeaders($this->user)->post('/api/user/skills', $data);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testUpdateFails()
    {
        $skill = factory(Skill::class)->create();

        $response = $this->withHeaders([])->put('/api/user/skills/'.$skill->id, []);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testUpdateFailsNotFound()
    {
        $data = ['experience' => 1];

        $response = $this->withHeaders($this->user)->put('/api/user/skills/2353252', $data);

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testUpdateSuccess()
    {
        $skill = factory(Skill::class)->create();

        $data = ['experience' => 1];

        $response = $this->withHeaders($this->user)->put('/api/user/skills/'.$skill->id, $data);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testDestroyFails()
    {
        $skill = factory(Skill::class)->create();

        $response = $this->withHeaders([])->delete('/api/user/skills/'.$skill->id);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testDestroyFailsNotFound()
    {
        $response = $this->withHeaders($this->user)->delete('/api/user/skills/23623623623632');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testDestroySuccess()
    {
        $skill = factory(UserSkill::class)->create(['user_id' => $this->auth['user']->id]);

        $response = $this->withHeaders($this->user)->delete('/api/user/skills/'.$skill->id);

        $response->assertStatus(Response::HTTP_OK);
    }
}