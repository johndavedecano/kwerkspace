// users-api.js
import axios from 'axios'

export function load(params = {}) {
  return axios.get('/admin/users', { params }).then(repsonse => repsonse.data)
}

export function show(id) {
  return axios.get(`/admin/users/${id}`).then(repsonse => repsonse.data)
}

export function destroy(id) {
  return axios.delete(`/admin/users/${id}`).then(repsonse => repsonse.data)
}

export function update(id, params = {}) {
  return axios
    .put(`/admin/users/${id}`, params)
    .then(repsonse => repsonse.data)
}

export function create(params = {}) {
  return axios.post('/admin/users', params).then(repsonse => repsonse.data)
}
