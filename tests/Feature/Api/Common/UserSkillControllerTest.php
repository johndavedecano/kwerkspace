<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/9/2018
 * Time: 3:53 PM
 */

namespace Tests\Feature\Api\Common;

use App\Entities\User;
use App\Entities\UserSkill;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserSkillControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testIndexSuccess()
    {
        $entity = factory(User::class)->create();

        factory(UserSkill::class,  10)->create(['user_id' => $entity->id]);

        $response = $this->get('api/common/users/'.$entity->id.'/skills');

        $response->assertOk();
    }

    public function testShowSuccess()
    {
        $entity = factory(User::class)->create();

        $relation = factory(UserSkill::class)->create(['user_id' => $entity->id]);

        $response = $this->get('api/common/users/'.$entity->id.'/skills/'.$relation->skill_id);

        $response->assertOk();
    }
}