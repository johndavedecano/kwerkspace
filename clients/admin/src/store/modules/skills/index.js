// skills module
import { load, show, destroy, update, create } from '@/api/skills-api'
import { notify } from '@/shared/notify'
import merge from 'lodash/merge'

import {
  SKILL_LOAD,
  SKILL_LOAD_SUCCESS,
  SKILL_LOAD_FAILURE,
} from '@/store/constants'

const authentication = {
  namespaced: true,
  state: {
    items: [],
    meta: {},
    isLoading: false,
    isLoaded: false,
  },
  actions: {
    async loadSkill({ commit }, params = {}) {
      try {
        commit(SKILL_LOAD, params)
        const { data, meta } = await load(params)
        commit(SKILL_LOAD_SUCCESS, { data, meta })
      } catch (err) {
        commit(SKILL_LOAD_FAILURE)
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
    async showSkill(_, id) {
      try {
        const { data } = await show(id)
        return data
      } catch (err) {
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
    async destroySkill(_, id) {
      try {
        const { data } = await destroy(id)
        notify({ type: 'success', text: 'Succesfully Deleted' })
        return data
      } catch (err) {
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
    async updateSkill(_, { id, params }) {
      try {
        const { data } = await update(id, params)
        notify({ type: 'success', text: 'Succesfully Updated' })
        return data
      } catch (err) {
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
    async createSkill(_, params) {
      try {
        const { data } = await create(params)
        notify({ type: 'success', text: 'Succesfully Created' })
        return data
      } catch (err) {
        notify({ type: 'error', text: err.message })
        throw err
      }
    },
  },
  mutations: {
    [SKILL_LOAD]: (state, params) => {
      state.isLoading = true
      state.meta = merge(state.meta, params)
    },
    [SKILL_LOAD_SUCCESS]: (state, { meta, data }) => {
      state.isLoading = false
      state.isLoaded = true
      state.items = data
      state.meta = merge(state.meta, meta)
    },
    [SKILL_LOAD_FAILURE]: state => {
      state.isLoading = false
      state.isLoaded = false
      state.items = []
      state.meta = {}
    },
  },
  getters: {
    isLoaded: state => state.isLoaded,
    isLoading: state => state.isLoading,
    items: state => state.items,
    meta: state => state.meta,
  },
}

export default authentication
