import get from 'lodash/get'
import has from 'lodash/has'

export class ValidationError {
  errors = {}

  constructor(errors = {}) {
    this.errors = errors
  }

  has(key) {
    return has(this.errors, key)
  }

  first(key) {
    return this.has(key) ? get(this.errors, `${key}[0]`, null) : null
  }

  get(key) {
    return get(this.errors, key)
  }
}
