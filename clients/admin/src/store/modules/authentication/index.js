// authentication module
import { login } from '@/api/auth-api'
import { user, token, initToken, removeToken } from '@/shared/auth'
import { notify } from '@/shared/notify'
import { LOGIN, LOGIN_FAILURE, LOGIN_SUCCESS, LOGOUT } from '@/store/constants'

const currentUser = user()
const currentToken = token()
const isLoggedIn = !!currentUser && !!currentToken

const authentication = {
  state: {
    isLoggingIn: false,
    isLoggedIn: isLoggedIn,
    user: currentUser
  },
  namespaced: true,
  actions: {
    async login({ commit }, { email, password }) {
      try {
        commit(LOGIN)
        const { token, user } = await login(email, password)
        initToken({ token, user })
        commit(LOGIN_SUCCESS, { user, isLoggedIn: true, isLoggingIn: false })
        notify({
          type: 'success',
          text: 'Successfully Logged In'
        })
      } catch (err) {
        commit(LOGIN_FAILURE)
        notify({
          timeout: 1000,
          type: 'error',
          text: 'Invalid email or password'
        })
        return Promise.reject(err)
      }
    },
    logout({ commit }) {
      removeToken()
      commit(LOGOUT)
    }
  },
  mutations: {
    [LOGIN]: state => {
      state.isLoggingIn = true
    },
    [LOGIN_FAILURE]: state => {
      state.isLoggingIn = false
    },
    [LOGIN_SUCCESS]: (state, { user, isLoggedIn, isLoggingIn }) => {
      state.isLoggingIn = isLoggingIn
      state.isLoggedIn = isLoggedIn
      state.user = user
    },
    [LOGOUT]: state => {
      state.isLoggingIn = false
      state.isLoggedIn = false
      state.user = null
    }
  }
}

export default authentication
