<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/9/2018
 * Time: 3:53 PM
 */

namespace Tests\Feature\Api\Common;

use App\Entities\Country;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CountryControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testIndexSuccess()
    {
        $response = $this->get('api/common/countries');

        $response->assertOk();
    }

    public function testShowSuccess()
    {
        $result = factory(Country::class)->create();

        $response = $this->get('api/common/countries/'.$result->id);

        $response->assertOk();
    }
}