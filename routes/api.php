<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'common', 'namespace' => 'Common', 'as' => 'common.'], function () {
    Route::resource('categories', 'CategoryController')->only(['index', 'show']);
    Route::resource('countries', 'CountryController')->only(['index', 'show']);
    Route::resource('features', 'FeatureController')->only(['index', 'show']);
    Route::resource('skills', 'SkillController')->only(['index', 'show']);
    Route::resource('users', 'UserController')->only(['index', 'show']);
    Route::resource('users.posts', 'UserPostController')->only(['index', 'show']);
    Route::resource('users.skills', 'UserSkillController')->only(['index', 'show']);
    Route::resource('posts', 'PostController')->only(['index', 'show']);
    Route::resource('posts.images', 'PostImageController')->only(['index', 'show']);
    Route::resource('posts.features', 'PostFeatureController')->only(['index', 'show']);
    Route::resource('posts.likes', 'PostLikeController')->only(['index', 'show']);
    Route::resource('posts.reviews', 'PostReviewController')->only(['index', 'show']);
});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth:api', 'admin'], 'as' => 'admin.'], function () {
    Route::resource('categories', 'CategoryController')->except(['edit', 'create']);
    Route::resource('countries', 'CountryController')->except(['edit', 'create']);
    Route::resource('features', 'FeatureController')->except(['edit', 'create']);
    Route::resource('inquiries', 'InquiryController')->except(['edit', 'create']);
    Route::resource('inquiries.replies', 'InquiryReplyController')->except(['edit', 'create']);
    Route::resource('users.skills', 'UserSkillController')->except(['edit', 'create']);
    Route::resource('users', 'UserController')->except(['edit', 'create']);
    Route::resource('skills', 'SkillController')->except(['edit', 'create']);
    Route::resource('reviews', 'ReviewController')->except(['edit', 'create']);
    Route::resource('posts', 'PostController')->except(['edit', 'create']);
    Route::resource('posts.images', 'PostImageController')->except(['update', 'edit', 'create']);
    Route::put('posts/{post}/images', 'PostImageController@update')->name('posts.images.update');
    Route::resource('posts.features', 'PostFeatureController')->except(['show', 'edit', 'create']);
    Route::resource('posts.likes', 'PostLikeController')->except(['show', 'edit', 'create']);
    Route::post('upload', 'UploadController@upload')->name('upload');

    Route::get('notifications', 'NotificationController@index')->name('index');
    Route::put('notifications', 'NotificationController@readAll')->name('read.all');
    Route::put('notifications/{notification}', 'NotificationController@read')->name('read');
    Route::delete('notifications', 'NotificationController@destroyAll')->name('destroy.all');
    Route::delete('notifications/{notification}', 'NotificationController@destroy')->name('destroy');
});

Route::group(['prefix' => 'user', 'namespace' => 'User', 'middleware' => 'auth:api', 'as' => 'user.'], function () {
    Route::get('profile', 'UserController@index')->name('profile.show');
    Route::put('profile', 'UserController@update')->name('profile.update');
    Route::resource('notifications', 'NotificationController')->except(['create', 'edit']);
    Route::resource('inquiries', 'InquiryController')->except(['create', 'edit']);
    Route::resource('inquiries.replies', 'InquiryReplyController')->except(['create', 'edit']);
    Route::resource('reviews', 'ReviewController')->except(['create', 'edit']);
    Route::resource('posts.images', 'PostImageController')->except(['update', 'edit', 'create']);
    Route::put('posts/{post}/images', 'PostImageController@update')->name('posts.images.update');
    Route::resource('posts.features', 'PostFeatureController')->except(['create', 'edit']);
    Route::post('posts/{post}/likes', 'PostLikeController@store')->name('posts.likes.store');
    Route::delete('posts/{post}/likes', 'PostLikeController@destroy')->name('posts.likes.destroy');
    Route::resource('posts', 'PostController')->except(['create', 'edit']);
    Route::resource('skills', 'UserSkillController')->except(['show', 'edit', 'create']);
    Route::post('upload', 'UploadController@upload')->name('upload');

    Route::get('notifications', 'NotificationController@index')->name('index');
    Route::put('notifications', 'NotificationController@readAll')->name('read.all');
    Route::put('notifications/{notification}', 'NotificationController@read')->name('read');
    Route::delete('notifications', 'NotificationController@destroyAll')->name('destroy.all');
    Route::delete('notifications/{notification}', 'NotificationController@destroy')->name('destroy');
});

Route::group(['prefix' => 'auth', 'namespace' => 'Auth', 'as' => 'auth.'], function () {
    Route::post('login', 'LoginController@store')->name('login');
    Route::post('logout', 'LoginController@destroy')->name('logout');
    Route::post('register', 'RegisterController@store')->name('register');
    Route::post('verify', 'RegisterController@update')->name('verify');
    Route::post('forgot', 'ForgotController@store')->name('forgot');
    Route::post('reset', 'ResetController@store')->name('reset');
});