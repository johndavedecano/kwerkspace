<?php

namespace App\Http\Controllers\Common;

use App\Http\Resources\PostImageResource;
use App\Services\Post\PostImageCollection;
use App\Http\Controllers\Controller;

/**
 * Class PostImageController
 *
 * @package App\Http\Controllers\Common
 */
class PostImageController extends Controller
{
    /**
     * @var \App\Services\Post\PostImageCollection
     */
    public $collection;

    /**
     * PostImageController constructor.
     *
     * @param \App\Services\Post\PostImageCollection $collection
     */
    public function __construct(PostImageCollection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * @param $postId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index($postId)
    {
        $results = $this->collection->listByPost($postId);

        return PostImageResource::collection($results);
    }

    /**
     * @param $postId
     * @param $id
     * @return \App\Http\Resources\PostImageResource
     */
    public function show($postId, $id)
    {
        $results = $this->collection->findByPost($postId, $id);

        return new PostImageResource($results);
    }
}
