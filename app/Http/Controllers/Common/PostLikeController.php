<?php

namespace App\Http\Controllers\Common;

use App\Http\Resources\PostLikeResource;
use App\Services\Post\PostLikeCollection;
use App\Http\Controllers\Controller;

/**
 * Class PostLikeController
 *
 * @package App\Http\Controllers\Common
 */
class PostLikeController extends Controller
{
    /**
     * @var \App\Services\Post\PostLikeCollection
     */
    public $collection;

    /**
     * PostLikeController constructor.
     *
     * @param \App\Services\Post\PostLikeCollection $collection
     */
    public function __construct(PostLikeCollection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * @param $postId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index($postId)
    {
        $results = $this->collection->listByPost($postId);

        return PostLikeResource::collection($results);
    }

    /**
     * @param $postId
     * @param $id
     * @return \App\Http\Resources\PostLikeResource
     */
    public function show($postId, $id)
    {
        $results = $this->collection->findByPost($postId, $id);

        return new PostLikeResource($results);
    }
}
