<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\PostLikeResource;
use App\Services\Post\PostLikeCollection;
use App\Services\Post\PostLikeManager;
use App\Http\Controllers\Controller;

/**
 * Class PostLikeController
 *
 * @package App\Http\Controllers\Admin
 */
class PostLikeController extends Controller
{
    /**
     * @var \App\Services\Post\PostLikeManager
     */
    protected $manager;

    /**
     * @var \App\Services\Post\PostLikeCollection
     */
    protected $collection;

    /**
     * PostLikeController constructor.
     *
     * @param \App\Services\Post\PostLikeCollection $collection
     * @param \App\Services\Post\PostLikeManager $manager
     */
    public function __construct(PostLikeCollection $collection, PostLikeManager $manager)
    {
        $this->collection = $collection;

        $this->manager = $manager;
    }

    /**
     * @param $postId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index($postId)
    {
        $results = $this->collection->listByPost($postId);

        return PostLikeResource::collection($results);
    }

    /**
     * @param $postId
     * @param $likeId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($postId, $likeId)
    {
        $results = $this->manager->deleteByPost($postId, $likeId);

        return response()->json($results);
    }
}
