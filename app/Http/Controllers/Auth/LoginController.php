<?php

namespace App\Http\Controllers\Auth;

use App\Services\User\UserAuthManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class LoginController
 *
 * @package App\Http\Controllers\Auth
 */
class LoginController extends Controller
{
    /**
     * @var \App\Services\User\UserAuthManager
     */
    protected $manager;

    /**
     * LoginController constructor.
     *
     * @param \App\Services\User\UserAuthManager $manager
     */
    public function __construct(UserAuthManager $manager)
    {
        $this->manager = $manager;

        $this->middleware('auth:api')->only(['login']);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|exists:users,email',
            'password' => 'required',
        ]);

        $results = $this->manager->login($request->only(['email', 'password']));

        return response()->json($results);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy()
    {
        $results = $this->manager->logout();

        return response()->json($results);
    }
}
