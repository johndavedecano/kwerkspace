<?php

namespace App\Jobs;

use App\Entities\Review;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;

class CalculatePostRating implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var \App\Entities\Review
     */
    protected $review;

    /**
     * CalculatePostRating constructor.
     *
     * @param \App\Entities\Review $review
     */
    public function __construct(Review $review)
    {
        $this->review = $review;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $rating = DB::table('reviews')->where(['post_id' => $this->review->post_id])->average('rating');

        DB::table('posts')->where('id', $this->review->post_id)->update(['rating' => $rating]);
    }
}
