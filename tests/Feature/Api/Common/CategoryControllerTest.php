<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/9/2018
 * Time: 3:53 PM
 */

namespace Tests\Feature\Api\Common;

use App\Entities\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class CategoryControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testIndexSuccess()
    {
        $response = $this->get('api/common/categories');

        $response->assertOk();
    }

    public function testShowSuccess()
    {
        $category = factory(Category::class)->create();

        $response = $this->get('api/common/categories/'.$category->id);

        $response->assertOk();
    }
}