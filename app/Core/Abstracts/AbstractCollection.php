<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 3:59 PM
 */

namespace App\Core\Abstracts;

use App\Core\Constants;
use Illuminate\Database\Eloquent\Model;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class AbstractCollection
 *
 * @package App\Core\Abstracts
 */
abstract class AbstractCollection
{
    /**
     * @var \Spatie\QueryBuilder\QueryBuilder
     */
    protected $builder;

    /**
     * @var \App\Entities\Category
     */
    public $model;

    /**
     * @return \Spatie\QueryBuilder\QueryBuilder
     */
    public function getBuilder(): QueryBuilder
    {
        return $this->builder;
    }

    /**
     * @param \Spatie\QueryBuilder\QueryBuilder $builder
     */
    public function setBuilder(QueryBuilder $builder): void
    {
        $this->builder = $builder;
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     */
    public function setModel(Model $model): void
    {
        $this->model = $model;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->model->findOrFail($id);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findNotDeleted($id)
    {
        return $this->model->where('status', '<>', Constants::STATUS_DELETED)->where('id', $id)->firstOrFail();
    }

    /**
     * @param $slug
     * @return mixed
     */
    public function findBySlug($slug)
    {
        return $this->model->where('slug', $slug)->firstOrFail();
    }
}