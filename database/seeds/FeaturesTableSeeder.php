<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FeaturesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('features')->truncate();

        $features = [
            '24h Access',
            'Air Conditioning',
            'Cleaning Services',
            'Copier',
            'Event Space',
            'Free Coffee / Tea',
            'Fully Furnished',
            'High Speed Wi-fi',
            'Kitchen  Lounge / Chill-out Area',
            'Meeting Rooms',
            'Phone Booths',
            'Printer',
            'Reception Service',
            'Scanner',
            'Snacks',
            'Storage Options'
        ];

        $index = 1;

        foreach ($features as $feature) {
            \App\Entities\Feature::create([
                'id' => $index,
                'name' => $feature,
            ]);

            $index++;
        }
    }
}
