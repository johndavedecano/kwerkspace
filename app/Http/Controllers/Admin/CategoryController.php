<?php

namespace App\Http\Controllers\Admin;

use App\Core\Constants;
use App\Http\Resources\CategoryResource;
use App\Services\Category\CategoryCollection;
use App\Services\Category\CategoryManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class CategoryController
 *
 * @package App\Http\Controllers\Admin
 */
class CategoryController extends Controller
{
    /**
     * @var \App\Services\Category\CategoryManager
     */
    public $manager;

    /**
     * @var \App\Services\Category\CategoryCollection
     */
    public $collection;

    /**
     * CategoryController constructor.
     *
     * @param \App\Services\Category\CategoryCollection $collection
     * @param \App\Services\Category\CategoryManager $manager
     */
    public function __construct(CategoryCollection $collection, CategoryManager $manager)
    {
        $this->collection = $collection;

        $this->manager = $manager;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $collection = $this->collection->listFromAdmin();

        return CategoryResource::collection($collection);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\CategoryResource
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $results = $this->manager->create($request->get('name'));

        return new CategoryResource($results);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\CategoryResource
     */
    public function show($id)
    {
        $results = $this->collection->find($id);

        return new CategoryResource($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \App\Http\Resources\CategoryResource
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['name' => 'required', 'status' => 'in:active,inactive,deleted']);

        $this->manager->update($id, $request->all());

        return $this->show($id);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\CategoryResource
     */
    public function destroy($id)
    {
        $this->manager->delete($id);

        return $this->show($id);
    }
}
