<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\PostResource;
use App\Services\Post\PostCollection;
use App\Services\Post\PostManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class PostController
 *
 * @package App\Http\Controllers\Admin
 */
class PostController extends Controller
{
    /**
     * @var \App\Services\Post\PostManager
     */
    protected $manager;

    /**
     * @var \App\Services\Post\PostCollection
     */
    protected $collection;

    /**
     * PostController constructor.
     *
     * @param \App\Services\Post\PostCollection $collection
     * @param \App\Services\Post\PostManager $manager
     */
    public function __construct(PostCollection $collection, PostManager $manager)
    {
        $this->collection = $collection;

        $this->manager = $manager;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $results = $this->collection->listFromAdmin();

        return PostResource::collection($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\PostResource
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
            'category_id' => 'required|exists:categories,id',
            'country_id' => 'required|exists:countries,id',
            'title' => 'required',
            'description' => 'required',
            'status' => 'required|in:active,inactive',
            'thumbnail' => 'required',
            'images' => 'required|array',
            'features' => 'required|array',
            'cover' => 'required',
            'area' => 'numeric',
            'max_occupants' => 'numeric|min:1',
            'payment_type' => 'required|in:monthly,daily,hourly',
            'payment_amount' => 'required|numeric|min:1',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
            'zipcode' => 'required',
            'lon' => 'required',
            'lat' => 'required',
        ]);

        $results = $this->manager->create($request->all());

        return new PostResource($results);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\PostResource
     */
    public function show($id)
    {
        $results = $this->collection->find($id);

        return new PostResource($results);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'user_id' => 'sometimes|required|exists:users,id',
            'category_id' => 'sometimes|required|exists:categories,id',
            'country_id' => 'sometimes|required|exists:countries,id',
            'title' => 'sometimes|required',
            'description' => 'sometimes|required',
            'status' => 'sometimes|required|in:active,inactive',
            'thumbnail' => 'sometimes|required',
            'cover' => 'sometimes|required',
            'area' => 'sometimes|required|numeric',
            'features' => 'sometimes|required|array',
            'max_occupants' => 'sometimes|required|numeric|min:1',
            'payment_type' => 'sometimes|required|in:monthly,daily,hourly',
            'payment_amount' => 'sometimes|required|numeric|min:1',
            'address' => 'sometimes|required',
            'city' => 'sometimes|required',
            'state' => 'sometimes|required',
            'zipcode' => 'sometimes|required',
            'lon' => 'sometimes|required',
            'lat' => 'sometimes|required',
        ]);

        $this->manager->update($id, $request->all());

        return $this->show($id);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\PostResource
     */
    public function destroy($id)
    {
        $this->manager->delete($id);

        return $this->show($id);
    }
}
