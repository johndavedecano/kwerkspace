<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:22 PM
 */

namespace App\Services\User;

use App\Core\Abstracts\AbstractManager;
use App\Core\Constants;
use App\Entities\User;
use Carbon\Carbon;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Password;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;

/**
 * Class UserAuthManager
 *
 * @package App\Services\User
 */
class UserAuthManager extends AbstractManager
{
    /**
     * @var \App\Entities\User
     */
    public $model;

    /**
     * @var \Tymon\JWTAuth\JWTAuth
     */
    protected $jwt;

    /**
     * UserAuthManager constructor.
     *
     * @param \App\Entities\User $model
     * @param \Tymon\JWTAuth\JWTAuth $jwt
     */
    public function __construct(User $model, JWTAuth $jwt)
    {
        $this->model = $model;

        $this->jwt = $jwt;
    }

    /**
     * @param array $data
     * @return \App\Entities\User
     */
    public function register($data = [])
    {
        $data['country_id'] = Constants::DEFAULT_COUNTRY; // Philippines

        $data['role'] = User::ROLE_USER;

        $user = $this->model->create($data);

        $user->sendEmailVerificationNotification();

        return $user;
    }

    /**
     * @param $token
     * @return \App\Entities\User
     */
    public function verify($token)
    {
        try {
            $user = User::verifyToken($token);

            if ($user->status === Constants::STATUS_ACTIVE && $user->hasVerifiedEmail()) {
                throw new \Exception('User is already verified');
            }

            $user->markEmailAsVerified();

            return $user;
        } catch(\Exception $e) {
            throw new HttpException(400, 'Unable to verify token');
        }
    }

    /**
     * @param \App\Entities\User $user
     * @return string
     */
    public function token(User $user)
    {
        return $this->jwt->fromUser($user);
    }

    /**
     * @param array $data
     * @return array
     * @throws \Illuminate\Auth\AuthenticationException
     */
    public function login($data = [])
    {
        $token = auth()->guard()->attempt($data);

        if (!$token) {
            throw new AuthenticationException('Invalid email or password');
        }

        $user = $this->model->where('email', $data['email'])->firstOrFail();

        if ($user->status !== Constants::STATUS_ACTIVE) {
            throw new AuthenticationException('Account is not active');
        }

        // TODO: Fire a login event for activity logs

        $user->update(['last_login_at' => Carbon::now()]);

        return [
            'token' => $token,
            'expires_in' => auth()->guard()->factory()->getTTL() * 60,
            'user' => $user,
        ];
    }

    /**
     * @param \App\Entities\User $user
     * @return array
     */
    public function loginUser(User $user)
    {
        return [
            'token' => $this->token($user),
            'expires_in' => auth()->guard()->factory()->getTTL() * 60,
            'user' => $user,
        ];
    }

    /**
     * @return mixed
     */
    public function logout()
    {
        return auth()->guard()->logout();
    }

    /**
     * @return mixed
     */
    private function getPasswordBroker()
    {
        return Password::broker();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function forgot($data = [])
    {
        $user = $this->model->where('email', '=', $data['email'])->first();

        if(!$user) {
            throw new ModelNotFoundException('User does not exists');
        }

        $broker = $this->getPasswordBroker();

        $sendingResponse = $broker->sendResetLink(['email' => $data['email']]);

        if($sendingResponse !== Password::RESET_LINK_SENT) {
            throw new HttpException(400, 'Unable to send reset email');
        }

        return $user;
    }

    /**
     * @param array $request
     * @return mixed
     */
    public function reset($request = [])
    {
        $user = null;

        $response = $this->getPasswordBroker()->reset(
            $request, function ($u, $password) use(&$user) {
                $user = $u;
                $user->password = $password;
                $user->save();
            }
        );

        if($response !== Password::PASSWORD_RESET) {
            throw new HttpException(400, 'Unable to reset password');
        }

        return $this->loginUser($user);
    }
}