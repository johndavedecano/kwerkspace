import Noty from 'noty'

export function notify(options) {
  return new Noty({ timeout: 3000, theme: 'bootstrap-v4', ...options }).show()
}
