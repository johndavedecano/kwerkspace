import axios from 'axios'
import { removeToken } from './auth'

export default function() {
  const token = localStorage.getItem('token')
  axios.defaults.baseURL = `${process.env.VUE_APP_API}/api`
  axios.defaults.headers.common['Accept'] = 'application/json'
  axios.defaults.headers.common['Content-Type'] = 'application/json'
  axios.defaults.headers.common['Authorization'] = `Bearer ${token}`
  axios.interceptors.response.use(undefined, function(err) {
    return new Promise((resolve, reject) => {
      if (err && err.response && err.response.status === 401) {
        removeToken()
        window.location.replace('/auth/login')
        resolve({})
        return
      }

      if (
        err &&
        err.response &&
        err.response.status === 422 &&
        err.response.data.error
      ) {
        reject(err) // todo
        return
      }

      reject(err)
    })
  })
}
