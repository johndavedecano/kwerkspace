## Laravel Co-Working Space Listing

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- Connects tenants with co working spaces
- Connects tenants with co workers
- Inquiries
- Reviews System
- and many more

Laravel Cowork is accessible, yet powerful, providing tools needed for large, robust co-working applications.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
