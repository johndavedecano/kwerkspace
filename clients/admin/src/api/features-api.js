// features-api.js
import axios from 'axios'

export function load(params = {}) {
  return axios
    .get('/admin/features', { params })
    .then(repsonse => repsonse.data)
}

export function show(id) {
  return axios.get(`/admin/features/${id}`).then(repsonse => repsonse.data)
}

export function destroy(id) {
  return axios.delete(`/admin/features/${id}`).then(repsonse => repsonse.data)
}

export function update(id, params = {}) {
  return axios
    .put(`/admin/features/${id}`, params)
    .then(repsonse => repsonse.data)
}

export function create(params = {}) {
  return axios.post('/admin/features', params).then(repsonse => repsonse.data)
}
