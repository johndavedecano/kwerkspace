<?php

namespace App\Http\Controllers\User;

use App\Http\Resources\PostImageResource;
use App\Services\Post\PostCollection;
use App\Services\Post\PostImageCollection;
use App\Services\Post\PostImageManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class PostImageController
 *
 * @package App\Http\Controllers\User
 */
class PostImageController extends Controller
{
    /**
     * @var \App\Services\Post\PostImageManager
     */
    protected $manager;

    /**
     * @var \App\Services\Post\PostImageCollection
     */
    protected $collection;

    /**
     * PostImageController constructor.
     *
     * @param \App\Services\Post\PostImageCollection $collection
     * @param \App\Services\Post\PostImageManager $manager
     */
    public function __construct(PostImageCollection $collection, PostImageManager $manager)
    {
        $this->manager = $manager;

        $this->collection = $collection;
    }

    /**
     * @param $postId
     * @return \App\Http\Resources\PostImageResource
     */
    public function index($postId)
    {
        $results = $this->collection->listByUserAndPost(auth()->id(), $postId);

        return new PostImageResource($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $postId
     * @return \App\Http\Resources\PostImageResource
     */
    public function store(Request $request, $postId)
    {
        $this->validate($request, [
            'files' => 'required',
        ]);

        $this->manager->createByUser(auth()->id(), $postId, $request->get('files'));

        return $this->index($postId);
    }

    /**
     * @param $postId
     * @param $id
     * @return \App\Http\Resources\PostImageResource
     */
    public function show($postId, $id)
    {
        $results = $this->collection->findByPost($postId, $id);

        return new PostImageResource($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $postId
     * @return \App\Http\Resources\PostImageResource
     */
    public function update(Request $request, $postId)
    {
        $this->validate($request, ['files' => 'required']);

        $this->manager->updateByUser(auth()->id(), $postId, $request->get('files'));

        return $this->index($postId);
    }

    /**
     * @param $postId
     * @param $id
     * @return \App\Http\Resources\PostImageResource
     */
    public function destroy($postId, $id)
    {
        $results = $this->manager->deleteByUser(auth()->id(), $postId, $id);

        return response()->json($results);
    }
}
