<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $post_id post id
 * @property text $files files
 * @property Post $post belongsTo
 */
class PostsImage extends Model
{
    /**
     * Database table name
     */
    protected $table = 'posts_images';

    /**
     * Protected columns from mass assignment
     */
    protected $guarded = ['id'];

    /**
     * Mass assignable columns
     */
    protected $fillable = ['files', 'post_id', 'files'];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @param $value
     * @return array|mixed
     */
    public function getFilesAttribute($value)
    {
        try {
            return (array) json_decode($value);
        } catch (\Exception $e) {
            return [];
        }
    }
}