<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/8/2018
 * Time: 7:34 PM
 */

namespace Tests\Feature\Api\Auth;

use App\Core\Constants;
use App\Entities\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class RegisterControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testStoreFailsValidation()
    {
        $response = $this->post('api/auth/register');

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStoreFailsEmailExists()
    {
        $user = factory(User::class)->create();

        $data = [
            'email' => $user->email,
            'name' => 'kulafu',
            'password' => 'password',
            'password_confirmation' => 'password'
        ];

        $response = $this->post('api/auth/register', $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStoreSuccess()
    {
        $data = [
            'email' => str_slug(str_random()).'@gmail.com',
            'name' => 'kulafu',
            'password' => 'password',
            'password_confirmation' => 'password'
        ];

        $response = $this->post('api/auth/register', $data);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testUpdateFailsValidation()
    {
        $response = $this->post('api/auth/verify');

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdateFailsAlreadyVerified()
    {
        $user = factory(User::class)->create();

        $user->markEmailAsVerified();

        $token = encrypt($user->email);

        $response = $this->post('api/auth/verify', ['token' => $token]);

        $response->assertStatus(Response::HTTP_BAD_REQUEST);
    }

    public function testUpdateSuccess()
    {
        $user = factory(User::class)->create([
            'status' => Constants::STATUS_INACTIVE,
            'email_verified_at' => null
        ]);

        $token = encrypt($user->email);

        $response = $this->post('api/auth/verify', ['token' => $token]);

        $response->assertStatus(Response::HTTP_OK);
    }
}
