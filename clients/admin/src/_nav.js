export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      icon: 'cui-dashboard',
    },
    {
      name: 'Posts',
      url: '/posts',
      icon: 'icon-note',
      children: [
        {
          name: 'New Post',
          url: '/posts/create',
          icon: 'icon-arrow-right',
        },
      ],
    },
    {
      name: 'Users',
      url: '/users',
      icon: 'icon-people',
      children: [
        {
          name: 'Listing',
          url: '/users/index',
          icon: 'icon-arrow-right',
        },
        {
          name: 'New Feature',
          url: '/users/create',
          icon: 'icon-arrow-right',
        },
      ],
    },
    {
      name: 'Inquiries',
      url: '/inquiries',
      icon: 'cui-envelope-closed',
      children: [
        {
          name: 'New Inquiry',
          url: '/inquiries/create',
          icon: 'icon-arrow-right',
        },
      ],
    },
    {
      name: 'Reviews',
      url: '/reviews',
      icon: 'icon-star',
      children: [
        {
          name: 'New Review',
          url: '/reviews/create',
          icon: 'icon-arrow-right',
        },
      ],
    },
    {
      name: 'Counries',
      url: '/countries',
      icon: 'icon-list',
      children: [
        {
          name: 'Listing',
          url: '/countries/index',
          icon: 'icon-arrow-right',
        },
        {
          name: 'New Country',
          url: '/countries/create',
          icon: 'icon-arrow-right',
        },
      ],
    },
    {
      name: 'Features',
      url: '/features',
      icon: 'icon-list',
      children: [
        {
          name: 'Listing',
          url: '/features/index',
          icon: 'icon-arrow-right',
        },
        {
          name: 'New Feature',
          url: '/features/create',
          icon: 'icon-arrow-right',
        },
      ],
    },
    {
      name: 'Skills',
      url: '/skills',
      icon: 'icon-list',
      children: [
        {
          name: 'Listing',
          url: '/skills/index',
          icon: 'icon-arrow-right',
        },
        {
          name: 'New Skill',
          url: '/skills/create',
          icon: 'icon-arrow-right',
        },
      ],
    },
    {
      name: 'Categories',
      url: '/categories',
      icon: 'icon-list',
      children: [
        {
          name: 'Listing',
          url: '/categories/index',
          icon: 'icon-arrow-right',
        },
        {
          name: 'New Category',
          url: '/categories/create',
          icon: 'icon-arrow-right',
        },
      ],
    },
  ],
}
