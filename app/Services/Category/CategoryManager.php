<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:20 PM
 */

namespace App\Services\Category;

use App\Core\Abstracts\AbstractManager;
use App\Core\Constants;
use App\Entities\Category;

class CategoryManager extends AbstractManager
{
    /**
     * @var \App\Entities\Category
     */
    public $model;

    /**
     * CategoryManager constructor.
     *
     * @param \App\Entities\Category $model
     */
    public function __construct(Category $model)
    {
        $this->model = $model;
    }

    /**
     * @param $name
     * @return mixed
     */
    public function create($name)
    {
        return $this->model->create(['name' => $name]);
    }

    /**
     * @param $id
     * @param array $request
     * @return mixed
     */
    public function update($id, $request = [])
    {
        $data = array_only($request, $this->model->getFillable());

        return $this->model->findOrFail($id)->update($data);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        return $this->model->findOrFail($id)->update(['status' => Constants::STATUS_DELETED]);
    }
}