<?php

namespace App\Entities;

use App\Entities\Relations\WithFeature;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $feature_id feature id
 * @property Feature $feature belongsTo
 */
class PostsFeature extends Model
{
    use WithFeature;

    /**
     * Database table name
     */
    protected $table = 'posts_features';

    /**
     * Protected columns from mass assignment
     */
    protected $guarded = ['id'];

    /**
     * Mass assignable columns
     */
    protected $fillable = ['post_id', 'feature_id'];

    /**
     * @var bool
     */
    public $timestamps = false;
}