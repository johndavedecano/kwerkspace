<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->truncate();

        $categories = [
            'Private Offices',
            'Co-working Spaces',
            'Meeting and Conference Rooms',
            'Training and Workshops Spaces',
            'Virtual Offices',
            'Retail Spaces',
            'Event Spaces',
            'Corporate Dining'
        ];

        $index = 1;

        foreach ($categories as $category) {
            \App\Entities\Category::create([
                'id' => $index,
                'name' => $category,
            ]);

            $index++;
        }
    }
}
