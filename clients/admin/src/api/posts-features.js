// posts-features-api.js
import axios from 'axios'

export function load(postId, params = {}) {
  return axios
    .get(`/admin/posts/${postId}/features`, { params })
    .then(repsonse => repsonse.data)
}

export function show(postId, featureId) {
  return axios
    .get(`/admin/posts/${postId}/features/${featureId}`)
    .then(repsonse => repsonse.data)
}

export function destroy(postId, featureId) {
  return axios
    .delete(`/admin/posts/${postId}/features/${featureId}`)
    .then(repsonse => repsonse.data)
}

export function update(postId, featureId, params = {}) {
  return axios
    .put(`/admin/posts/${postId}/features/${featureId}`, { params })
    .then(repsonse => repsonse.data)
}

export function create(postId, params = {}) {
  return axios
    .post(`/admin/posts/${postId}/features`, params)
    .then(repsonse => repsonse.data)
}
