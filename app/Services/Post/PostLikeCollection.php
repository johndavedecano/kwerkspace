<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/30/2018
 * Time: 7:50 AM
 */

namespace App\Services\Post;

use App\Core\Abstracts\AbstractCollection;
use App\Core\Interfaces\CollectionInterface;
use App\Entities\Post;
use App\Entities\PostsLike;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class PostLikeCollection
 *
 * @package App\Services\Post
 */
class PostLikeCollection extends AbstractCollection implements CollectionInterface
{
    /**
     * @var \App\Entities\Post
     */
    protected $post;

    const USER_FIELDS = ['name', 'slug', 'id', 'avatar'];

    /**
     * PostLikeCollection constructor.
     *
     * @param \App\Entities\PostsLike $model
     * @param \App\Entities\Post $post
     */
    public function __construct(PostsLike $model, Post $post)
    {
        $this->model = $model;

        $this->post = $post;

        $this->builder = QueryBuilder::for(PostsLike::class);
    }

    /**
     * @param $postId
     * @return mixed
     */
    public function getLikesCount($postId)
    {
        return $this->builder->where('post_id', $postId)->count();
    }

    /**
     * @param $postId
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function listByPost($postId)
    {
        return $this->builder->with([
            'user' => function($query) {
                return $query->select(static::USER_FIELDS);
            }
        ])->where('post_id', $postId)->paginate();
    }

    /**
     * @param $postId
     * @param $userId
     * @return mixed
     */
    public function findByPost($postId, $userId)
    {
        return $this->post
            ->findOrFail($postId)
            ->likes()
            ->where('user_id', $userId)
            ->firstOrFail();
    }
}