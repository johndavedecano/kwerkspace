// users-skills-api.js
import axios from 'axios'

export function load(userId, params = {}) {
  return axios
    .get(`/admin/users/${userId}/skills`, { params })
    .then(repsonse => repsonse.data)
}

export function show(userId, likeId) {
  return axios
    .get(`/admin/users/${userId}/skills/${likeId}`)
    .then(repsonse => repsonse.data)
}

export function destroy(userId, likeId) {
  return axios
    .delete(`/admin/users/${userId}/skills/${likeId}`)
    .then(repsonse => repsonse.data)
}

export function update(userId, likeId, params = {}) {
  return axios
    .put(`/admin/users/${userId}/skills/${likeId}`, params)
    .then(repsonse => repsonse.data)
}

export function create(userId, params = {}) {
  return axios
    .post(`/admin/users/${userId}/skills`, params)
    .then(repsonse => repsonse.data)
}
