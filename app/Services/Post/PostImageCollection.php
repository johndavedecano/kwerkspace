<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/30/2018
 * Time: 7:50 AM
 */

namespace App\Services\Post;

use App\Core\Abstracts\AbstractCollection;
use App\Core\Interfaces\CollectionInterface;
use App\Entities\Post;
use App\Entities\PostsImage;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class PostImageCollection
 *
 * @package App\Services\Post
 */
class PostImageCollection extends AbstractCollection implements CollectionInterface
{
    /**
     * @var \App\Entities\Post
     */
    protected $post;

    /**
     * PostImageCollection constructor.
     *
     * @param \App\Entities\Post $post
     * @param \App\Entities\PostsImage $model
     */
    public function __construct(Post $post, PostsImage $model)
    {
        $this->model = $model;

        $this->post = $post;

        $this->builder = QueryBuilder::for(PostsImage::class);
    }

    /**
     * @param $userId
     * @param $postId
     * @return mixed
     */
    public function listByUserAndPost($userId, $postId)
    {
        return $this->post->where(['user_id' => $userId, 'id' => $postId])->firstOrFail()->images()->get();
    }

    /**
     * @param $postId
     * @return \Illuminate\Database\Eloquent\Collection|\Spatie\QueryBuilder\QueryBuilder[]
     */
    public function listByPost($postId)
    {
        return $this->builder->where('post_id', $postId)->get();
    }

    /**
     * @param $postId
     * @param $imageId
     * @return mixed
     */
    public function findByPost($postId, $imageId)
    {
        return $this->model->where(['post_id' => $postId, 'id' => $imageId])->firstOrFail();
    }

}