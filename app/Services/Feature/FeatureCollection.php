<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:20 PM
 */

namespace App\Services\Feature;

use App\Core\Abstracts\AbstractCollection;
use App\Core\Interfaces\CollectionInterface;
use App\Entities\Feature;
use App\Services\Traits\WithFindSlug;
use App\Services\Traits\WithNameSearch;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class FeatureCollection
 *
 * @package App\Services\Feature
 */
class FeatureCollection extends AbstractCollection implements CollectionInterface
{
    use WithFindSlug, WithNameSearch;

    /**
     * FeatureCollection constructor.
     *
     * @param \App\Entities\Feature $model
     */
    public function __construct(Feature $model)
    {
        $this->builder = QueryBuilder::for(Feature::class);

        $this->model = $model;
    }
}