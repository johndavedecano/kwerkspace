<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/6/2018
 * Time: 7:46 PM
 */

namespace Tests\Feature\Api\Admin;

use App\Entities\Skill;
use App\Entities\User;
use App\Entities\UserSkill;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\Feature\Api\Authentication;
use Tests\TestCase;

/**
 * Class UserSkillControllerTest
 *
 * @package Tests\Feature\Api\Admin
 */
class UserSkillControllerTest extends TestCase
{
    use RefreshDatabase, Authentication;

    public function setUp()
    {
        parent::setUp();

        $this->admin = $this->getAuthHeaders(User::ROLE_ADMIN);

        $this->user = $this->getAuthHeaders(User::ROLE_USER);
    }

    public function testIndexFails()
    {
        $response = $this->withHeaders(['Accept' => 'application/json'])->get('/api/admin/users/1/skills');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testIndexFailsNotAllowed()
    {
        $response = $this->withHeaders($this->user)->get('/api/admin/users/1/skills');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testIndexSuccess()
    {
        $user = factory(User::class)->create();

        $response = $this->withHeaders($this->admin)->get('/api/admin/users/'.$user->id.'/skills');

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testStoreFails()
    {
        $response = $this->withHeaders([])->post('/api/admin/users/346363/skills');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testStoreFailsNotFound()
    {
        $data = ['skills' => [
            [
                'skill_id' => factory(Skill::class)->create()->id,
                'experience' => 1,
            ]
        ]];

        $response = $this->withHeaders($this->admin)->post('/api/admin/users/112323522/skills', $data);

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testStoreValidationFailsNotFound()
    {
        $user = factory(User::class)->create();

        $data = ['skills' => [
            [
                'skill_id' => 235626526,
                'experience' => 1,
            ]
        ]];

        $response = $this->withHeaders($this->admin)->post('/api/admin/users/'.$user->id.'/skills', $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStoreValidationFails()
    {
        $user = factory(User::class)->create();

        $response = $this->withHeaders($this->admin)->post('/api/admin/users/'.$user->id.'/skills');

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStoreSuccess()
    {
        $user = factory(User::class)->create();

        $data = ['skills' => [
            [
                'skill_id' => factory(Skill::class)->create()->id,
                'experience' => 1,
            ]
        ]];

        $response = $this->withHeaders($this->admin)->post('/api/admin/users/'.$user->id.'/skills', $data);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testUpdateFails()
    {
        $user = factory(User::class)->create();

        $skill = factory(Skill::class)->create();

        $response = $this->withHeaders([])->put('/api/admin/users/'.$user->id.'/skills/'.$skill->id, []);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }


    public function testUpdateFailsNotAdmin()
    {
        $user = factory(User::class)->create();

        $skill = factory(Skill::class)->create();

        $response = $this->withHeaders($this->user)->put('/api/admin/users/'.$user->id.'/skills/'.$skill->id, []);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testUpdateFailsNotFound()
    {
        $user = factory(User::class)->create();

        $data = ['experience' => 1];

        $response = $this->withHeaders($this->admin)->put('/api/admin/users/'.$user->id.'/skills/2353252', $data);

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testUpdateSuccess()
    {
        $user = factory(User::class)->create();

        $skill = factory(Skill::class)->create();

        $data = ['experience' => 1];

        $response = $this->withHeaders($this->admin)->put('/api/admin/users/'.$user->id.'/skills/'.$skill->id, $data);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testDestroyFails()
    {
        $user = factory(User::class)->create();

        $skill = factory(Skill::class)->create();

        $response = $this->withHeaders([])->delete('/api/admin/users/'.$user->id.'/skills/'.$skill->id);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testDestroyFailsNotFound()
    {
        $user = factory(User::class)->create();

        $response = $this->withHeaders($this->admin)->delete('/api/admin/users/'.$user->id.'/skills/23623623623632');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testDestroySuccess()
    {
        $user = factory(User::class)->create();

        $skill = factory(UserSkill::class)->create(['user_id' => $user->id]);

        $response = $this->withHeaders($this->admin)->delete('/api/admin/users/'.$user->id.'/skills/'.$skill->id);

        $response->assertStatus(Response::HTTP_OK);
    }
}