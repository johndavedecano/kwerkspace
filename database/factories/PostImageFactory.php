<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Entities\PostsImage::class, function (Faker $faker) {
    $image = new \App\Core\Types\FakeImage();

    return [
        'post_id' => function () {
            return factory(\App\Entities\Post::class)->create()->id;
        },
        'files' => json_encode($image()),
    ];
});
