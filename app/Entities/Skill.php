<?php

namespace App\Entities;

use App\Entities\Others\WithSlug;
use App\Entities\Scopes\ScopeActive;
use App\Entities\Scopes\ScopeDeleted;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Skill
 *
 * @package App\Entities
 */
class Skill extends Model
{
    use ScopeActive, ScopeDeleted;

    use WithSlug;

    /**
     * @var string
     */
    public $slugFrom = 'name';

    /**
     * @var string
     */
    public $slugTo = 'slug';

    /**
     * Database table name
     */
    protected $table = 'skills';

    /**
     * Protected columns from mass assignment
     */
    protected $guarded = ['id'];

    /**
     * Mass assignable columns
     */
    protected $fillable = ['status', 'name', 'slug', 'status'];

    /**
     * @var bool
     */
    public $timestamps = false;
}