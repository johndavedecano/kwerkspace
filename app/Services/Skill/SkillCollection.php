<?php
/**
 * Created by PhpStorm.
 * \App\Entities\Skill: Dave
 * Date: 9/29/2018
 * Time: 2:20 PM
 */

namespace App\Services\Skill;

use App\Core\Abstracts\AbstractCollection;
use App\Core\Interfaces\CollectionInterface;
use App\Entities\Skill;
use App\Services\Traits\WithFindSlug;
use App\Services\Traits\WithNameSearch;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class SkillCollection
 * @package App\Services\\App\Entities\Skill
 */
class SkillCollection extends AbstractCollection implements CollectionInterface
{
    use WithFindSlug, WithNameSearch;

    /**
     * \App\Entities\SkillCollection constructor.
     *
     * @param \App\Entities\Skill $model
     */
    public function __construct(Skill $model)
    {
        $this->builder = QueryBuilder::for(Skill::class);

        $this->model = $model;
    }
}