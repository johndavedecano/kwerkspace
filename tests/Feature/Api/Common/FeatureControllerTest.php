<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/9/2018
 * Time: 3:53 PM
 */

namespace Tests\Feature\Api\Common;

use App\Entities\Feature;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FeatureControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testIndexSuccess()
    {
        $response = $this->get('api/common/features');

        $response->assertOk();
    }

    public function testShowSuccess()
    {
        $result = factory(Feature::class)->create();

        $response = $this->get('api/common/features/'.$result->id);

        $response->assertOk();
    }
}