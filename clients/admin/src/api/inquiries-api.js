// inquiries-api.js
import axios from 'axios'

export function load(params = {}) {
  return axios
    .get('/admin/inquiries', { params })
    .then(repsonse => repsonse.data)
}

export function show(id) {
  return axios.get(`/admin/inquiries/${id}`).then(repsonse => repsonse.data)
}

export function destroy(id) {
  return axios.delete(`/admin/inquiries/${id}`).then(repsonse => repsonse.data)
}

export function update(id, params = {}) {
  return axios
    .put(`/admin/inquiries/${id}`, { params })
    .then(repsonse => repsonse.data)
}

export function create(params = {}) {
  return axios.post('/admin/inquiries', params).then(repsonse => repsonse.data)
}
