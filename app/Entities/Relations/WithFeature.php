<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 12:30 PM
 */

namespace App\Entities\Relations;

use App\Entities\Feature;

/**
 * Trait WithFeature
 *
 * @package App\Entities\Relations
 */
trait WithFeature
{
    /**
     * @return mixed
     */
    public function feature()
    {
        return $this->belongsTo(Feature::class, 'feature_id');
    }
}