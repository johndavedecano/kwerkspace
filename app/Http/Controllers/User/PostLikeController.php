<?php

namespace App\Http\Controllers\User;

use App\Services\Post\PostLikeCollection;
use App\Services\Post\PostLikeManager;
use App\Http\Controllers\Controller;

/**
 * Class PostLikeController
 *
 * @package App\Http\Controllers\User
 */
class PostLikeController extends Controller
{
    /**
     * @var \App\Services\Post\PostLikeManager
     */
    public $manager;

    /**
     * @var \App\Services\Post\PostLikeCollection
     */
    public $collection;

    /**
     * PostLikeController constructor.
     *
     * @param \App\Services\Post\PostLikeCollection $collection
     * @param \App\Services\Post\PostLikeManager $manager
     */
    public function __construct(PostLikeCollection $collection, PostLikeManager $manager)
    {
        $this->manager = $manager;

        $this->collection = $collection;
    }

    /**
     * @param $postId
     * @return \Illuminate\Http\JsonResponse
     */
    public function store($postId)
    {
        $this->manager->like(auth()->id(), $postId);

        return response()->json(['count' => $this->collection->getLikesCount($postId)]);
    }

    /**
     * @param $postId
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($postId)
    {
        $this->manager->unlike(auth()->id(), $postId);

        return response()->json(['count' => $this->collection->getLikesCount($postId)]);
    }
}
