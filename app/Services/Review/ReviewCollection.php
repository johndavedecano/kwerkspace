<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/30/2018
 * Time: 7:50 AM
 */

namespace App\Services\Review;

use App\Core\Abstracts\AbstractCollection;
use App\Core\Constants;
use App\Core\Interfaces\CollectionInterface;
use App\Entities\Review;
use Spatie\QueryBuilder\QueryBuilder;

/**
 * Class ReviewCollection
 *
 * @package App\Services\Review
 */
class ReviewCollection extends AbstractCollection implements CollectionInterface
{
    /**
     * ReviewCollection constructor.
     *
     * @param \App\Entities\Review $model
     */
    public function __construct(Review $model)
    {
        $this->model = $model;

        $this->builder = QueryBuilder::for(Review::class);
    }

    /**
     * @param $userId
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function listFromUser($userId)
    {
        return $this->builder->where('user_id', $userId)->with([
            'user' => function($query) {
                return $query->select(['name', 'slug', 'id', 'avatar']);
            }
        ])->paginate(Constants::PAGINATION_LIMIT);
    }

    /**
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function listFromAdmin()
    {
        return $this->builder->with([
            'user' => function($query) {
                return $query->select(['name', 'slug', 'id', 'avatar']);
            }
        ])->paginate(Constants::PAGINATION_LIMIT);
    }

    /**
     * @param $postId
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function listByPost($postId)
    {
        return $this->builder->with([
            'user' => function($query) {
                return $query->select(['name', 'slug', 'id', 'avatar']);
            }
        ])->where('post_id', $postId)->paginate(Constants::PAGINATION_LIMIT);
    }

    /**
     * @param $postId
     * @param $reviewId
     * @return mixed
     */
    public function findByPost($postId, $reviewId)
    {
        return $this->model->where('post_id', $postId)->where('id', $reviewId)->firstOrFail();
    }

    /**
     * @param $userId
     * @param $reviewId
     * @return mixed
     */
    public function findByUser($userId, $reviewId)
    {
        return $this->model->where('user_id', $userId)->where('id', $reviewId)->firstOrFail();
    }
}