<?php

namespace App\Http\Controllers\Common;

use App\Http\Resources\PostResource;
use App\Services\Post\PostCollection;
use App\Http\Controllers\Controller;

/**
 * Class UserPostController
 *
 * @package App\Http\Controllers\Common
 */
class UserPostController extends Controller
{
    /**
     * @var \App\Services\Post\PostCollection
     */
    public $collection;

    /**
     * UserPostController constructor.
     *
     * @param \App\Services\Post\PostCollection $collection
     */
    public function __construct(PostCollection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * @param $userId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index($userId)
    {
        $collection = $this->collection->listByUser($userId);

        return PostResource::collection($collection);
    }

    /**
     * @param $userId
     * @param $id
     * @return \App\Http\Resources\PostResource
     */
    public function show($userId, $id)
    {
        $results = $this->collection->findByUserFromCommon($userId, $id);

        return new PostResource($results);
    }
}
