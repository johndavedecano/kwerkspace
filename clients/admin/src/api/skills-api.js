// skills-api.js
import axios from 'axios'

export function load(params = {}) {
  return axios.get('/admin/skills', { params }).then(repsonse => repsonse.data)
}

export function show(id) {
  return axios.get(`/admin/skills/${id}`).then(repsonse => repsonse.data)
}

export function destroy(id) {
  return axios.delete(`/admin/skills/${id}`).then(repsonse => repsonse.data)
}

export function update(id, params = {}) {
  return axios
    .put(`/admin/skills/${id}`, params)
    .then(repsonse => repsonse.data)
}

export function create(params = {}) {
  return axios.post('/admin/skills', params).then(repsonse => repsonse.data)
}
