<?php

namespace App\Http\Controllers\Admin;

use App\Http\Resources\InquiryResource;
use App\Services\Inquiry\InquiryCollection;
use App\Services\Inquiry\InquiryManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class InquiryController
 *
 * @package App\Http\Controllers\Admin
 */
class InquiryController extends Controller
{
    /**
     * @var \App\Services\Inquiry\InquiryCollection
     */
    protected $collection;

    /**
     * @var \App\Services\Inquiry\InquiryManager
     */
    protected $manager;

    /**
     * InquiryController constructor.
     *
     * @param \App\Services\Inquiry\InquiryCollection $collection
     * @param \App\Services\Inquiry\InquiryManager $manager
     */
    public function __construct(InquiryCollection $collection, InquiryManager $manager)
    {
        $this->manager = $manager;

        $this->collection = $collection;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $results = $this->collection->listFromAdmin();

        return InquiryResource::collection($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \App\Http\Resources\InquiryResource
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required|exists:users,id',
            'post_id' => 'required|exists:posts,id',
            'email' => 'required|email',
            'name' => 'required',
            'contact_number' => 'required',
            'description' => 'required',
        ]);

        $results = $this->manager->create($request->all());

        return new InquiryResource($results);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\InquiryResource
     */
    public function show($id)
    {
        $results = $this->collection->find($id);

        return new InquiryResource($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $id
     * @return \App\Http\Resources\InquiryResource
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'user_id' => 'sometimes|required|exists:users,id',
            'post_id' => 'sometimes|required|exists:posts,id',
            'email' => 'sometimes|required|email',
            'name' => 'sometimes|required',
            'contact_number' => 'sometimes|required',
            'description' => 'sometimes|required',
        ]);

        $this->manager->update($id, $request->all());

        return $this->show($id);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\InquiryResource
     */
    public function destroy($id)
    {
        $this->manager->delete($id);

        return new InquiryResource(['id' => $id]);
    }
}
