<?php

namespace App\Http\Controllers\User;

use App\Http\Resources\FeatureResource;
use App\Services\Post\PostCollection;
use App\Services\Post\PostFeatureCollection;
use App\Services\Post\PostFeatureManager;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class PostFeatureController
 *
 * @package App\Http\Controllers\User
 */
class PostFeatureController extends Controller
{
    /**
     * @var \App\Services\Post\PostCollection
     */
    protected $postCollection;

    /**
     * @var \App\Services\Post\PostFeatureCollection
     */
    protected $featureCollection;

    /**
     * @var \App\Services\Post\PostFeatureManager
     */
    protected $manager;

    /**
     * PostFeatureController constructor.
     *
     * @param \App\Services\Post\PostCollection $postCollection
     * @param \App\Services\Post\PostFeatureCollection $featureCollection
     * @param \App\Services\Post\PostFeatureManager $manager
     */
    public function __construct(
        PostCollection $postCollection,
        PostFeatureCollection $featureCollection,
        PostFeatureManager $manager
    ) {
        $this->postCollection = $postCollection;

        $this->featureCollection = $featureCollection;

        $this->manager = $manager;
    }

    /**
     * @param $postId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index($postId)
    {
        $results = $this->featureCollection->listByUserAndPost(auth()->id(), $postId);

        return FeatureResource::collection($results);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param $postId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function store(Request $request, $postId)
    {
        $this->validate($request, ['features' => 'required|array']);

        $this->manager->fillByUser(auth()->id(), $postId, $request->get('features'));

        return $this->index($postId);
    }

    /**
     * @param $postId
     * @param $id
     * @return \App\Http\Resources\FeatureResource
     */
    public function show($postId, $id)
    {
        $results = $this->featureCollection->findByUserAndPost(auth()->id(), $postId, $id);

        return new FeatureResource($results);
    }

    /**
     * @param $postId
     * @param $id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function update($postId, $id)
    {
        $this->manager->attachByUser(auth()->id(), $postId, $id);

        return $this->index($postId);
    }

    /**
     * @param $postId
     * @param $id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function destroy($postId, $id)
    {
        $this->manager->detachByUser(auth()->id(), $postId, $id);

        return $this->index($postId);
    }
}
