<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 12:28 PM
 */

namespace App\Entities\Relations;

use App\Entities\User;

/**
 * Trait WithPostLikes
 *
 * @package App\Entities\Relations
 */
trait WithPostLikes
{
    /**
     * postsLikes
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function likes()
    {
        return $this->belongsToMany(User::class, 'posts_likes');
    }
}