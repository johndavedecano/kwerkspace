<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/8/2018
 * Time: 7:34 PM
 */

namespace Tests\Feature\Api\Auth;

use App\Entities\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class ResetControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testStoreFailsValidation()
    {
        $response = $this->post('api/auth/reset');

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }


    public function testStoreSuccess()
    {
        $user = factory(User::class)->create();

        $token = app('auth.password.broker')->createToken($user);

        $data = [
            'token' => $token,
            'password' => 'password',
            'password_confirmation' => 'password'
        ];

        $response = $this->post('api/auth/reset', $data);

        $response->assertStatus(Response::HTTP_OK);
    }
}