<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 9/29/2018
 * Time: 2:41 PM
 */

namespace App\Core;

/**
 * Class Constants
 *
 * @package App\Core
 */
class Constants
{
    const PAGINATION_LIMIT = 25;

    const STATUS_DELETED = 'deleted';

    const STATUS_INACTIVE = 'inactive';

    const STATUS_ACTIVE = 'active';

    const DEFAULT_COUNTRY = 175;

    const USER_SKILLS_LIMIT = 10;

    const POST_FEATURES_LIMIT = 10;

    const POST_IMAGES_LIMIT = 10;
}