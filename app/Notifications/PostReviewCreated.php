<?php

namespace App\Notifications;

use App\Entities\Review;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PostReviewCreated extends Notification implements ShouldQueue
{
    use Queueable;

    protected $review;

    /**
     * PostReviewCreated constructor.
     *
     * @param \App\Entities\Review $review
     */
    public function __construct(Review $review)
    {
        $this->review = $review;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'mail'];
    }

    public function toMail()
    {
        $owner = $this->review->post->user->name;

        $subject = $this->review->user->name. ' reviewed your post';

        return (new MailMessage)
            ->greeting('Hi '.$owner)
            ->subject($subject)
            ->line('Title: '.$this->review->title)
            ->line('Rating: '.$this->review->rating)
            ->line('Description: '.$this->review->description)
            ->line('Date: '.$this->review->created_at)
            ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'review' => $this->review->toArray(),
            'user' => [
                'id' => $this->review->user->id,
                'name' => $this->review->user->name,
                'slug' => $this->review->user->slug,
                'avatar' => $this->review->user->avatar,
            ]
        ];
    }
}
