<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Entities\Inquiry::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->paragraph,
        'contact_number' => $faker->phoneNumber,
        'email' => $faker->email,
        'post_id' => function () {
            return factory(\App\Entities\Post::class)->create()->id;
        },
        'user_id' => $faker->numberBetween(1, 50),
    ];
});
