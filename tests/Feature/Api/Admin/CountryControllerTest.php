<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/6/2018
 * Time: 7:46 PM
 */

namespace Tests\Feature\Api\Admin;

use App\Entities\Country;
use App\Entities\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\Feature\Api\Authentication;
use Tests\TestCase;

/**
 * Class CountryControllerTest
 *
 * @package Tests\Feature\Api\Admin
 */
class CountryControllerTest extends TestCase
{
    use RefreshDatabase, Authentication;

    public function setUp()
    {
        parent::setUp();

        $this->admin = $this->getAuthHeaders(User::ROLE_ADMIN);

        $this->user = $this->getAuthHeaders(User::ROLE_USER);
    }

    public function testIndexFails()
    {
        $response = $this->withHeaders(['Accept' => 'application/json'])->get('/api/admin/countries');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testIndexFailsNotAllowed()
    {
        $response = $this->withHeaders($this->user)->get('/api/admin/countries');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testIndexSuccess()
    {
        $response = $this->withHeaders($this->admin)->get('/api/admin/countries');

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testStoreFails()
    {
        $response = $this->withHeaders(['Accept' => 'application/json'])->post('/api/admin/countries');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testStoreValidationFails()
    {
        $response = $this->withHeaders($this->admin)->post('/api/admin/countries');

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testStoreSuccess()
    {
        $response = $this->withHeaders($this->admin)->post('/api/admin/countries', [
            'name' => 'test',
            'slug' => str_slug(str_random()),
        ]);

        $response->assertStatus(Response::HTTP_CREATED);
    }

    public function testUpdateFails()
    {
        $results = factory(Country::class)->create();

        $response = $this->withHeaders(['Accept' => 'application/json'])->put('/api/admin/countries/'.$results->id);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testUpdateFailsValidation()
    {
        $results = factory(Country::class)->create();

        $response = $this->withHeaders($this->admin)->put('/api/admin/countries/'.$results->id, ['name' => '']);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    public function testUpdateSuccess()
    {
        $results = factory(Country::class)->create();

        $response = $this->withHeaders($this->admin)->put('/api/admin/countries/'.$results->id, [
            'name' => str_random(),
            'slug' => str_slug(str_random()),
        ]);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testDestroyFails()
    {
        $results = factory(Country::class)->create();

        $response = $this->withHeaders(['Accept' => 'application/json'])->delete('/api/admin/countries/'.$results->id);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testDestroyFailsNotFound()
    {
        $response = $this->withHeaders($this->admin)->delete('/api/admin/countries/3496878963');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testDestroySuccess()
    {
        $results = factory(Country::class)->create();

        $response = $this->withHeaders($this->admin)->delete('/api/admin/countries/'.$results->id);

        $response->assertStatus(Response::HTTP_OK);
    }

    public function testShowFails()
    {
        $results = factory(Country::class)->create();

        $response = $this->withHeaders(['Accept' => 'application/json'])->get('/api/admin/countries/'.$results->id);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testShowFailsNotFound()
    {
        $response = $this->withHeaders($this->admin)->get('/api/admin/countries/3496878963');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testShowSuccess()
    {
        $results = factory(Country::class)->create();

        $response = $this->withHeaders($this->admin)->get('/api/admin/countries/'.$results->id);

        $response->assertStatus(Response::HTTP_OK);
    }
}