<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/6/2018
 * Time: 8:36 PM
 */

namespace Tests\Feature\Api;

use App\Core\Constants;
use App\Entities\Country;
use App\Entities\User;
use Tymon\JWTAuth\Facades\JWTAuth;

/**
 * Trait Authentication
 *
 * @package Tests\Api\Admin
 */
trait Authentication
{
    public $auth = [];

    /**
     * @return mixed
     */
    private function country()
    {
        return factory(Country::class)->create();
    }

    /**
     * @param string $role
     * @return mixed
     */
    private function user($role = 'admin')
    {
        $country = $this->country();

        $user = factory(User::class)->create([
            'email_verified_at' => date('Y-m-d H:i:s'),
            'status' => Constants::STATUS_ACTIVE,
            'role' => $role,
            'country_id' => $country->id,
        ]);

        $this->auth[$role] = $user;

        return $user;
    }

    /**
     * @param \App\Entities\User $user
     * @return mixed
     */
    public function token(User $user)
    {
        return JWTAuth::fromUser($user);
    }

    /**
     * @param string $role
     * @return array
     */
    public function getAuthHeaders($role = 'admin')
    {
        $user = $this->user($role);

        $token = $this->token($user);

        return [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$token,
        ];
    }
}