<?php

namespace App\Http\Controllers\Common;

use App\Http\Resources\FeatureResource;
use App\Services\Feature\FeatureCollection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
 * Class FeatureController
 *
 * @package App\Http\Controllers\Common
 */
class FeatureController extends Controller
{
    /**
     * @var \App\Services\Feature\FeatureCollection
     */
    public $collection;

    /**
     * FeatureController constructor.
     *
     * @param \App\Services\Feature\FeatureCollection $collection
     */
    public function __construct(FeatureCollection $collection)
    {
        $this->collection = $collection;
    }

    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index()
    {
        $collection = $this->collection->listFromCommon();

        return FeatureResource::collection($collection);
    }

    /**
     * @param $id
     * @return \App\Http\Resources\FeatureResource
     */
    public function show($id)
    {
        $results = $this->collection->findNotDeleted($id);

        return new FeatureResource($results);
    }
}
