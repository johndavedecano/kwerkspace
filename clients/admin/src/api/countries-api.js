// countries-api.js
import axios from 'axios'

export function load(params = {}) {
  return axios
    .get('/admin/countries', { params })
    .then(repsonse => repsonse.data)
}

export function show(id) {
  return axios.get(`/admin/countries/${id}`).then(repsonse => repsonse.data)
}

export function destroy(id) {
  return axios.delete(`/admin/countries/${id}`).then(repsonse => repsonse.data)
}

export function update(id, params = {}) {
  return axios
    .put(`/admin/countries/${id}`, params)
    .then(repsonse => repsonse.data)
}

export function create(params = {}) {
  return axios.post('/admin/countries', params).then(repsonse => repsonse.data)
}

export function list(params = {}) {
  return axios
    .get('/common/countries?all=true', { params })
    .then(repsonse => repsonse.data)
}
