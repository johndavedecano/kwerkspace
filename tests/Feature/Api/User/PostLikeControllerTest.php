<?php
/**
 * Created by PhpStorm.
 * User: Dave
 * Date: 10/6/2018
 * Time: 7:46 PM
 */

namespace Tests\Feature\Api\User;

use App\Entities\Post;
use App\Entities\PostsLike;
use App\Entities\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\Feature\Api\Authentication;
use Tests\TestCase;

/**
 * Class PostLikeControllerTest
 * @package Tests\Feature\Api\User
 */
class PostLikeControllerTest extends TestCase
{
    use RefreshDatabase, Authentication;

    public function setUp()
    {
        parent::setUp();

        $this->user = $this->getAuthHeaders(User::ROLE_USER);
    }

    public function testPostLikeFailsUnauthorized()
    {
        $response = $this->withHeaders([])->post('/api/user/posts/564654654654654/likes');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testPostLikeFails()
    {
        $response = $this->withHeaders($this->user)->post('/api/user/posts/564654654654654/likes');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testPostLikeSuccess()
    {
        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $response = $this->withHeaders($this->user)->post('/api/user/posts/'.$post->id.'/likes');

        $response->assertStatus(Response::HTTP_OK);
    }


    public function testPostUnlikeFailsUnauthorized()
    {
        $response = $this->withHeaders([])->delete('/api/user/posts/564654654654654/likes');

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function testPostUnlikeFails()
    {
        $response = $this->withHeaders($this->user)->delete('/api/user/posts/564654654654654/likes');

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    public function testPostUnlikeSuccess()
    {
        $post = factory(Post::class)->create(['user_id' => $this->auth['user']->id]);

        $response = $this->withHeaders($this->user)->delete('/api/user/posts/'.$post->id.'/likes');

        $response->assertStatus(Response::HTTP_OK);
    }
}