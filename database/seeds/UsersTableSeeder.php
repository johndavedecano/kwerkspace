<?php

use Illuminate\Database\Seeder;
use App\Entities\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        User::create([
            'id'         => 1,
            'name'       => 'Admin',
            'email'      => 'admin@admin.com',
            'role'       => User::ROLE_ADMIN,
            'password'   => 'password',
            'country_id' => 1,
            'slug'       => 'admin',
            'email_verified_at' => date('Y-m-d H:i:s'),
            'status'     => \App\Core\Constants::STATUS_ACTIVE
        ]);

        factory(User::class, \App\Core\Constants::PAGINATION_LIMIT * 10)->create();
    }
}
