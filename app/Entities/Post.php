<?php

namespace App\Entities;

use App\Entities\Others\WithSlug;
use App\Entities\Relations\WithCategory;
use App\Entities\Relations\WithCountry;
use App\Entities\Relations\WithFeatures;
use App\Entities\Relations\WithInquiries;
use App\Entities\Relations\WithPostImages;
use App\Entities\Relations\WithPostLikes;
use App\Entities\Relations\WithReplies;
use App\Entities\Relations\WithReviews;
use App\Entities\Relations\WithUser;
use App\Entities\Scopes\ScopeActive;
use App\Entities\Scopes\ScopeDeleted;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Post
 *
 * @package App\Entities
 *
 */
class Post extends Model
{
    use WithInquiries, WithFeatures, WithCountry, WithUser, WithReplies, WithPostLikes, WithReviews, WithCategory, WithPostImages;

    use ScopeActive, ScopeDeleted;

    use WithSlug;

    const PAYMENT_TYPE_MONTHLY = 'monthly';

    const PAYMENT_TYPE_WEEKLY = 'weekly';

    const PAYMENT_TYPE_DAILY = 'daily';

    const PAYMENT_TYPE_ANNUALLY = 'annually';

    const PAYMENT_TYPE_HOURLY = 'hourly';

    /**
     * Database table name
     */
    protected $table = 'posts';

    /**
     * Protected columns from mass assignment
     */
    protected $guarded = ['id'];

    /**
     * Mass assignable columns
     */
    protected $fillable = [
        'user_id',
        'category_id',
        'country_id',
        'title',
        'slug',
        'status',
        'description',
        'policies',
        'thumbnail',
        'cover',
        'max_occupants',
        'payment_type',
        'payment_amount',
        'area',
        'address',
        'city',
        'state',
        'zipcode',
        'lon',
        'lat',
        'mon',
        'tue',
        'wed',
        'thu',
        'fri',
        'sat',
        'sun',
        'rating',
    ];

    /**
     * @var string
     */
    public $slugFrom = 'title';

    /**
     * @var string
     */
    public $slugTo = 'slug';

    /**
     * @param $value
     * @return array|mixed
     */
    public function getCoverAttribute($value)
    {
        try {
            return (array) json_decode($value);
        } catch (\Exception $e) {
            return [];
        }
    }

    /**
     * @param $value
     * @return array|mixed
     */
    public function getThumbnailAttribute($value)
    {
        try {
            return (array) json_decode($value);
        } catch (\Exception $e) {
            return [];
        }
    }
}